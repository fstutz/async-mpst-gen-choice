# test_proj_classical.py

from datatypes.classical_projection.GlobalTypeTop import *
from parsing.InputParser import get_input_from_file


def test_proj_avail_msgs_1():
    res = get_input_from_file("../global_types/types_for_tests/avail_msgs_1.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None


def test_proj_avail_msgs_2():
    res = get_input_from_file("../global_types/types_for_tests/avail_msgs_2.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'P' or proc == 'R':
            # projections for P and R not defined as expected
            assert proj is None
        elif proc == 'Q':
            assert proj is not None


def test_proj_merge_recursions():
    res = get_input_from_file("../global_types/types_for_tests/merge_recursions.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None


def test_proj_empty_loops():
    res = get_input_from_file("../global_types/types_for_tests/empty_loops.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'P' or proc == 'R':
            assert proj is None
        elif proc == 'Q' or proc == 'S':
            assert proj is not None


def test_proj_empty_loops_2():
    res = get_input_from_file("../global_types/types_for_tests/empty_loops_2.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None


def test_proj_empty_loops_3():
    res = get_input_from_file("../global_types/types_for_tests/empty_loops_3.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None


def test_proj_break_languages():
    res = get_input_from_file("../global_types/types_for_tests/break_languages.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'P' or proc == 'R':
            assert proj is not None
        elif proc == 'Q':
            assert proj is None


def test_proj_break_languages_2():
    res = get_input_from_file("../global_types/types_for_tests/break_languages_2.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None


def test_proj_loop_with_exit():
    res = get_input_from_file("../global_types/types_for_tests/loop_with_exit.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None

def test_example_intro_not_projectable_with_classical():
    res = get_input_from_file("../global_types/types_for_tests/example_intro_version_not_projectable_classical.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'R':
            assert proj is None
        elif proc == 'P' or proc == 'Q':
            assert proj is not None

def test_example_intro_projectable_with_classical():
    res = get_input_from_file("../global_types/types_for_tests/example_intro_version_projectable_classical.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None

def test_odd_even():
    res = get_input_from_file("../global_types/types_for_tests/odd_even.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'R':
            assert proj is None
        elif proc == 'P' or proc == 'Q':
            assert proj is not None

def test_final_state_outgoing_receive():
    res = get_input_from_file("../global_types/types_for_tests/final_state_outgoing_receive.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'R':
            assert proj is None
        elif proc == 'P' or proc == 'Q':
            assert proj is not None

def test_first_disagreement_must_be_send():
    res = get_input_from_file("../global_types/types_for_tests/first_disagreement_must_be_send_event.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'S':
            assert proj is None
        else:
            assert proj is not None

def test_two_buyer_protocol():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None

def test_two_buyer_protocol_omit_no():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol_omit_no.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'S':
            assert proj is None
        else:
            assert proj is not None

def test_two_buyer_protocol_subscribe():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol_subscription.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'B':
            assert proj is None
        else:
            assert proj is not None

def test_two_buyer_protocol_inner_recursion():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol_inner_recursion.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        assert proj is not None

def test_motivation_rcv_validity_negative():
    res = get_input_from_file("../global_types/projectability/motivation_rcv_validity_negative.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'R':
            assert proj is None
        else:
            assert proj is not None

def test_motivation_snd_validity_negative():
    res = get_input_from_file("../global_types/projectability/motivation_snd_validity_negative.gt")
    tplv_type = GlobalTypeTop(res)
    for proc in tplv_type.get_procs():
        proj = tplv_type.project_onto(proc)
        if proc == 'R':
            assert proj is None
        else:
            assert proj is not None
