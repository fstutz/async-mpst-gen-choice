# test_parser_helpers.py

from parsing.InputParser import get_input_from_file
from datatypes.classical_projection.GlobalTypeTop import *


def test_simple_globaltype_parser_set_of_processes_top_level():
    res = get_input_from_file("../global_types/types_for_tests/simple_globaltype.gt")
    # print('\n' + str(res) + '\n')
    # print(', '.join(str(s) for s in res.get_all_processes()))
    assert res.get_procs() == ['P', 'Q', 'Q1']
    toplv = GlobalTypeTop(res)
    # print('\n' + str(toplv))


def test_workload_balancer_2_parser_set_of_processes_top_level():
    res = get_input_from_file("../global_types/parametric/base_types/load_balancer_2.gt")
    # print('\n' + str(res) + '\n')
    # print(', '.join(str(s) for s in res.get_all_processes()))
    assert res.get_procs() == ['C', 'S', 'W1', 'W2']
    toplv = GlobalTypeTop(res)
    # print('\n' + str(toplv))


def test_group_present_parser_set_of_processes_top_level():
    res = get_input_from_file("../global_types/types_for_tests/group_present.gt")
    # print('\n' + str(res) + '\n')
    # print(', '.join(str(s) for s in res.get_all_processes()))
    assert res.get_procs() == ['F1', 'F2', 'F3', 'S']
    toplv = GlobalTypeTop(res)
    # print('\n' + str(toplv))


def test_logging_1_parser_set_of_processes_top_level():
    res = get_input_from_file("../global_types/parametric/base_types/logging_1.gt")
    # print('\n' + str(res) + '\n')
    # print(', '.join(str(s) for s in res.get_all_processes()))
    assert res.get_procs() == ['BE', 'C', 'LOG', 'WEB']
    toplv = GlobalTypeTop(res)
    # print('\n' + str(toplv))

