# test_proj_subset.py

from datatypes.subset_projection.GlobalTypeFSM import GlobalTypeFSM
from parsing.InputParser import get_input_from_file


def test_subset_proj_avail_msgs_1():
    res = get_input_from_file("../global_types/types_for_tests/avail_msgs_1.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None


def test_proj_avail_msgs_2():
    res = get_input_from_file("../global_types/types_for_tests/avail_msgs_2.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        if proc == 'P' or proc == 'R':
            # checked the subset construction of P for reasonability
            # projections for P and R not defined as expected
            assert proj is None
        elif proc == 'Q':
            assert proj is not None


def test_proj_merge_recursions():
    res = get_input_from_file("../global_types/types_for_tests/merge_recursions.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None


def test_proj_empty_loops():
    res = get_input_from_file("../global_types/types_for_tests/empty_loops.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None
    # CHECK: The previous projection could not handle this but our subset projection can
        # if proc == 'P' or proc == 'R':
        #     assert proj is None
        # elif proc == 'Q' or proc == 'S':
        #     assert proj is not None


def test_proj_empty_loops_2():
    res = get_input_from_file("../global_types/types_for_tests/empty_loops_2.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None


def test_proj_empty_loops_3():
    res = get_input_from_file("../global_types/types_for_tests/empty_loops_3.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None


def test_proj_break_languages():
    res = get_input_from_file("../global_types/types_for_tests/break_languages.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None
        # CHECK: The previous projection could not handle this but our subset projection can
        # if proc == 'P' or proc == 'R':
        #     assert proj is not None
        # elif proc == 'Q':
        #     assert proj is None


def test_proj_break_languages_2():
    res = get_input_from_file("../global_types/types_for_tests/break_languages_2.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None


def test_proj_loop_with_exit():
    res = get_input_from_file("../global_types/types_for_tests/loop_with_exit.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_example_intro_not_projectable_with_classical():
    res = get_input_from_file("../global_types/types_for_tests/example_intro_version_not_projectable_classical.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_example_intro_projectable_with_classical():
    res = get_input_from_file("../global_types/types_for_tests/example_intro_version_projectable_classical.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_odd_even():
    res = get_input_from_file("../global_types/types_for_tests/odd_even.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_deadlock_characterization_needs_send_state_val():
    res = get_input_from_file("../global_types/types_for_tests/deadlock_characterization_needs_send_state_val.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        if proc == 'R':
            assert proj is None
        elif proc == 'P' or proc == 'Q':
            assert proj is not None

def test_final_state_outgoing_receive():
    res = get_input_from_file("../global_types/types_for_tests/final_state_outgoing_receive.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_mixed_choice_needs_finite_runs_for_necessity():
    res = get_input_from_file("../global_types/types_for_tests/mixed_choice_needs_finite_runs_for_necessity.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        if proc == 'R':
            assert proj is None
        elif proc == 'P' or proc == 'Q':
            assert proj is not None

def test_first_disagreement_must_be_send():
    res = get_input_from_file("../global_types/types_for_tests/first_disagreement_must_be_send_event.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        if proc == 'S':
            assert proj is None
        else:
            assert proj is not None

def test_two_buyer_protocol():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_two_buyer_protocol_omit_no():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol_omit_no.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_two_buyer_protocol_subscribe():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol_subscription.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_two_buyer_protocol_inner_recursion():
    res = get_input_from_file("../global_types/types_for_tests/two_buyer_protocol_inner_recursion.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_state_explosion():
    res = get_input_from_file("../global_types/parametric/state_explosion/state_explosion_3.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_empty_loop_no_exit():
    # in this example, C does not have any final states, which is fine as we only have infinite executions
    res = get_input_from_file("../global_types/types_for_tests/empty_loop_no_exit.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_empty_loop_with_exit():
    # in contrast to the previous example, C has a final state
    res = get_input_from_file("../global_types/types_for_tests/empty_loop_with_exit.gt")
    fsm = GlobalTypeFSM(res)
    for proc in res.get_procs():
        proj = fsm.project_onto(proc)
        assert proj is not None

def test_same_subterm_one_state():
    # for this example, there are common subterms, so one sees a difference
    res = get_input_from_file("../global_types/types_for_tests/same_subterm.gt")
    fsm = GlobalTypeFSM(res)