# test_parser_helpers.py
from copy import deepcopy

from parsing.InputParser import get_input_from_file
from datatypes.subset_projection.GlobalTypeFSM import *
from datatypes.GlobalTypeParsed import GlobalTypeParsed


def test_simple_globaltype_parser_set_of_processes_top_level():
    res = get_input_from_file("../global_types/types_for_tests/simple_globaltype.gt")
    # print('\n' + str(res) + '\n')
    # print(', '.join(str(s) for s in res.get_all_processes()))
    fsm = GlobalTypeFSM(res)
    # print('\n' + str(fsm))
    proj_by_erasure = ProjectionByErasure(fsm, "Q1")
    # print('\n' + str(proj_by_erasure))
    proj = fsm.project_onto("P")
    # print('\n' + str(proj))


def test_same_subterm_one_state():
    gt1 = get_input_from_file("../global_types/types_for_tests/same_subterm_2.gt")
    gt2 = deepcopy(gt1)
    res = GlobalTypeParsed.compare_global_types(gt1, gt2)

def test_same_subterm_one_state_state_exp():
    gt1 = get_input_from_file("../global_types/types_for_tests/state_explosion_3.gt")
    res = GlobalTypeFSM(gt1)
    # print(res)

