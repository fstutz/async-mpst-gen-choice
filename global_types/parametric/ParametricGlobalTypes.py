import math
from enum import Enum
from parsing.InputParser import get_tplv_gt_from_file
import glob
from os import path
from evaluation_functionality.evaluation_config import PREFIX_PARAMETRIC_TYPES


class ParametricGlobalTypes:

    @staticmethod
    def get_representation_from_comps(num, prefix, line_generator, postfix):
        res = prefix
        res += "\n\t+\n".join(line_generator(i) for i in range(1, num+1))
        res += postfix
        return res

    @staticmethod
    def get_load_balancer_representation(num):
        prefix = "μ t. C -> S : w . ( \n"
        # nested function definition for lines
        def line_generator(i): return "\tS -> W" + str(i) + " : w. W" + str(i) + " -> C: r . t"
        postfix = "\n )"
        return ParametricGlobalTypes.get_representation_from_comps(num, prefix, line_generator, postfix)

    @staticmethod
    def get_logging_representation(num):
        prefix = "μ t. C -> Web : req. ( \n\tWeb -> C: answer.Web -> Log: log. Log -> Web: ack. t \n\t+ \n "
        # nested function definition for lines
        def line_generator(i): return "\tWeb -> BE" + str(i) + " : req. BE" + str(i) + " -> Log: log. " + \
                                      "Log -> BE" + str(i) + " : ack. BE" + str(i) + " -> C: answer. t"
        postfix = "\n )"
        return ParametricGlobalTypes.get_representation_from_comps(num, prefix, line_generator, postfix)

    @staticmethod
    def get_mem_cache_representation(num):
        prefix = "μ t. Client -> Cache: addr.(\n\tCache -> Client: data.t \n\t+ \n"

        # nested function definition for lines
        def line_generator(i): return "\tCache -> Mem" + str(i) + " : addr. Mem" + str(i) + " -> Cache: data. " + \
                                      "Cache -> Client : data. t"
        postfix = "\n )"
        return ParametricGlobalTypes.get_representation_from_comps(num, prefix, line_generator, postfix)

    @staticmethod
    def get_p2p_broadcast_representation(num):
        prefix = "μ t. (\n"

        midfix = ""
        for i in range(1, num+1):
            for j in range(1, num+1):
                if i != j:
                    midfix += '\tW' + str(i) + ' -> ' + 'W' + str(j) + ': bc.\n'

        postfix = "\tt\n)"
        return prefix + midfix + postfix

    @staticmethod
    def get_tree_broadcast_representation(num_workers):
        prefix = "// indices are not used incrementally \n"
        prefix += "μ t. (\n"

        num_full_layers = int(math.log(num_workers, 2))
        num_full_workers = int(math.pow(2, num_full_layers + 1))
        num_left_workers = num_workers - int(num_full_workers/2)
        midfix = ''
        layer_counter = num_full_layers
        while layer_counter > 0:
            step_size = int(math.pow(2, layer_counter))
            i = 1
            while i <= num_full_workers:
                midfix += '\tW' + str(i) + ' -> W' + str(i + step_size) + ': bc.\n'
                i += 2 * step_size
            layer_counter -= 1
            midfix += '\n'
        # one more incomplete layer
        step_size = 1 # int(math.pow(2, layer_counter))
        i = 1
        j = num_left_workers
        while i <= num_full_workers and j > 0:
            midfix += '\tW' + str(i) + ' -> W' + str(i + step_size) + ': bc.\n'
            i += 2 * step_size
            j -= 1
        if num_left_workers != 0:
            midfix += '\n'

        postfix = "\tt\n)"
        return prefix + midfix + postfix

    @staticmethod
    def get_map_reduce_representation(num_workers_per_key, num_keys):

        # 1
        res_str = "IO -> W: req. \n\n"

        # 2
        res_map_str = '// map \n'
        for key in range(1, num_keys+1):
            list_map_str = ['W -> W' + str(num_workers_per_key * (key-1) + w) + ': mp.\n' for w in range(1, num_workers_per_key+1)]
            res_map_str += ''.join(s for s in list_map_str)
        res_str += res_map_str
        res_str += '\n'

        # 3a
        res_fst_layer = '// remap key results \n'
        for key in range(1, num_keys+1):
            for key2 in range(1, num_keys+1):
                if key != key2:
                    list_map_str = ['W' + str(num_workers_per_key * (key-1) + i) +
                                    ' -> W' + str(num_workers_per_key * (key2-1) + i) +
                                    ': k' + str(key2) + '.\n'
                                    for i in range(1, num_workers_per_key+1)]
                    res_fst_layer += ''.join(s for s in list_map_str)
        res_str += res_fst_layer
        res_str += '\n'

        # 3b
        red_str = "// reduce layers \n\n"
        for key in range(1, num_keys+1):
            i = 1
            while i+1 <= num_workers_per_key:
                red_str += '// k' + str(key) + ' combined: ' + str(i) + '\n'
                w = 1
                while w+i <= num_workers_per_key:
                    red_str += 'W' + str(num_workers_per_key * (key-1) + w+i) + \
                               ' -> W' + str(num_workers_per_key * (key-1) + w) + ': k' + str(key) + '.\n'
                    w += 2*i
                i *= 2
            red_str += '\n'

        res_str += red_str
        res_str += '\n'

        # 4
        red_fin_str = '// propagate\n'
        for key in range(1, num_keys+1):
            red_fin_str += 'W' + str(num_workers_per_key * (key-1) + 1) + ' -> W: rd' + str(key) + '.\n'

        res_str += red_fin_str

        # 5
        res_str += "\nW -> IO: ans. \n0\n"

        return res_str

    @staticmethod
    def get_state_explosion_representation(param_r):
        assert param_r > 1
        # Intro
        res_str = "μ t1.(P->R:m1. 0 + \n" \
                  "      P->R:n1. μ t2. ( P->R:m2. P->Q:a. t2 + \n" \
                  "                       P->R:n2. P->Q:a. "
        closing_parens = "))"
        # Loops
        for i in range(1, param_r):
            res_str += "μ t3c" + str(i) + ". \n" + \
                       "                     ( P->R:m3c" + str(i) + ". P->Q:b. t3c" + str(i) + " + \n" \
                                                                                               "                       P->R:n3c" + str(
                i) + ". P->Q:a. "
            closing_parens += ")"
        res_str += "t1 " + closing_parens
        return res_str

    @staticmethod
    # this actually might generate one (more) type which is greater than the given size but this is fine
    def write_representation_up_to_size(file_prefix, repr_func, size, start, snd=None):
        current_size = 0
        i = start
        while current_size <= size:
            filename = file_prefix + str(i) + ".gt"
            # create (or delete content of) the file and write representation to it
            if snd is not None:
                gt_repr = repr_func(i, snd)
            else:
                gt_repr = repr_func(i)
            text_file = open(filename, "w+")
            text_file.write(gt_repr)
            text_file.close()
            # get type and update size:
            tplv_type = get_tplv_gt_from_file(filename)
            current_size = tplv_type.get_size()
            # increase parameter
            i += 1

    @staticmethod
    def write_representation_up_to_size_w_kind(kind, size, snd=None):
        filename_prefix = PREFIX_PARAMETRIC_TYPES + kind.value + "/" + kind.value + "_"
        start = ParametricKinds.get_start(kind)
        write_repr_func = ParametricKinds.get_write_repr_func(kind)
        if kind == ParametricKinds.MAP_REDUCE:
            filename_prefix += "k" + str(snd) + "_"
        ParametricGlobalTypes.write_representation_up_to_size(filename_prefix, write_repr_func, size, start, snd)

    @staticmethod
    def write_spin_repr(kind):
        directory_from = "../" + PREFIX_PARAMETRIC_TYPES + kind.value + "/"
        directory_to = "../evaluation_data/" + kind.value + "/spin_code/"
        list_of_files = glob.glob(directory_from + "*.gt")
        for filename in list_of_files:
            basename = path.splitext(path.basename(filename))[0]
            try:
                f = open(directory_to + basename + ".pml")
                f.close()
            except FileNotFoundError:
                tplv_type = get_tplv_gt_from_file(filename)
                tplv_type.write_spin_repr(directory_to + basename + ".pml")


class ParametricKinds(Enum):
    LOAD_BALANCER = 'load_balancer'
    LOGGING = 'logging'
    MEM_CACHE = 'mem_cache'
    P2P_BROADCAST = 'p2p_broadcast'
    TREE_BROADCAST = 'tree_broadcast'
    MAP_REDUCE = 'map_reduce'
    STATE_EXPLOSION = 'state_explosion'

    @staticmethod
    def get_start(kind):
        if kind == ParametricKinds.LOAD_BALANCER:
            return 1
        elif kind == ParametricKinds.LOGGING:
            return 1
        elif kind == ParametricKinds.MEM_CACHE:
            return 1
        elif kind == ParametricKinds.P2P_BROADCAST:
            return 2
        elif kind == ParametricKinds.TREE_BROADCAST:
            return 2
        elif kind == ParametricKinds.MAP_REDUCE:
            return 1
        elif kind == ParametricKinds.STATE_EXPLOSION:
            return 2
        else:
            raise("get_start for parametric example called for non-parametric example")

    @staticmethod
    def get_write_repr_func(kind):
        if kind == ParametricKinds.LOAD_BALANCER:
            return ParametricGlobalTypes.get_load_balancer_representation
        elif kind == ParametricKinds.LOGGING:
            return ParametricGlobalTypes.get_logging_representation
        elif kind == ParametricKinds.MEM_CACHE:
            return ParametricGlobalTypes.get_mem_cache_representation
        elif kind == ParametricKinds.P2P_BROADCAST:
            return ParametricGlobalTypes.get_p2p_broadcast_representation
        elif kind == ParametricKinds.TREE_BROADCAST:
            return ParametricGlobalTypes.get_tree_broadcast_representation
        elif kind == ParametricKinds.MAP_REDUCE:
            return ParametricGlobalTypes.get_map_reduce_representation
        elif kind == ParametricKinds.STATE_EXPLOSION:
            return ParametricGlobalTypes.get_state_explosion_representation
        else:
            raise Exception('This is no kind of parametric example.')

    @staticmethod
    def has_snd(kind):
        if kind == ParametricKinds.MAP_REDUCE:
            return True
        else:
            return False
