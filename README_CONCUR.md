# Generalising Projection in Asynchronous Multiparty Session Types

This prototype implements a generalised projection operator for asynchronous Multiparty Session Types.
In particular, it uses a global message causality analysis to ensure correctness of projections.

## Corresponding Publication

This work has been published at CONCUR 2021 and can be found on [Arxiv](https://arxiv.org/abs/2107.03984).
To use the same version for reproducing results, please refer to the corresponding tag.

## Contact

- Felix Stutz (fstutz@mpi-sws.org)

## Reproducing the results

Two experiments can be reproduced using this repo:

1) "Projectability" obtains the run times of projecting examples (of which some can only be projected with our generalised projection operator);
   The models can be found in `global_types/projectability`.
2) "Overhead" obtains a plot which compares the run time for four parametrisable examples which do not need the generalised projection operator:
   first using lazy evaluation for message causality analysis and second not computing any information for the latter.
   Some models (up to some initial size) can be found in individual folders `global_types/parametric`, e.g. mem_cache.
   When provided with bigger size bounds, the tool will create new models.


## Installing with `virtualenv`
The program is written in **Python 3.8**.

```bash
# If virtualenv is not installed:
pip3 install virtualenv
pip3 virtualenv venv # create new virtualenv in venv
source venv/bin/activate
# Install the dependencies:
pip3 install -r requirements.txt
```

## Running the experiments

When we activated the virtual environment (and installed the requirements), 
we can run the `help`-command to list the different options:

```bash
(venv) user@computer:path$ python3.8 main.py -h 
usage: main.py [-h] [--option O] [--num_runs N] [--size S] [--save_to FILE]

optional arguments:
  -h, --help      show this help message and exit
  --option O      specifies which experiment should be run: 'projectability_classical' (default) produces a table of individual projection times; 'overhead' plots a graph for four parametrisable examples up to some size
  --num_runs N    specifies the number of runs
  --size S        specifies the size up to which the overhead experiment should run; default is 500 and can easily need 15 minutes per run
  --save_to FILE  store output in file (relative to where the script is called from); will not overwrite existing files but then print instead
```
The output can be saved to a file. However, the tool does not overwrite existing files but output then either on `stdout` or show the plot.

1) **Projectability**   
This is the default option and it uses solely one parameter `num_runs` which defaults to 10 for this option. 
   It outputs a table of the following format:
   ```bash
   Name                    Size     |P|     Time in ms (average of 10 runs)
   Instr. Contr. Prot. A    16       3      0.384    
   Instr. Contr. Prot. B    13       3      0.308    
   Multi Party Game         16       3      0.353    
   OAuth2                    7       3      0.206    
   Streaming                 7       4      0.227    
   Non-Compatible Merge      8       3      0.261    
   Spring-Hibernate         44       6      1.573    
   Group Present            43       4      1.336    
   Late Learning            12       4      0.413    
   Load Balancer (n=10)     32      12      4.222    
   Logging (n=10)           56      13     17.212    
   ```

2) **Overhead**   
   This can be run by providing `--option overhead`.
   The default parameters are `size=500` and `num_runs=1` which takes approximately 15 minutes to complete.
   Note that a single run can suffer from outliers.
