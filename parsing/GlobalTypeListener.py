# Generated from /home/fms/git-repos/Session MSC/async-mpst-gen-choice/parser/GlobalType.g4 by ANTLR 4.9
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .GlobalTypeParser import GlobalTypeParser
else:
    from GlobalTypeParser import GlobalTypeParser

# This class defines a complete listener for a parse tree produced by GlobalTypeParser.
class GlobalTypeListener(ParseTreeListener):

    # Enter a parse tree produced by GlobalTypeParser#spec.
    def enterSpec(self, ctx:GlobalTypeParser.SpecContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#spec.
    def exitSpec(self, ctx:GlobalTypeParser.SpecContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#globaltype.
    def enterGlobaltype(self, ctx:GlobalTypeParser.GlobaltypeContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#globaltype.
    def exitGlobaltype(self, ctx:GlobalTypeParser.GlobaltypeContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#null_type.
    def enterNull_type(self, ctx:GlobalTypeParser.Null_typeContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#null_type.
    def exitNull_type(self, ctx:GlobalTypeParser.Null_typeContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#choice_msg_exc.
    def enterChoice_msg_exc(self, ctx:GlobalTypeParser.Choice_msg_excContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#choice_msg_exc.
    def exitChoice_msg_exc(self, ctx:GlobalTypeParser.Choice_msg_excContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#single_choice.
    def enterSingle_choice(self, ctx:GlobalTypeParser.Single_choiceContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#single_choice.
    def exitSingle_choice(self, ctx:GlobalTypeParser.Single_choiceContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#msg_exchange.
    def enterMsg_exchange(self, ctx:GlobalTypeParser.Msg_exchangeContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#msg_exchange.
    def exitMsg_exchange(self, ctx:GlobalTypeParser.Msg_exchangeContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#recursion.
    def enterRecursion(self, ctx:GlobalTypeParser.RecursionContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#recursion.
    def exitRecursion(self, ctx:GlobalTypeParser.RecursionContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#bind_rec_variable.
    def enterBind_rec_variable(self, ctx:GlobalTypeParser.Bind_rec_variableContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#bind_rec_variable.
    def exitBind_rec_variable(self, ctx:GlobalTypeParser.Bind_rec_variableContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#ref_rec_variable.
    def enterRef_rec_variable(self, ctx:GlobalTypeParser.Ref_rec_variableContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#ref_rec_variable.
    def exitRef_rec_variable(self, ctx:GlobalTypeParser.Ref_rec_variableContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#proc.
    def enterProc(self, ctx:GlobalTypeParser.ProcContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#proc.
    def exitProc(self, ctx:GlobalTypeParser.ProcContext):
        pass


    # Enter a parse tree produced by GlobalTypeParser#msg.
    def enterMsg(self, ctx:GlobalTypeParser.MsgContext):
        pass

    # Exit a parse tree produced by GlobalTypeParser#msg.
    def exitMsg(self, ctx:GlobalTypeParser.MsgContext):
        pass



del GlobalTypeParser