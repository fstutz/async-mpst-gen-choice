chan W35_W15 = [1] of { int };
chan W25_W21 = [1] of { int };
chan W34_W33 = [1] of { int };
chan W_W33 = [1] of { int };
chan W36_W35 = [1] of { int };
chan W16_W15 = [1] of { int };
chan W_W5 = [1] of { int };
chan W_W38 = [1] of { int };
chan W20_W19 = [1] of { int };
chan W25_W5 = [1] of { int };
chan W_W22 = [1] of { int };
chan W19_W17 = [1] of { int };
chan W22_W2 = [1] of { int };
chan W39_W37 = [1] of { int };
chan W18_W38 = [1] of { int };
chan W_W23 = [1] of { int };
chan W21_W = [1] of { int };
chan W14_W13 = [1] of { int };
chan W13_W33 = [1] of { int };
chan W28_W8 = [1] of { int };
chan W27_W25 = [1] of { int };
chan W10_W30 = [1] of { int };
chan W_W28 = [1] of { int };
chan W_W27 = [1] of { int };
chan W_W24 = [1] of { int };
chan W_W21 = [1] of { int };
chan W_W1 = [1] of { int };
chan W33_W29 = [1] of { int };
chan W3_W23 = [1] of { int };
chan W_W18 = [1] of { int };
chan W_W2 = [1] of { int };
chan W35_W33 = [1] of { int };
chan W9_W29 = [1] of { int };
chan W5_W25 = [1] of { int };
chan W_W34 = [1] of { int };
chan W16_W36 = [1] of { int };
chan W10_W9 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W19_W39 = [1] of { int };
chan W_W19 = [1] of { int };
chan W30_W29 = [1] of { int };
chan W32_W31 = [1] of { int };
chan W_W31 = [1] of { int };
chan W12_W11 = [1] of { int };
chan W26_W25 = [1] of { int };
chan W11_W31 = [1] of { int };
chan W_W35 = [1] of { int };
chan W24_W4 = [1] of { int };
chan W28_W27 = [1] of { int };
chan W7_W5 = [1] of { int };
chan W38_W37 = [1] of { int };
chan W40_W20 = [1] of { int };
chan W8_W28 = [1] of { int };
chan W_W15 = [1] of { int };
chan W27_W7 = [1] of { int };
chan W31_W11 = [1] of { int };
chan W6_W26 = [1] of { int };
chan W37_W17 = [1] of { int };
chan W1_W21 = [1] of { int };
chan W22_W21 = [1] of { int };
chan W7_W27 = [1] of { int };
chan W30_W10 = [1] of { int };
chan W_W11 = [1] of { int };
chan W14_W34 = [1] of { int };
chan W21_W1 = [1] of { int };
chan W_IO = [1] of { int };
chan W_W8 = [1] of { int };
chan W23_W3 = [1] of { int };
chan W6_W5 = [1] of { int };
chan W_W17 = [1] of { int };
chan W31_W29 = [1] of { int };
chan W_W3 = [1] of { int };
chan W_W37 = [1] of { int };
chan W18_W17 = [1] of { int };
chan W_W4 = [1] of { int };
chan W40_W39 = [1] of { int };
chan W38_W18 = [1] of { int };
chan W33_W13 = [1] of { int };
chan W_W29 = [1] of { int };
chan W4_W24 = [1] of { int };
chan W12_W32 = [1] of { int };
chan W8_W7 = [1] of { int };
chan W_W25 = [1] of { int };
chan W36_W16 = [1] of { int };
chan W_W39 = [1] of { int };
chan IO_W = [1] of { int };
chan W1_W = [1] of { int };
chan W20_W40 = [1] of { int };
chan W_W32 = [1] of { int };
chan W_W10 = [1] of { int };
chan W2_W22 = [1] of { int };
chan W_W30 = [1] of { int };
chan W5_W1 = [1] of { int };
chan W29_W9 = [1] of { int };
chan W17_W37 = [1] of { int };
chan W32_W12 = [1] of { int };
chan W_W12 = [1] of { int };
chan W_W6 = [1] of { int };
chan W34_W14 = [1] of { int };
chan W_W14 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W_W9 = [1] of { int };
chan W39_W19 = [1] of { int };
chan W9_W1 = [1] of { int };
chan W15_W35 = [1] of { int };
chan W_W7 = [1] of { int };
chan W11_W9 = [1] of { int };
chan W24_W23 = [1] of { int };
chan W_W40 = [1] of { int };
chan W4_W3 = [1] of { int };
chan W37_W21 = [1] of { int };
chan W_W13 = [1] of { int };
chan W_W16 = [1] of { int };
chan W_W20 = [1] of { int };
chan W17_W1 = [1] of { int };
chan W29_W21 = [1] of { int };
chan W15_W13 = [1] of { int };
chan W13_W9 = [1] of { int };
chan W26_W6 = [1] of { int };
chan W23_W21 = [1] of { int };
chan W_W26 = [1] of { int };
chan W_W36 = [1] of { int };

init
{
	run IO();
	run W();
	run W1();
	run W10();
	run W11();
	run W12();
	run W13();
	run W14();
	run W15();
	run W16();
	run W17();
	run W18();
	run W19();
	run W2();
	run W20();
	run W21();
	run W22();
	run W23();
	run W24();
	run W25();
	run W26();
	run W27();
	run W28();
	run W29();
	run W3();
	run W30();
	run W31();
	run W32();
	run W33();
	run W34();
	run W35();
	run W36();
	run W37();
	run W38();
	run W39();
	run W4();
	run W40();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype IO()
{
state1:
	if
	:: IO_W!1 -> goto state2
	fi
state2:
	if
	:: W_IO?2 -> goto state3
	fi
state3:
	skip
} /* IO */

proctype W()
{
state1:
	if
	:: IO_W?1 -> goto state2
	fi
state2:
	if
	:: W_W1!3 -> goto state3
	fi
state3:
	if
	:: W_W2!3 -> goto state4
	fi
state4:
	if
	:: W_W3!3 -> goto state5
	fi
state5:
	if
	:: W_W4!3 -> goto state6
	fi
state6:
	if
	:: W_W5!3 -> goto state7
	fi
state7:
	if
	:: W_W6!3 -> goto state8
	fi
state8:
	if
	:: W_W7!3 -> goto state9
	fi
state9:
	if
	:: W_W8!3 -> goto state10
	fi
state10:
	if
	:: W_W9!3 -> goto state11
	fi
state11:
	if
	:: W_W10!3 -> goto state12
	fi
state12:
	if
	:: W_W11!3 -> goto state13
	fi
state13:
	if
	:: W_W12!3 -> goto state14
	fi
state14:
	if
	:: W_W13!3 -> goto state15
	fi
state15:
	if
	:: W_W14!3 -> goto state16
	fi
state16:
	if
	:: W_W15!3 -> goto state17
	fi
state17:
	if
	:: W_W16!3 -> goto state18
	fi
state18:
	if
	:: W_W17!3 -> goto state19
	fi
state19:
	if
	:: W_W18!3 -> goto state20
	fi
state20:
	if
	:: W_W19!3 -> goto state21
	fi
state21:
	if
	:: W_W20!3 -> goto state22
	fi
state22:
	if
	:: W_W21!3 -> goto state23
	fi
state23:
	if
	:: W_W22!3 -> goto state24
	fi
state24:
	if
	:: W_W23!3 -> goto state25
	fi
state25:
	if
	:: W_W24!3 -> goto state26
	fi
state26:
	if
	:: W_W25!3 -> goto state27
	fi
state27:
	if
	:: W_W26!3 -> goto state28
	fi
state28:
	if
	:: W_W27!3 -> goto state29
	fi
state29:
	if
	:: W_W28!3 -> goto state30
	fi
state30:
	if
	:: W_W29!3 -> goto state31
	fi
state31:
	if
	:: W_W30!3 -> goto state32
	fi
state32:
	if
	:: W_W31!3 -> goto state33
	fi
state33:
	if
	:: W_W32!3 -> goto state34
	fi
state34:
	if
	:: W_W33!3 -> goto state35
	fi
state35:
	if
	:: W_W34!3 -> goto state36
	fi
state36:
	if
	:: W_W35!3 -> goto state37
	fi
state37:
	if
	:: W_W36!3 -> goto state38
	fi
state38:
	if
	:: W_W37!3 -> goto state39
	fi
state39:
	if
	:: W_W38!3 -> goto state40
	fi
state40:
	if
	:: W_W39!3 -> goto state41
	fi
state41:
	if
	:: W_W40!3 -> goto state42
	fi
state42:
	if
	:: W1_W?4 -> goto state43
	fi
state43:
	if
	:: W21_W?5 -> goto state44
	fi
state44:
	if
	:: W_IO!2 -> goto state45
	fi
state45:
	skip
} /* W */

proctype W1()
{
state1:
	if
	:: W_W1?3 -> goto state2
	fi
state2:
	if
	:: W1_W21!6 -> goto state3
	fi
state3:
	if
	:: W21_W1?7 -> goto state4
	fi
state4:
	if
	:: W2_W1?7 -> goto state5
	fi
state5:
	if
	:: W3_W1?7 -> goto state6
	fi
state6:
	if
	:: W5_W1?7 -> goto state7
	fi
state7:
	if
	:: W9_W1?7 -> goto state8
	fi
state8:
	if
	:: W17_W1?7 -> goto state9
	fi
state9:
	if
	:: W1_W!4 -> goto state10
	fi
state10:
	skip
} /* W1 */

proctype W10()
{
state1:
	if
	:: W_W10?3 -> goto state2
	fi
state2:
	if
	:: W10_W30!6 -> goto state3
	fi
state3:
	if
	:: W30_W10?7 -> goto state4
	fi
state4:
	if
	:: W10_W9!7 -> goto state5
	fi
state5:
	skip
} /* W10 */

proctype W11()
{
state1:
	if
	:: W_W11?3 -> goto state2
	fi
state2:
	if
	:: W11_W31!6 -> goto state3
	fi
state3:
	if
	:: W31_W11?7 -> goto state4
	fi
state4:
	if
	:: W12_W11?7 -> goto state5
	fi
state5:
	if
	:: W11_W9!7 -> goto state6
	fi
state6:
	skip
} /* W11 */

proctype W12()
{
state1:
	if
	:: W_W12?3 -> goto state2
	fi
state2:
	if
	:: W12_W32!6 -> goto state3
	fi
state3:
	if
	:: W32_W12?7 -> goto state4
	fi
state4:
	if
	:: W12_W11!7 -> goto state5
	fi
state5:
	skip
} /* W12 */

proctype W13()
{
state1:
	if
	:: W_W13?3 -> goto state2
	fi
state2:
	if
	:: W13_W33!6 -> goto state3
	fi
state3:
	if
	:: W33_W13?7 -> goto state4
	fi
state4:
	if
	:: W14_W13?7 -> goto state5
	fi
state5:
	if
	:: W15_W13?7 -> goto state6
	fi
state6:
	if
	:: W13_W9!7 -> goto state7
	fi
state7:
	skip
} /* W13 */

proctype W14()
{
state1:
	if
	:: W_W14?3 -> goto state2
	fi
state2:
	if
	:: W14_W34!6 -> goto state3
	fi
state3:
	if
	:: W34_W14?7 -> goto state4
	fi
state4:
	if
	:: W14_W13!7 -> goto state5
	fi
state5:
	skip
} /* W14 */

proctype W15()
{
state1:
	if
	:: W_W15?3 -> goto state2
	fi
state2:
	if
	:: W15_W35!6 -> goto state3
	fi
state3:
	if
	:: W35_W15?7 -> goto state4
	fi
state4:
	if
	:: W16_W15?7 -> goto state5
	fi
state5:
	if
	:: W15_W13!7 -> goto state6
	fi
state6:
	skip
} /* W15 */

proctype W16()
{
state1:
	if
	:: W_W16?3 -> goto state2
	fi
state2:
	if
	:: W16_W36!6 -> goto state3
	fi
state3:
	if
	:: W36_W16?7 -> goto state4
	fi
state4:
	if
	:: W16_W15!7 -> goto state5
	fi
state5:
	skip
} /* W16 */

proctype W17()
{
state1:
	if
	:: W_W17?3 -> goto state2
	fi
state2:
	if
	:: W17_W37!6 -> goto state3
	fi
state3:
	if
	:: W37_W17?7 -> goto state4
	fi
state4:
	if
	:: W18_W17?7 -> goto state5
	fi
state5:
	if
	:: W19_W17?7 -> goto state6
	fi
state6:
	if
	:: W17_W1!7 -> goto state7
	fi
state7:
	skip
} /* W17 */

proctype W18()
{
state1:
	if
	:: W_W18?3 -> goto state2
	fi
state2:
	if
	:: W18_W38!6 -> goto state3
	fi
state3:
	if
	:: W38_W18?7 -> goto state4
	fi
state4:
	if
	:: W18_W17!7 -> goto state5
	fi
state5:
	skip
} /* W18 */

proctype W19()
{
state1:
	if
	:: W_W19?3 -> goto state2
	fi
state2:
	if
	:: W19_W39!6 -> goto state3
	fi
state3:
	if
	:: W39_W19?7 -> goto state4
	fi
state4:
	if
	:: W20_W19?7 -> goto state5
	fi
state5:
	if
	:: W19_W17!7 -> goto state6
	fi
state6:
	skip
} /* W19 */

proctype W2()
{
state1:
	if
	:: W_W2?3 -> goto state2
	fi
state2:
	if
	:: W2_W22!6 -> goto state3
	fi
state3:
	if
	:: W22_W2?7 -> goto state4
	fi
state4:
	if
	:: W2_W1!7 -> goto state5
	fi
state5:
	skip
} /* W2 */

proctype W20()
{
state1:
	if
	:: W_W20?3 -> goto state2
	fi
state2:
	if
	:: W20_W40!6 -> goto state3
	fi
state3:
	if
	:: W40_W20?7 -> goto state4
	fi
state4:
	if
	:: W20_W19!7 -> goto state5
	fi
state5:
	skip
} /* W20 */

proctype W21()
{
state1:
	if
	:: W_W21?3 -> goto state2
	fi
state2:
	if
	:: W1_W21?6 -> goto state3
	fi
state3:
	if
	:: W21_W1!7 -> goto state4
	fi
state4:
	if
	:: W22_W21?6 -> goto state5
	fi
state5:
	if
	:: W23_W21?6 -> goto state6
	fi
state6:
	if
	:: W25_W21?6 -> goto state7
	fi
state7:
	if
	:: W29_W21?6 -> goto state8
	fi
state8:
	if
	:: W37_W21?6 -> goto state9
	fi
state9:
	if
	:: W21_W!5 -> goto state10
	fi
state10:
	skip
} /* W21 */

proctype W22()
{
state1:
	if
	:: W_W22?3 -> goto state2
	fi
state2:
	if
	:: W2_W22?6 -> goto state3
	fi
state3:
	if
	:: W22_W2!7 -> goto state4
	fi
state4:
	if
	:: W22_W21!6 -> goto state5
	fi
state5:
	skip
} /* W22 */

proctype W23()
{
state1:
	if
	:: W_W23?3 -> goto state2
	fi
state2:
	if
	:: W3_W23?6 -> goto state3
	fi
state3:
	if
	:: W23_W3!7 -> goto state4
	fi
state4:
	if
	:: W24_W23?6 -> goto state5
	fi
state5:
	if
	:: W23_W21!6 -> goto state6
	fi
state6:
	skip
} /* W23 */

proctype W24()
{
state1:
	if
	:: W_W24?3 -> goto state2
	fi
state2:
	if
	:: W4_W24?6 -> goto state3
	fi
state3:
	if
	:: W24_W4!7 -> goto state4
	fi
state4:
	if
	:: W24_W23!6 -> goto state5
	fi
state5:
	skip
} /* W24 */

proctype W25()
{
state1:
	if
	:: W_W25?3 -> goto state2
	fi
state2:
	if
	:: W5_W25?6 -> goto state3
	fi
state3:
	if
	:: W25_W5!7 -> goto state4
	fi
state4:
	if
	:: W26_W25?6 -> goto state5
	fi
state5:
	if
	:: W27_W25?6 -> goto state6
	fi
state6:
	if
	:: W25_W21!6 -> goto state7
	fi
state7:
	skip
} /* W25 */

proctype W26()
{
state1:
	if
	:: W_W26?3 -> goto state2
	fi
state2:
	if
	:: W6_W26?6 -> goto state3
	fi
state3:
	if
	:: W26_W6!7 -> goto state4
	fi
state4:
	if
	:: W26_W25!6 -> goto state5
	fi
state5:
	skip
} /* W26 */

proctype W27()
{
state1:
	if
	:: W_W27?3 -> goto state2
	fi
state2:
	if
	:: W7_W27?6 -> goto state3
	fi
state3:
	if
	:: W27_W7!7 -> goto state4
	fi
state4:
	if
	:: W28_W27?6 -> goto state5
	fi
state5:
	if
	:: W27_W25!6 -> goto state6
	fi
state6:
	skip
} /* W27 */

proctype W28()
{
state1:
	if
	:: W_W28?3 -> goto state2
	fi
state2:
	if
	:: W8_W28?6 -> goto state3
	fi
state3:
	if
	:: W28_W8!7 -> goto state4
	fi
state4:
	if
	:: W28_W27!6 -> goto state5
	fi
state5:
	skip
} /* W28 */

proctype W29()
{
state1:
	if
	:: W_W29?3 -> goto state2
	fi
state2:
	if
	:: W9_W29?6 -> goto state3
	fi
state3:
	if
	:: W29_W9!7 -> goto state4
	fi
state4:
	if
	:: W30_W29?6 -> goto state5
	fi
state5:
	if
	:: W31_W29?6 -> goto state6
	fi
state6:
	if
	:: W33_W29?6 -> goto state7
	fi
state7:
	if
	:: W29_W21!6 -> goto state8
	fi
state8:
	skip
} /* W29 */

proctype W3()
{
state1:
	if
	:: W_W3?3 -> goto state2
	fi
state2:
	if
	:: W3_W23!6 -> goto state3
	fi
state3:
	if
	:: W23_W3?7 -> goto state4
	fi
state4:
	if
	:: W4_W3?7 -> goto state5
	fi
state5:
	if
	:: W3_W1!7 -> goto state6
	fi
state6:
	skip
} /* W3 */

proctype W30()
{
state1:
	if
	:: W_W30?3 -> goto state2
	fi
state2:
	if
	:: W10_W30?6 -> goto state3
	fi
state3:
	if
	:: W30_W10!7 -> goto state4
	fi
state4:
	if
	:: W30_W29!6 -> goto state5
	fi
state5:
	skip
} /* W30 */

proctype W31()
{
state1:
	if
	:: W_W31?3 -> goto state2
	fi
state2:
	if
	:: W11_W31?6 -> goto state3
	fi
state3:
	if
	:: W31_W11!7 -> goto state4
	fi
state4:
	if
	:: W32_W31?6 -> goto state5
	fi
state5:
	if
	:: W31_W29!6 -> goto state6
	fi
state6:
	skip
} /* W31 */

proctype W32()
{
state1:
	if
	:: W_W32?3 -> goto state2
	fi
state2:
	if
	:: W12_W32?6 -> goto state3
	fi
state3:
	if
	:: W32_W12!7 -> goto state4
	fi
state4:
	if
	:: W32_W31!6 -> goto state5
	fi
state5:
	skip
} /* W32 */

proctype W33()
{
state1:
	if
	:: W_W33?3 -> goto state2
	fi
state2:
	if
	:: W13_W33?6 -> goto state3
	fi
state3:
	if
	:: W33_W13!7 -> goto state4
	fi
state4:
	if
	:: W34_W33?6 -> goto state5
	fi
state5:
	if
	:: W35_W33?6 -> goto state6
	fi
state6:
	if
	:: W33_W29!6 -> goto state7
	fi
state7:
	skip
} /* W33 */

proctype W34()
{
state1:
	if
	:: W_W34?3 -> goto state2
	fi
state2:
	if
	:: W14_W34?6 -> goto state3
	fi
state3:
	if
	:: W34_W14!7 -> goto state4
	fi
state4:
	if
	:: W34_W33!6 -> goto state5
	fi
state5:
	skip
} /* W34 */

proctype W35()
{
state1:
	if
	:: W_W35?3 -> goto state2
	fi
state2:
	if
	:: W15_W35?6 -> goto state3
	fi
state3:
	if
	:: W35_W15!7 -> goto state4
	fi
state4:
	if
	:: W36_W35?6 -> goto state5
	fi
state5:
	if
	:: W35_W33!6 -> goto state6
	fi
state6:
	skip
} /* W35 */

proctype W36()
{
state1:
	if
	:: W_W36?3 -> goto state2
	fi
state2:
	if
	:: W16_W36?6 -> goto state3
	fi
state3:
	if
	:: W36_W16!7 -> goto state4
	fi
state4:
	if
	:: W36_W35!6 -> goto state5
	fi
state5:
	skip
} /* W36 */

proctype W37()
{
state1:
	if
	:: W_W37?3 -> goto state2
	fi
state2:
	if
	:: W17_W37?6 -> goto state3
	fi
state3:
	if
	:: W37_W17!7 -> goto state4
	fi
state4:
	if
	:: W38_W37?6 -> goto state5
	fi
state5:
	if
	:: W39_W37?6 -> goto state6
	fi
state6:
	if
	:: W37_W21!6 -> goto state7
	fi
state7:
	skip
} /* W37 */

proctype W38()
{
state1:
	if
	:: W_W38?3 -> goto state2
	fi
state2:
	if
	:: W18_W38?6 -> goto state3
	fi
state3:
	if
	:: W38_W18!7 -> goto state4
	fi
state4:
	if
	:: W38_W37!6 -> goto state5
	fi
state5:
	skip
} /* W38 */

proctype W39()
{
state1:
	if
	:: W_W39?3 -> goto state2
	fi
state2:
	if
	:: W19_W39?6 -> goto state3
	fi
state3:
	if
	:: W39_W19!7 -> goto state4
	fi
state4:
	if
	:: W40_W39?6 -> goto state5
	fi
state5:
	if
	:: W39_W37!6 -> goto state6
	fi
state6:
	skip
} /* W39 */

proctype W4()
{
state1:
	if
	:: W_W4?3 -> goto state2
	fi
state2:
	if
	:: W4_W24!6 -> goto state3
	fi
state3:
	if
	:: W24_W4?7 -> goto state4
	fi
state4:
	if
	:: W4_W3!7 -> goto state5
	fi
state5:
	skip
} /* W4 */

proctype W40()
{
state1:
	if
	:: W_W40?3 -> goto state2
	fi
state2:
	if
	:: W20_W40?6 -> goto state3
	fi
state3:
	if
	:: W40_W20!7 -> goto state4
	fi
state4:
	if
	:: W40_W39!6 -> goto state5
	fi
state5:
	skip
} /* W40 */

proctype W5()
{
state1:
	if
	:: W_W5?3 -> goto state2
	fi
state2:
	if
	:: W5_W25!6 -> goto state3
	fi
state3:
	if
	:: W25_W5?7 -> goto state4
	fi
state4:
	if
	:: W6_W5?7 -> goto state5
	fi
state5:
	if
	:: W7_W5?7 -> goto state6
	fi
state6:
	if
	:: W5_W1!7 -> goto state7
	fi
state7:
	skip
} /* W5 */

proctype W6()
{
state1:
	if
	:: W_W6?3 -> goto state2
	fi
state2:
	if
	:: W6_W26!6 -> goto state3
	fi
state3:
	if
	:: W26_W6?7 -> goto state4
	fi
state4:
	if
	:: W6_W5!7 -> goto state5
	fi
state5:
	skip
} /* W6 */

proctype W7()
{
state1:
	if
	:: W_W7?3 -> goto state2
	fi
state2:
	if
	:: W7_W27!6 -> goto state3
	fi
state3:
	if
	:: W27_W7?7 -> goto state4
	fi
state4:
	if
	:: W8_W7?7 -> goto state5
	fi
state5:
	if
	:: W7_W5!7 -> goto state6
	fi
state6:
	skip
} /* W7 */

proctype W8()
{
state1:
	if
	:: W_W8?3 -> goto state2
	fi
state2:
	if
	:: W8_W28!6 -> goto state3
	fi
state3:
	if
	:: W28_W8?7 -> goto state4
	fi
state4:
	if
	:: W8_W7!7 -> goto state5
	fi
state5:
	skip
} /* W8 */

proctype W9()
{
state1:
	if
	:: W_W9?3 -> goto state2
	fi
state2:
	if
	:: W9_W29!6 -> goto state3
	fi
state3:
	if
	:: W29_W9?7 -> goto state4
	fi
state4:
	if
	:: W10_W9?7 -> goto state5
	fi
state5:
	if
	:: W11_W9?7 -> goto state6
	fi
state6:
	if
	:: W13_W9?7 -> goto state7
	fi
state7:
	if
	:: W9_W1!7 -> goto state8
	fi
state8:
	skip
} /* W9 */
