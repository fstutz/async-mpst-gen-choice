chan W_W4 = [1] of { int };
chan W16_W14 = [1] of { int };
chan W_W5 = [1] of { int };
chan W10_W1 = [1] of { int };
chan W12_W10 = [1] of { int };
chan W1_W10 = [1] of { int };
chan W2_W11 = [1] of { int };
chan W16_W7 = [1] of { int };
chan W8_W7 = [1] of { int };
chan W13_W4 = [1] of { int };
chan W_W1 = [1] of { int };
chan W8_W17 = [1] of { int };
chan IO_W = [1] of { int };
chan W17_W16 = [1] of { int };
chan W7_W16 = [1] of { int };
chan W1_W = [1] of { int };
chan W_W18 = [1] of { int };
chan W_W2 = [1] of { int };
chan W11_W2 = [1] of { int };
chan W_W10 = [1] of { int };
chan W11_W10 = [1] of { int };
chan W10_W = [1] of { int };
chan W17_W8 = [1] of { int };
chan W14_W5 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W18_W10 = [1] of { int };
chan W6_W15 = [1] of { int };
chan W5_W1 = [1] of { int };
chan W_W12 = [1] of { int };
chan W_W6 = [1] of { int };
chan W7_W5 = [1] of { int };
chan W4_W13 = [1] of { int };
chan W_W14 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W_W9 = [1] of { int };
chan W9_W1 = [1] of { int };
chan W_W15 = [1] of { int };
chan W_W7 = [1] of { int };
chan W15_W6 = [1] of { int };
chan W3_W12 = [1] of { int };
chan W12_W3 = [1] of { int };
chan W14_W10 = [1] of { int };
chan W9_W18 = [1] of { int };
chan W4_W3 = [1] of { int };
chan W18_W9 = [1] of { int };
chan W13_W12 = [1] of { int };
chan W15_W14 = [1] of { int };
chan W_W13 = [1] of { int };
chan W_W11 = [1] of { int };
chan W_W16 = [1] of { int };
chan W5_W14 = [1] of { int };
chan W_IO = [1] of { int };
chan W_W8 = [1] of { int };
chan W6_W5 = [1] of { int };
chan W_W17 = [1] of { int };
chan W_W3 = [1] of { int };

init
{
	run IO();
	run W();
	run W1();
	run W10();
	run W11();
	run W12();
	run W13();
	run W14();
	run W15();
	run W16();
	run W17();
	run W18();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype IO()
{
state1:
	if
	:: IO_W!1 -> goto state2
	fi
state2:
	if
	:: W_IO?2 -> goto state3
	fi
state3:
	skip
} /* IO */

proctype W()
{
state1:
	if
	:: IO_W?1 -> goto state2
	fi
state2:
	if
	:: W_W1!3 -> goto state3
	fi
state3:
	if
	:: W_W2!3 -> goto state4
	fi
state4:
	if
	:: W_W3!3 -> goto state5
	fi
state5:
	if
	:: W_W4!3 -> goto state6
	fi
state6:
	if
	:: W_W5!3 -> goto state7
	fi
state7:
	if
	:: W_W6!3 -> goto state8
	fi
state8:
	if
	:: W_W7!3 -> goto state9
	fi
state9:
	if
	:: W_W8!3 -> goto state10
	fi
state10:
	if
	:: W_W9!3 -> goto state11
	fi
state11:
	if
	:: W_W10!3 -> goto state12
	fi
state12:
	if
	:: W_W11!3 -> goto state13
	fi
state13:
	if
	:: W_W12!3 -> goto state14
	fi
state14:
	if
	:: W_W13!3 -> goto state15
	fi
state15:
	if
	:: W_W14!3 -> goto state16
	fi
state16:
	if
	:: W_W15!3 -> goto state17
	fi
state17:
	if
	:: W_W16!3 -> goto state18
	fi
state18:
	if
	:: W_W17!3 -> goto state19
	fi
state19:
	if
	:: W_W18!3 -> goto state20
	fi
state20:
	if
	:: W1_W?4 -> goto state21
	fi
state21:
	if
	:: W10_W?5 -> goto state22
	fi
state22:
	if
	:: W_IO!2 -> goto state23
	fi
state23:
	skip
} /* W */

proctype W1()
{
state1:
	if
	:: W_W1?3 -> goto state2
	fi
state2:
	if
	:: W1_W10!6 -> goto state3
	fi
state3:
	if
	:: W10_W1?7 -> goto state4
	fi
state4:
	if
	:: W2_W1?7 -> goto state5
	fi
state5:
	if
	:: W3_W1?7 -> goto state6
	fi
state6:
	if
	:: W5_W1?7 -> goto state7
	fi
state7:
	if
	:: W9_W1?7 -> goto state8
	fi
state8:
	if
	:: W1_W!4 -> goto state9
	fi
state9:
	skip
} /* W1 */

proctype W10()
{
state1:
	if
	:: W_W10?3 -> goto state2
	fi
state2:
	if
	:: W1_W10?6 -> goto state3
	fi
state3:
	if
	:: W10_W1!7 -> goto state4
	fi
state4:
	if
	:: W11_W10?6 -> goto state5
	fi
state5:
	if
	:: W12_W10?6 -> goto state6
	fi
state6:
	if
	:: W14_W10?6 -> goto state7
	fi
state7:
	if
	:: W18_W10?6 -> goto state8
	fi
state8:
	if
	:: W10_W!5 -> goto state9
	fi
state9:
	skip
} /* W10 */

proctype W11()
{
state1:
	if
	:: W_W11?3 -> goto state2
	fi
state2:
	if
	:: W2_W11?6 -> goto state3
	fi
state3:
	if
	:: W11_W2!7 -> goto state4
	fi
state4:
	if
	:: W11_W10!6 -> goto state5
	fi
state5:
	skip
} /* W11 */

proctype W12()
{
state1:
	if
	:: W_W12?3 -> goto state2
	fi
state2:
	if
	:: W3_W12?6 -> goto state3
	fi
state3:
	if
	:: W12_W3!7 -> goto state4
	fi
state4:
	if
	:: W13_W12?6 -> goto state5
	fi
state5:
	if
	:: W12_W10!6 -> goto state6
	fi
state6:
	skip
} /* W12 */

proctype W13()
{
state1:
	if
	:: W_W13?3 -> goto state2
	fi
state2:
	if
	:: W4_W13?6 -> goto state3
	fi
state3:
	if
	:: W13_W4!7 -> goto state4
	fi
state4:
	if
	:: W13_W12!6 -> goto state5
	fi
state5:
	skip
} /* W13 */

proctype W14()
{
state1:
	if
	:: W_W14?3 -> goto state2
	fi
state2:
	if
	:: W5_W14?6 -> goto state3
	fi
state3:
	if
	:: W14_W5!7 -> goto state4
	fi
state4:
	if
	:: W15_W14?6 -> goto state5
	fi
state5:
	if
	:: W16_W14?6 -> goto state6
	fi
state6:
	if
	:: W14_W10!6 -> goto state7
	fi
state7:
	skip
} /* W14 */

proctype W15()
{
state1:
	if
	:: W_W15?3 -> goto state2
	fi
state2:
	if
	:: W6_W15?6 -> goto state3
	fi
state3:
	if
	:: W15_W6!7 -> goto state4
	fi
state4:
	if
	:: W15_W14!6 -> goto state5
	fi
state5:
	skip
} /* W15 */

proctype W16()
{
state1:
	if
	:: W_W16?3 -> goto state2
	fi
state2:
	if
	:: W7_W16?6 -> goto state3
	fi
state3:
	if
	:: W16_W7!7 -> goto state4
	fi
state4:
	if
	:: W17_W16?6 -> goto state5
	fi
state5:
	if
	:: W16_W14!6 -> goto state6
	fi
state6:
	skip
} /* W16 */

proctype W17()
{
state1:
	if
	:: W_W17?3 -> goto state2
	fi
state2:
	if
	:: W8_W17?6 -> goto state3
	fi
state3:
	if
	:: W17_W8!7 -> goto state4
	fi
state4:
	if
	:: W17_W16!6 -> goto state5
	fi
state5:
	skip
} /* W17 */

proctype W18()
{
state1:
	if
	:: W_W18?3 -> goto state2
	fi
state2:
	if
	:: W9_W18?6 -> goto state3
	fi
state3:
	if
	:: W18_W9!7 -> goto state4
	fi
state4:
	if
	:: W18_W10!6 -> goto state5
	fi
state5:
	skip
} /* W18 */

proctype W2()
{
state1:
	if
	:: W_W2?3 -> goto state2
	fi
state2:
	if
	:: W2_W11!6 -> goto state3
	fi
state3:
	if
	:: W11_W2?7 -> goto state4
	fi
state4:
	if
	:: W2_W1!7 -> goto state5
	fi
state5:
	skip
} /* W2 */

proctype W3()
{
state1:
	if
	:: W_W3?3 -> goto state2
	fi
state2:
	if
	:: W3_W12!6 -> goto state3
	fi
state3:
	if
	:: W12_W3?7 -> goto state4
	fi
state4:
	if
	:: W4_W3?7 -> goto state5
	fi
state5:
	if
	:: W3_W1!7 -> goto state6
	fi
state6:
	skip
} /* W3 */

proctype W4()
{
state1:
	if
	:: W_W4?3 -> goto state2
	fi
state2:
	if
	:: W4_W13!6 -> goto state3
	fi
state3:
	if
	:: W13_W4?7 -> goto state4
	fi
state4:
	if
	:: W4_W3!7 -> goto state5
	fi
state5:
	skip
} /* W4 */

proctype W5()
{
state1:
	if
	:: W_W5?3 -> goto state2
	fi
state2:
	if
	:: W5_W14!6 -> goto state3
	fi
state3:
	if
	:: W14_W5?7 -> goto state4
	fi
state4:
	if
	:: W6_W5?7 -> goto state5
	fi
state5:
	if
	:: W7_W5?7 -> goto state6
	fi
state6:
	if
	:: W5_W1!7 -> goto state7
	fi
state7:
	skip
} /* W5 */

proctype W6()
{
state1:
	if
	:: W_W6?3 -> goto state2
	fi
state2:
	if
	:: W6_W15!6 -> goto state3
	fi
state3:
	if
	:: W15_W6?7 -> goto state4
	fi
state4:
	if
	:: W6_W5!7 -> goto state5
	fi
state5:
	skip
} /* W6 */

proctype W7()
{
state1:
	if
	:: W_W7?3 -> goto state2
	fi
state2:
	if
	:: W7_W16!6 -> goto state3
	fi
state3:
	if
	:: W16_W7?7 -> goto state4
	fi
state4:
	if
	:: W8_W7?7 -> goto state5
	fi
state5:
	if
	:: W7_W5!7 -> goto state6
	fi
state6:
	skip
} /* W7 */

proctype W8()
{
state1:
	if
	:: W_W8?3 -> goto state2
	fi
state2:
	if
	:: W8_W17!6 -> goto state3
	fi
state3:
	if
	:: W17_W8?7 -> goto state4
	fi
state4:
	if
	:: W8_W7!7 -> goto state5
	fi
state5:
	skip
} /* W8 */

proctype W9()
{
state1:
	if
	:: W_W9?3 -> goto state2
	fi
state2:
	if
	:: W9_W18!6 -> goto state3
	fi
state3:
	if
	:: W18_W9?7 -> goto state4
	fi
state4:
	if
	:: W9_W1!7 -> goto state5
	fi
state5:
	skip
} /* W9 */
