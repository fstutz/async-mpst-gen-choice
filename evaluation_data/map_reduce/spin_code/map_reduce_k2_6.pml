chan W_W4 = [1] of { int };
chan W9_W7 = [1] of { int };
chan W_W5 = [1] of { int };
chan W11_W5 = [1] of { int };
chan W8_W7 = [1] of { int };
chan W4_W10 = [1] of { int };
chan W7_W = [1] of { int };
chan W2_W8 = [1] of { int };
chan W_W1 = [1] of { int };
chan W9_W3 = [1] of { int };
chan IO_W = [1] of { int };
chan W12_W6 = [1] of { int };
chan W1_W = [1] of { int };
chan W_W2 = [1] of { int };
chan W_W10 = [1] of { int };
chan W6_W12 = [1] of { int };
chan W10_W9 = [1] of { int };
chan W1_W7 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W5_W1 = [1] of { int };
chan W12_W11 = [1] of { int };
chan W_W12 = [1] of { int };
chan W_W6 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W_W9 = [1] of { int };
chan W_W7 = [1] of { int };
chan W11_W7 = [1] of { int };
chan W8_W2 = [1] of { int };
chan W4_W3 = [1] of { int };
chan W3_W9 = [1] of { int };
chan W_W11 = [1] of { int };
chan W10_W4 = [1] of { int };
chan W7_W1 = [1] of { int };
chan W_IO = [1] of { int };
chan W_W8 = [1] of { int };
chan W6_W5 = [1] of { int };
chan W_W3 = [1] of { int };
chan W5_W11 = [1] of { int };

init
{
	run IO();
	run W();
	run W1();
	run W10();
	run W11();
	run W12();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype IO()
{
state1:
	if
	:: IO_W!1 -> goto state2
	fi
state2:
	if
	:: W_IO?2 -> goto state3
	fi
state3:
	skip
} /* IO */

proctype W()
{
state1:
	if
	:: IO_W?1 -> goto state2
	fi
state2:
	if
	:: W_W1!3 -> goto state3
	fi
state3:
	if
	:: W_W2!3 -> goto state4
	fi
state4:
	if
	:: W_W3!3 -> goto state5
	fi
state5:
	if
	:: W_W4!3 -> goto state6
	fi
state6:
	if
	:: W_W5!3 -> goto state7
	fi
state7:
	if
	:: W_W6!3 -> goto state8
	fi
state8:
	if
	:: W_W7!3 -> goto state9
	fi
state9:
	if
	:: W_W8!3 -> goto state10
	fi
state10:
	if
	:: W_W9!3 -> goto state11
	fi
state11:
	if
	:: W_W10!3 -> goto state12
	fi
state12:
	if
	:: W_W11!3 -> goto state13
	fi
state13:
	if
	:: W_W12!3 -> goto state14
	fi
state14:
	if
	:: W1_W?4 -> goto state15
	fi
state15:
	if
	:: W7_W?5 -> goto state16
	fi
state16:
	if
	:: W_IO!2 -> goto state17
	fi
state17:
	skip
} /* W */

proctype W1()
{
state1:
	if
	:: W_W1?3 -> goto state2
	fi
state2:
	if
	:: W1_W7!6 -> goto state3
	fi
state3:
	if
	:: W7_W1?7 -> goto state4
	fi
state4:
	if
	:: W2_W1?7 -> goto state5
	fi
state5:
	if
	:: W3_W1?7 -> goto state6
	fi
state6:
	if
	:: W5_W1?7 -> goto state7
	fi
state7:
	if
	:: W1_W!4 -> goto state8
	fi
state8:
	skip
} /* W1 */

proctype W10()
{
state1:
	if
	:: W_W10?3 -> goto state2
	fi
state2:
	if
	:: W4_W10?6 -> goto state3
	fi
state3:
	if
	:: W10_W4!7 -> goto state4
	fi
state4:
	if
	:: W10_W9!6 -> goto state5
	fi
state5:
	skip
} /* W10 */

proctype W11()
{
state1:
	if
	:: W_W11?3 -> goto state2
	fi
state2:
	if
	:: W5_W11?6 -> goto state3
	fi
state3:
	if
	:: W11_W5!7 -> goto state4
	fi
state4:
	if
	:: W12_W11?6 -> goto state5
	fi
state5:
	if
	:: W11_W7!6 -> goto state6
	fi
state6:
	skip
} /* W11 */

proctype W12()
{
state1:
	if
	:: W_W12?3 -> goto state2
	fi
state2:
	if
	:: W6_W12?6 -> goto state3
	fi
state3:
	if
	:: W12_W6!7 -> goto state4
	fi
state4:
	if
	:: W12_W11!6 -> goto state5
	fi
state5:
	skip
} /* W12 */

proctype W2()
{
state1:
	if
	:: W_W2?3 -> goto state2
	fi
state2:
	if
	:: W2_W8!6 -> goto state3
	fi
state3:
	if
	:: W8_W2?7 -> goto state4
	fi
state4:
	if
	:: W2_W1!7 -> goto state5
	fi
state5:
	skip
} /* W2 */

proctype W3()
{
state1:
	if
	:: W_W3?3 -> goto state2
	fi
state2:
	if
	:: W3_W9!6 -> goto state3
	fi
state3:
	if
	:: W9_W3?7 -> goto state4
	fi
state4:
	if
	:: W4_W3?7 -> goto state5
	fi
state5:
	if
	:: W3_W1!7 -> goto state6
	fi
state6:
	skip
} /* W3 */

proctype W4()
{
state1:
	if
	:: W_W4?3 -> goto state2
	fi
state2:
	if
	:: W4_W10!6 -> goto state3
	fi
state3:
	if
	:: W10_W4?7 -> goto state4
	fi
state4:
	if
	:: W4_W3!7 -> goto state5
	fi
state5:
	skip
} /* W4 */

proctype W5()
{
state1:
	if
	:: W_W5?3 -> goto state2
	fi
state2:
	if
	:: W5_W11!6 -> goto state3
	fi
state3:
	if
	:: W11_W5?7 -> goto state4
	fi
state4:
	if
	:: W6_W5?7 -> goto state5
	fi
state5:
	if
	:: W5_W1!7 -> goto state6
	fi
state6:
	skip
} /* W5 */

proctype W6()
{
state1:
	if
	:: W_W6?3 -> goto state2
	fi
state2:
	if
	:: W6_W12!6 -> goto state3
	fi
state3:
	if
	:: W12_W6?7 -> goto state4
	fi
state4:
	if
	:: W6_W5!7 -> goto state5
	fi
state5:
	skip
} /* W6 */

proctype W7()
{
state1:
	if
	:: W_W7?3 -> goto state2
	fi
state2:
	if
	:: W1_W7?6 -> goto state3
	fi
state3:
	if
	:: W7_W1!7 -> goto state4
	fi
state4:
	if
	:: W8_W7?6 -> goto state5
	fi
state5:
	if
	:: W9_W7?6 -> goto state6
	fi
state6:
	if
	:: W11_W7?6 -> goto state7
	fi
state7:
	if
	:: W7_W!5 -> goto state8
	fi
state8:
	skip
} /* W7 */

proctype W8()
{
state1:
	if
	:: W_W8?3 -> goto state2
	fi
state2:
	if
	:: W2_W8?6 -> goto state3
	fi
state3:
	if
	:: W8_W2!7 -> goto state4
	fi
state4:
	if
	:: W8_W7!6 -> goto state5
	fi
state5:
	skip
} /* W8 */

proctype W9()
{
state1:
	if
	:: W_W9?3 -> goto state2
	fi
state2:
	if
	:: W3_W9?6 -> goto state3
	fi
state3:
	if
	:: W9_W3!7 -> goto state4
	fi
state4:
	if
	:: W10_W9?6 -> goto state5
	fi
state5:
	if
	:: W9_W7!6 -> goto state6
	fi
state6:
	skip
} /* W9 */
