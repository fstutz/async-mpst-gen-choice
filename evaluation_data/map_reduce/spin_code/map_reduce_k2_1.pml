chan W_W1 = [1] of { int };
chan IO_W = [1] of { int };
chan W_IO = [1] of { int };
chan W1_W2 = [1] of { int };
chan W1_W = [1] of { int };
chan W2_W1 = [1] of { int };
chan W_W2 = [1] of { int };
chan W2_W = [1] of { int };

init
{
	run IO();
	run W();
	run W1();
	run W2()
}

proctype IO()
{
state1:
	if
	:: IO_W!1 -> goto state2
	fi
state2:
	if
	:: W_IO?2 -> goto state3
	fi
state3:
	skip
} /* IO */

proctype W()
{
state1:
	if
	:: IO_W?1 -> goto state2
	fi
state2:
	if
	:: W_W1!3 -> goto state3
	fi
state3:
	if
	:: W_W2!3 -> goto state4
	fi
state4:
	if
	:: W1_W?4 -> goto state5
	fi
state5:
	if
	:: W2_W?5 -> goto state6
	fi
state6:
	if
	:: W_IO!2 -> goto state7
	fi
state7:
	skip
} /* W */

proctype W1()
{
state1:
	if
	:: W_W1?3 -> goto state2
	fi
state2:
	if
	:: W1_W2!6 -> goto state3
	fi
state3:
	if
	:: W2_W1?7 -> goto state4
	fi
state4:
	if
	:: W1_W!4 -> goto state5
	fi
state5:
	skip
} /* W1 */

proctype W2()
{
state1:
	if
	:: W_W2?3 -> goto state2
	fi
state2:
	if
	:: W1_W2?6 -> goto state3
	fi
state3:
	if
	:: W2_W1!7 -> goto state4
	fi
state4:
	if
	:: W2_W!5 -> goto state5
	fi
state5:
	skip
} /* W2 */
