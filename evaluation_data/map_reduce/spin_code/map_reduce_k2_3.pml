chan W_W4 = [1] of { int };
chan W5_W2 = [1] of { int };
chan W_W6 = [1] of { int };
chan W_W5 = [1] of { int };
chan W4_W1 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W3_W6 = [1] of { int };
chan W5_W4 = [1] of { int };
chan W6_W3 = [1] of { int };
chan W1_W4 = [1] of { int };
chan W_W1 = [1] of { int };
chan IO_W = [1] of { int };
chan W_IO = [1] of { int };
chan W1_W = [1] of { int };
chan W2_W5 = [1] of { int };
chan W_W2 = [1] of { int };
chan W4_W = [1] of { int };
chan W6_W4 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W_W3 = [1] of { int };

init
{
	run IO();
	run W();
	run W1();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6()
}

proctype IO()
{
state1:
	if
	:: IO_W!1 -> goto state2
	fi
state2:
	if
	:: W_IO?2 -> goto state3
	fi
state3:
	skip
} /* IO */

proctype W()
{
state1:
	if
	:: IO_W?1 -> goto state2
	fi
state2:
	if
	:: W_W1!3 -> goto state3
	fi
state3:
	if
	:: W_W2!3 -> goto state4
	fi
state4:
	if
	:: W_W3!3 -> goto state5
	fi
state5:
	if
	:: W_W4!3 -> goto state6
	fi
state6:
	if
	:: W_W5!3 -> goto state7
	fi
state7:
	if
	:: W_W6!3 -> goto state8
	fi
state8:
	if
	:: W1_W?4 -> goto state9
	fi
state9:
	if
	:: W4_W?5 -> goto state10
	fi
state10:
	if
	:: W_IO!2 -> goto state11
	fi
state11:
	skip
} /* W */

proctype W1()
{
state1:
	if
	:: W_W1?3 -> goto state2
	fi
state2:
	if
	:: W1_W4!6 -> goto state3
	fi
state3:
	if
	:: W4_W1?7 -> goto state4
	fi
state4:
	if
	:: W2_W1?7 -> goto state5
	fi
state5:
	if
	:: W3_W1?7 -> goto state6
	fi
state6:
	if
	:: W1_W!4 -> goto state7
	fi
state7:
	skip
} /* W1 */

proctype W2()
{
state1:
	if
	:: W_W2?3 -> goto state2
	fi
state2:
	if
	:: W2_W5!6 -> goto state3
	fi
state3:
	if
	:: W5_W2?7 -> goto state4
	fi
state4:
	if
	:: W2_W1!7 -> goto state5
	fi
state5:
	skip
} /* W2 */

proctype W3()
{
state1:
	if
	:: W_W3?3 -> goto state2
	fi
state2:
	if
	:: W3_W6!6 -> goto state3
	fi
state3:
	if
	:: W6_W3?7 -> goto state4
	fi
state4:
	if
	:: W3_W1!7 -> goto state5
	fi
state5:
	skip
} /* W3 */

proctype W4()
{
state1:
	if
	:: W_W4?3 -> goto state2
	fi
state2:
	if
	:: W1_W4?6 -> goto state3
	fi
state3:
	if
	:: W4_W1!7 -> goto state4
	fi
state4:
	if
	:: W5_W4?6 -> goto state5
	fi
state5:
	if
	:: W6_W4?6 -> goto state6
	fi
state6:
	if
	:: W4_W!5 -> goto state7
	fi
state7:
	skip
} /* W4 */

proctype W5()
{
state1:
	if
	:: W_W5?3 -> goto state2
	fi
state2:
	if
	:: W2_W5?6 -> goto state3
	fi
state3:
	if
	:: W5_W2!7 -> goto state4
	fi
state4:
	if
	:: W5_W4!6 -> goto state5
	fi
state5:
	skip
} /* W5 */

proctype W6()
{
state1:
	if
	:: W_W6?3 -> goto state2
	fi
state2:
	if
	:: W3_W6?6 -> goto state3
	fi
state3:
	if
	:: W6_W3!7 -> goto state4
	fi
state4:
	if
	:: W6_W4!6 -> goto state5
	fi
state5:
	skip
} /* W6 */
