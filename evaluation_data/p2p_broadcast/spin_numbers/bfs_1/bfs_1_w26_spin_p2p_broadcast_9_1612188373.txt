> promela file    : ../p2p_broadcast/spin_code/p2p_broadcast_9.pml
> date            : 01-Feb-2021 15:06:13
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../p2p_broadcast/spin_code/p2p_broadcast_9.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w26 -n
Depth=      10 States=       11 Transitions=       11 Memory=   512.781	
Depth=      20 States=      610 Transitions= 1.82e+03 Memory=   513.172	
Depth=      30 States= 1.58e+04 Transitions= 7.14e+04 Memory=   527.234	t=     0.21 R=   8e+04
Depth=      40 States= 1.34e+05 Transitions= 7.22e+05 Memory=   626.062	t=     2.12 R=   6e+04
Depth=      50 States= 6.14e+05 Transitions= 3.68e+06 Memory=  1005.750	t=     10.9 R=   6e+04
Depth=      54 States=    1e+06 Transitions= 6.14e+06 Memory=  1309.266	t=     18.2 R=   6e+04
Depth=      60 States= 1.85e+06 Transitions= 1.18e+07 Memory=  1955.359	t=     35.1 R=   5e+04
Depth=      61 States= 2.03e+06 Transitions=  1.3e+07 Memory=  2087.391	t=     38.6 R=   5e+04
Depth=      65 States=    3e+06 Transitions= 1.96e+07 Memory=  2810.438	t=     58.2 R=   5e+04
Depth=      69 States=    4e+06 Transitions= 2.65e+07 Memory=  3551.453	t=     78.7 R=   5e+04
Depth=      70 States= 4.07e+06 Transitions=  2.7e+07 Memory=  3599.891	t=     80.1 R=   5e+04
Depth=      73 States=    5e+06 Transitions= 3.35e+07 Memory=  4288.172	t=     99.3 R=   5e+04
Depth=      76 States=    6e+06 Transitions= 4.05e+07 Memory=  5017.078	t=      120 R=   5e+04
Depth=      79 States=    7e+06 Transitions= 4.77e+07 Memory=  5731.922	t=      141 R=   5e+04
Depth=      80 States=  7.3e+06 Transitions= 4.99e+07 Memory=  5947.547	t=      148 R=   5e+04
Depth=      81 States=    8e+06 Transitions= 5.49e+07 Memory=  6436.219	t=      163 R=   5e+04
Depth=      84 States=    9e+06 Transitions= 6.21e+07 Memory=  7138.562	t=      184 R=   5e+04
Depth=      87 States=    1e+07 Transitions= 6.92e+07 Memory=  7843.250	t=      206 R=   5e+04
pan: reached -DMEMLIM bound
	8.58961e+09 bytes used
	409600 bytes more needed
	8.58993e+09 bytes limit
hint: to reduce memory, recompile with
  -DCOLLAPSE # good, fast compression, or
  -DMA=732   # better/slower compression, or
  -DHC # hash-compaction, approximation
  -DBITSTATE # supertrace, approximation

(Spin Version 6.5.1 -- 31 July 2020)
Warning: Search not completed
	+ Breadth-First Search
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 732 byte, depth reached 89, errors: 0
 10484943 states, stored
	1.04849e+07 nominal states (stored-atomic)
 62118853 states, matched
 72603796 transitions (= stored+matched)
        0 atomic steps
hash conflicts:   2237493 (resolved)

Stats on memory usage (in Megabytes):
 7599.408	equivalent memory usage for states (stored*(State-vector + overhead))
 7686.870	actual memory usage for states
  512.000	memory used for hash table (-w26)
    7.183	memory lost to fragmentation
 8191.688	total actual memory usage



pan: elapsed time 216 seconds
pan: rate  48586.39 states/second

real	3m36.225s
user	3m31.417s
sys	0m4.792s
