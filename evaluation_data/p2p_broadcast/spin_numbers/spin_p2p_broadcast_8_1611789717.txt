> promela file    : ../p2p_broadcast/spin_code/p2p_broadcast_8.pml
> date            : 28-Jan-2021 00:21:58
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../p2p_broadcast/spin_code/p2p_broadcast_8.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n
Depth=  547096 States=    1e+06 Transitions= 4.17e+06 Memory=  1065.011	t=     10.3 R=   1e+05
Depth=  561996 States=    2e+06 Transitions= 1.04e+07 Memory=  1591.573	t=     26.1 R=   8e+04
pan: resizing hashtable to -w21..  done
Depth=  561996 States=    3e+06 Transitions= 1.67e+07 Memory=  2133.745	t=     41.7 R=   7e+04

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 580 byte, depth reached 561996, errors: 0
  3616628 states, stored
 18405449 states, matched
 22022077 transitions (= stored+matched)
        0 atomic steps
hash conflicts:  18407785 (resolved)

Stats on memory usage (in Megabytes):
 2097.044	equivalent memory usage for states (stored*(State-vector + overhead))
 1908.943	actual memory usage for states (compression: 91.03%)
         	state-vector as stored = 525 byte + 28 byte overhead
   16.000	memory used for hash table (-w21)
  534.058	memory used for DFS stack (-m10000000)
 2458.355	total actual memory usage



pan: elapsed time 55 seconds
pan: rate 65756.873 states/second

real	0m55.314s
user	0m53.983s
sys	0m1.320s
