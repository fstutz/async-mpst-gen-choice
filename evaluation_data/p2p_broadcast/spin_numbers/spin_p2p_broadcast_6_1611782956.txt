> promela file    : ../p2p_broadcast/spin_code/p2p_broadcast_6.pml
> date            : 27-Jan-2021 22:29:16
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../p2p_broadcast/spin_code/p2p_broadcast_6.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 332 byte, depth reached 7132, errors: 0
    24772 states, stored
    79684 states, matched
   104456 transitions (= stored+matched)
        0 atomic steps
hash conflicts:      1165 (resolved)

Stats on memory usage (in Megabytes):
    8.505	equivalent memory usage for states (stored*(State-vector + overhead))
    8.182	actual memory usage for states (compression: 96.21%)
         	state-vector as stored = 318 byte + 28 byte overhead
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  545.870	total actual memory usage



pan: elapsed time 0.15 seconds
pan: rate 165146.67 states/second

real	0m0.402s
user	0m0.222s
sys	0m0.174s
