#Size	#Para	 Time		 Diff		 Proc		 Diff
4		2	     0.111	     0.111	     0.056	     0.056
8		3	     0.325	     0.214	     0.108	     0.053
14		4	     0.494	     0.170	     0.124	     0.015
22		5	     0.751	     0.256	     0.150	     0.027
32		6	     1.201	     0.450	     0.200	     0.050
44		7	     1.876	     0.675	     0.268	     0.068
58		8	     2.642	     0.766	     0.330	     0.062
74		9	     3.697	     1.055	     0.411	     0.081
92		10	     4.951	     1.254	     0.495	     0.084
112		11	     6.464	     1.513	     0.588	     0.093
134		12	    19.343	    12.878	     1.612	     1.024
158		13	    10.837	    -8.506	     0.834	    -0.778
184		14	    14.536	     3.699	     1.038	     0.205
212		15	    16.779	     2.243	     1.119	     0.080
242		16	    20.550	     3.771	     1.284	     0.166
274		17	    24.063	     3.512	     1.415	     0.131
308		18	    28.952	     4.889	     1.608	     0.193
344		19	    34.047	     5.095	     1.792	     0.184
382		20	    52.640	    18.593	     2.632	     0.840
422		21	    62.971	    10.330	     2.999	     0.367
464		22	    66.777	     3.806	     3.035	     0.037
508		23	    68.207	     1.430	     2.966	    -0.070
554		24	   116.049	    47.842	     4.835	     1.870
602		25	    88.533	   -27.517	     3.541	    -1.294
652		26	   101.393	    12.861	     3.900	     0.358
704		27	   114.018	    12.625	     4.223	     0.323
758		28	   128.148	    14.129	     4.577	     0.354
814		29	   141.220	    13.072	     4.870	     0.293
872		30	   160.671	    19.451	     5.356	     0.486
932		31	   190.926	    30.255	     6.159	     0.803
994		32	   198.835	     7.909	     6.214	     0.055
1058		33	   222.128	    23.293	     6.731	     0.518
