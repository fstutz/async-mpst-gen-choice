chan W1_W7 = [1] of { int };
chan W1_W9 = [1] of { int };
chan W10_W11 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W1_W4 = [1] of { int };
chan W1_W5 = [1] of { int };
chan W6_W2 = [1] of { int };
chan W6_W4 = [1] of { int };
chan W6_W7 = [1] of { int };
chan W2_W11 = [1] of { int };
chan W4_W11 = [1] of { int };
chan W8_W11 = [1] of { int };
chan W7_W5 = [1] of { int };
chan W5_W10 = [1] of { int };
chan W3_W7 = [1] of { int };
chan W8_W10 = [1] of { int };
chan W11_W9 = [1] of { int };
chan W3_W2 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W7_W11 = [1] of { int };
chan W8_W5 = [1] of { int };
chan W2_W6 = [1] of { int };
chan W4_W6 = [1] of { int };
chan W9_W7 = [1] of { int };
chan W2_W5 = [1] of { int };
chan W4_W5 = [1] of { int };
chan W11_W3 = [1] of { int };
chan W9_W4 = [1] of { int };
chan W9_W2 = [1] of { int };
chan W5_W1 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W4_W1 = [1] of { int };
chan W10_W3 = [1] of { int };
chan W6_W8 = [1] of { int };
chan W7_W1 = [1] of { int };
chan W1_W6 = [1] of { int };
chan W1_W11 = [1] of { int };
chan W9_W10 = [1] of { int };
chan W9_W6 = [1] of { int };
chan W10_W9 = [1] of { int };
chan W2_W10 = [1] of { int };
chan W4_W10 = [1] of { int };
chan W10_W5 = [1] of { int };
chan W3_W10 = [1] of { int };
chan W8_W7 = [1] of { int };
chan W5_W11 = [1] of { int };
chan W3_W11 = [1] of { int };
chan W7_W2 = [1] of { int };
chan W7_W4 = [1] of { int };
chan W6_W3 = [1] of { int };
chan W7_W10 = [1] of { int };
chan W10_W8 = [1] of { int };
chan W6_W5 = [1] of { int };
chan W6_W9 = [1] of { int };
chan W9_W1 = [1] of { int };
chan W3_W6 = [1] of { int };
chan W8_W6 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W1_W10 = [1] of { int };
chan W9_W11 = [1] of { int };
chan W11_W1 = [1] of { int };
chan W8_W1 = [1] of { int };
chan W8_W3 = [1] of { int };
chan W5_W2 = [1] of { int };
chan W5_W4 = [1] of { int };
chan W5_W9 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W8_W9 = [1] of { int };
chan W11_W6 = [1] of { int };
chan W11_W10 = [1] of { int };
chan W7_W6 = [1] of { int };
chan W1_W8 = [1] of { int };
chan W5_W3 = [1] of { int };
chan W10_W7 = [1] of { int };
chan W10_W4 = [1] of { int };
chan W11_W4 = [1] of { int };
chan W10_W2 = [1] of { int };
chan W11_W2 = [1] of { int };
chan W5_W6 = [1] of { int };
chan W9_W3 = [1] of { int };
chan W11_W7 = [1] of { int };
chan W3_W8 = [1] of { int };
chan W2_W8 = [1] of { int };
chan W4_W8 = [1] of { int };
chan W6_W1 = [1] of { int };
chan W7_W8 = [1] of { int };
chan W10_W6 = [1] of { int };
chan W11_W8 = [1] of { int };
chan W2_W9 = [1] of { int };
chan W4_W9 = [1] of { int };
chan W9_W5 = [1] of { int };
chan W3_W9 = [1] of { int };
chan W7_W3 = [1] of { int };
chan W8_W2 = [1] of { int };
chan W8_W4 = [1] of { int };
chan W2_W3 = [1] of { int };
chan W4_W3 = [1] of { int };
chan W11_W5 = [1] of { int };
chan W7_W9 = [1] of { int };
chan W9_W8 = [1] of { int };
chan W6_W11 = [1] of { int };
chan W2_W7 = [1] of { int };
chan W4_W7 = [1] of { int };
chan W6_W10 = [1] of { int };
chan W10_W1 = [1] of { int };
chan W2_W4 = [1] of { int };
chan W4_W2 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W5_W8 = [1] of { int };
chan W3_W5 = [1] of { int };

init
{
	run W1();
	run W10();
	run W11();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W2!1 -> goto state3
	fi
state3:
	if
	:: W1_W3!1 -> goto state4
	fi
state4:
	if
	:: W1_W4!1 -> goto state5
	fi
state5:
	if
	:: W1_W5!1 -> goto state6
	fi
state6:
	if
	:: W1_W6!1 -> goto state7
	fi
state7:
	if
	:: W1_W7!1 -> goto state8
	fi
state8:
	if
	:: W1_W8!1 -> goto state9
	fi
state9:
	if
	:: W1_W9!1 -> goto state10
	fi
state10:
	if
	:: W1_W10!1 -> goto state11
	fi
state11:
	if
	:: W1_W11!1 -> goto state12
	fi
state12:
	if
	:: W2_W1?1 -> goto state13
	fi
state13:
	if
	:: W3_W1?1 -> goto state14
	fi
state14:
	if
	:: W4_W1?1 -> goto state15
	fi
state15:
	if
	:: W5_W1?1 -> goto state16
	fi
state16:
	if
	:: W6_W1?1 -> goto state17
	fi
state17:
	if
	:: W7_W1?1 -> goto state18
	fi
state18:
	if
	:: W8_W1?1 -> goto state19
	fi
state19:
	if
	:: W9_W1?1 -> goto state20
	fi
state20:
	if
	:: W10_W1?1 -> goto state21
	fi
state21:
	if
	:: W11_W1?1 -> goto state22
	fi
state22:
	goto state2
} /* W1 */

proctype W10()
{
state1:
	goto state2
state2:
	if
	:: W1_W10?1 -> goto state3
	fi
state3:
	if
	:: W2_W10?1 -> goto state4
	fi
state4:
	if
	:: W3_W10?1 -> goto state5
	fi
state5:
	if
	:: W4_W10?1 -> goto state6
	fi
state6:
	if
	:: W5_W10?1 -> goto state7
	fi
state7:
	if
	:: W6_W10?1 -> goto state8
	fi
state8:
	if
	:: W7_W10?1 -> goto state9
	fi
state9:
	if
	:: W8_W10?1 -> goto state10
	fi
state10:
	if
	:: W9_W10?1 -> goto state11
	fi
state11:
	if
	:: W10_W1!1 -> goto state12
	fi
state12:
	if
	:: W10_W2!1 -> goto state13
	fi
state13:
	if
	:: W10_W3!1 -> goto state14
	fi
state14:
	if
	:: W10_W4!1 -> goto state15
	fi
state15:
	if
	:: W10_W5!1 -> goto state16
	fi
state16:
	if
	:: W10_W6!1 -> goto state17
	fi
state17:
	if
	:: W10_W7!1 -> goto state18
	fi
state18:
	if
	:: W10_W8!1 -> goto state19
	fi
state19:
	if
	:: W10_W9!1 -> goto state20
	fi
state20:
	if
	:: W10_W11!1 -> goto state21
	fi
state21:
	if
	:: W11_W10?1 -> goto state22
	fi
state22:
	goto state2
} /* W10 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: W1_W11?1 -> goto state3
	fi
state3:
	if
	:: W2_W11?1 -> goto state4
	fi
state4:
	if
	:: W3_W11?1 -> goto state5
	fi
state5:
	if
	:: W4_W11?1 -> goto state6
	fi
state6:
	if
	:: W5_W11?1 -> goto state7
	fi
state7:
	if
	:: W6_W11?1 -> goto state8
	fi
state8:
	if
	:: W7_W11?1 -> goto state9
	fi
state9:
	if
	:: W8_W11?1 -> goto state10
	fi
state10:
	if
	:: W9_W11?1 -> goto state11
	fi
state11:
	if
	:: W10_W11?1 -> goto state12
	fi
state12:
	if
	:: W11_W1!1 -> goto state13
	fi
state13:
	if
	:: W11_W2!1 -> goto state14
	fi
state14:
	if
	:: W11_W3!1 -> goto state15
	fi
state15:
	if
	:: W11_W4!1 -> goto state16
	fi
state16:
	if
	:: W11_W5!1 -> goto state17
	fi
state17:
	if
	:: W11_W6!1 -> goto state18
	fi
state18:
	if
	:: W11_W7!1 -> goto state19
	fi
state19:
	if
	:: W11_W8!1 -> goto state20
	fi
state20:
	if
	:: W11_W9!1 -> goto state21
	fi
state21:
	if
	:: W11_W10!1 -> goto state22
	fi
state22:
	goto state2
} /* W11 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_W1!1 -> goto state4
	fi
state4:
	if
	:: W2_W3!1 -> goto state5
	fi
state5:
	if
	:: W2_W4!1 -> goto state6
	fi
state6:
	if
	:: W2_W5!1 -> goto state7
	fi
state7:
	if
	:: W2_W6!1 -> goto state8
	fi
state8:
	if
	:: W2_W7!1 -> goto state9
	fi
state9:
	if
	:: W2_W8!1 -> goto state10
	fi
state10:
	if
	:: W2_W9!1 -> goto state11
	fi
state11:
	if
	:: W2_W10!1 -> goto state12
	fi
state12:
	if
	:: W2_W11!1 -> goto state13
	fi
state13:
	if
	:: W3_W2?1 -> goto state14
	fi
state14:
	if
	:: W4_W2?1 -> goto state15
	fi
state15:
	if
	:: W5_W2?1 -> goto state16
	fi
state16:
	if
	:: W6_W2?1 -> goto state17
	fi
state17:
	if
	:: W7_W2?1 -> goto state18
	fi
state18:
	if
	:: W8_W2?1 -> goto state19
	fi
state19:
	if
	:: W9_W2?1 -> goto state20
	fi
state20:
	if
	:: W10_W2?1 -> goto state21
	fi
state21:
	if
	:: W11_W2?1 -> goto state22
	fi
state22:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W2_W3?1 -> goto state4
	fi
state4:
	if
	:: W3_W1!1 -> goto state5
	fi
state5:
	if
	:: W3_W2!1 -> goto state6
	fi
state6:
	if
	:: W3_W4!1 -> goto state7
	fi
state7:
	if
	:: W3_W5!1 -> goto state8
	fi
state8:
	if
	:: W3_W6!1 -> goto state9
	fi
state9:
	if
	:: W3_W7!1 -> goto state10
	fi
state10:
	if
	:: W3_W8!1 -> goto state11
	fi
state11:
	if
	:: W3_W9!1 -> goto state12
	fi
state12:
	if
	:: W3_W10!1 -> goto state13
	fi
state13:
	if
	:: W3_W11!1 -> goto state14
	fi
state14:
	if
	:: W4_W3?1 -> goto state15
	fi
state15:
	if
	:: W5_W3?1 -> goto state16
	fi
state16:
	if
	:: W6_W3?1 -> goto state17
	fi
state17:
	if
	:: W7_W3?1 -> goto state18
	fi
state18:
	if
	:: W8_W3?1 -> goto state19
	fi
state19:
	if
	:: W9_W3?1 -> goto state20
	fi
state20:
	if
	:: W10_W3?1 -> goto state21
	fi
state21:
	if
	:: W11_W3?1 -> goto state22
	fi
state22:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W1_W4?1 -> goto state3
	fi
state3:
	if
	:: W2_W4?1 -> goto state4
	fi
state4:
	if
	:: W3_W4?1 -> goto state5
	fi
state5:
	if
	:: W4_W1!1 -> goto state6
	fi
state6:
	if
	:: W4_W2!1 -> goto state7
	fi
state7:
	if
	:: W4_W3!1 -> goto state8
	fi
state8:
	if
	:: W4_W5!1 -> goto state9
	fi
state9:
	if
	:: W4_W6!1 -> goto state10
	fi
state10:
	if
	:: W4_W7!1 -> goto state11
	fi
state11:
	if
	:: W4_W8!1 -> goto state12
	fi
state12:
	if
	:: W4_W9!1 -> goto state13
	fi
state13:
	if
	:: W4_W10!1 -> goto state14
	fi
state14:
	if
	:: W4_W11!1 -> goto state15
	fi
state15:
	if
	:: W5_W4?1 -> goto state16
	fi
state16:
	if
	:: W6_W4?1 -> goto state17
	fi
state17:
	if
	:: W7_W4?1 -> goto state18
	fi
state18:
	if
	:: W8_W4?1 -> goto state19
	fi
state19:
	if
	:: W9_W4?1 -> goto state20
	fi
state20:
	if
	:: W10_W4?1 -> goto state21
	fi
state21:
	if
	:: W11_W4?1 -> goto state22
	fi
state22:
	goto state2
} /* W4 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W2_W5?1 -> goto state4
	fi
state4:
	if
	:: W3_W5?1 -> goto state5
	fi
state5:
	if
	:: W4_W5?1 -> goto state6
	fi
state6:
	if
	:: W5_W1!1 -> goto state7
	fi
state7:
	if
	:: W5_W2!1 -> goto state8
	fi
state8:
	if
	:: W5_W3!1 -> goto state9
	fi
state9:
	if
	:: W5_W4!1 -> goto state10
	fi
state10:
	if
	:: W5_W6!1 -> goto state11
	fi
state11:
	if
	:: W5_W7!1 -> goto state12
	fi
state12:
	if
	:: W5_W8!1 -> goto state13
	fi
state13:
	if
	:: W5_W9!1 -> goto state14
	fi
state14:
	if
	:: W5_W10!1 -> goto state15
	fi
state15:
	if
	:: W5_W11!1 -> goto state16
	fi
state16:
	if
	:: W6_W5?1 -> goto state17
	fi
state17:
	if
	:: W7_W5?1 -> goto state18
	fi
state18:
	if
	:: W8_W5?1 -> goto state19
	fi
state19:
	if
	:: W9_W5?1 -> goto state20
	fi
state20:
	if
	:: W10_W5?1 -> goto state21
	fi
state21:
	if
	:: W11_W5?1 -> goto state22
	fi
state22:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: W1_W6?1 -> goto state3
	fi
state3:
	if
	:: W2_W6?1 -> goto state4
	fi
state4:
	if
	:: W3_W6?1 -> goto state5
	fi
state5:
	if
	:: W4_W6?1 -> goto state6
	fi
state6:
	if
	:: W5_W6?1 -> goto state7
	fi
state7:
	if
	:: W6_W1!1 -> goto state8
	fi
state8:
	if
	:: W6_W2!1 -> goto state9
	fi
state9:
	if
	:: W6_W3!1 -> goto state10
	fi
state10:
	if
	:: W6_W4!1 -> goto state11
	fi
state11:
	if
	:: W6_W5!1 -> goto state12
	fi
state12:
	if
	:: W6_W7!1 -> goto state13
	fi
state13:
	if
	:: W6_W8!1 -> goto state14
	fi
state14:
	if
	:: W6_W9!1 -> goto state15
	fi
state15:
	if
	:: W6_W10!1 -> goto state16
	fi
state16:
	if
	:: W6_W11!1 -> goto state17
	fi
state17:
	if
	:: W7_W6?1 -> goto state18
	fi
state18:
	if
	:: W8_W6?1 -> goto state19
	fi
state19:
	if
	:: W9_W6?1 -> goto state20
	fi
state20:
	if
	:: W10_W6?1 -> goto state21
	fi
state21:
	if
	:: W11_W6?1 -> goto state22
	fi
state22:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W1_W7?1 -> goto state3
	fi
state3:
	if
	:: W2_W7?1 -> goto state4
	fi
state4:
	if
	:: W3_W7?1 -> goto state5
	fi
state5:
	if
	:: W4_W7?1 -> goto state6
	fi
state6:
	if
	:: W5_W7?1 -> goto state7
	fi
state7:
	if
	:: W6_W7?1 -> goto state8
	fi
state8:
	if
	:: W7_W1!1 -> goto state9
	fi
state9:
	if
	:: W7_W2!1 -> goto state10
	fi
state10:
	if
	:: W7_W3!1 -> goto state11
	fi
state11:
	if
	:: W7_W4!1 -> goto state12
	fi
state12:
	if
	:: W7_W5!1 -> goto state13
	fi
state13:
	if
	:: W7_W6!1 -> goto state14
	fi
state14:
	if
	:: W7_W8!1 -> goto state15
	fi
state15:
	if
	:: W7_W9!1 -> goto state16
	fi
state16:
	if
	:: W7_W10!1 -> goto state17
	fi
state17:
	if
	:: W7_W11!1 -> goto state18
	fi
state18:
	if
	:: W8_W7?1 -> goto state19
	fi
state19:
	if
	:: W9_W7?1 -> goto state20
	fi
state20:
	if
	:: W10_W7?1 -> goto state21
	fi
state21:
	if
	:: W11_W7?1 -> goto state22
	fi
state22:
	goto state2
} /* W7 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: W1_W8?1 -> goto state3
	fi
state3:
	if
	:: W2_W8?1 -> goto state4
	fi
state4:
	if
	:: W3_W8?1 -> goto state5
	fi
state5:
	if
	:: W4_W8?1 -> goto state6
	fi
state6:
	if
	:: W5_W8?1 -> goto state7
	fi
state7:
	if
	:: W6_W8?1 -> goto state8
	fi
state8:
	if
	:: W7_W8?1 -> goto state9
	fi
state9:
	if
	:: W8_W1!1 -> goto state10
	fi
state10:
	if
	:: W8_W2!1 -> goto state11
	fi
state11:
	if
	:: W8_W3!1 -> goto state12
	fi
state12:
	if
	:: W8_W4!1 -> goto state13
	fi
state13:
	if
	:: W8_W5!1 -> goto state14
	fi
state14:
	if
	:: W8_W6!1 -> goto state15
	fi
state15:
	if
	:: W8_W7!1 -> goto state16
	fi
state16:
	if
	:: W8_W9!1 -> goto state17
	fi
state17:
	if
	:: W8_W10!1 -> goto state18
	fi
state18:
	if
	:: W8_W11!1 -> goto state19
	fi
state19:
	if
	:: W9_W8?1 -> goto state20
	fi
state20:
	if
	:: W10_W8?1 -> goto state21
	fi
state21:
	if
	:: W11_W8?1 -> goto state22
	fi
state22:
	goto state2
} /* W8 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: W1_W9?1 -> goto state3
	fi
state3:
	if
	:: W2_W9?1 -> goto state4
	fi
state4:
	if
	:: W3_W9?1 -> goto state5
	fi
state5:
	if
	:: W4_W9?1 -> goto state6
	fi
state6:
	if
	:: W5_W9?1 -> goto state7
	fi
state7:
	if
	:: W6_W9?1 -> goto state8
	fi
state8:
	if
	:: W7_W9?1 -> goto state9
	fi
state9:
	if
	:: W8_W9?1 -> goto state10
	fi
state10:
	if
	:: W9_W1!1 -> goto state11
	fi
state11:
	if
	:: W9_W2!1 -> goto state12
	fi
state12:
	if
	:: W9_W3!1 -> goto state13
	fi
state13:
	if
	:: W9_W4!1 -> goto state14
	fi
state14:
	if
	:: W9_W5!1 -> goto state15
	fi
state15:
	if
	:: W9_W6!1 -> goto state16
	fi
state16:
	if
	:: W9_W7!1 -> goto state17
	fi
state17:
	if
	:: W9_W8!1 -> goto state18
	fi
state18:
	if
	:: W9_W10!1 -> goto state19
	fi
state19:
	if
	:: W9_W11!1 -> goto state20
	fi
state20:
	if
	:: W10_W9?1 -> goto state21
	fi
state21:
	if
	:: W11_W9?1 -> goto state22
	fi
state22:
	goto state2
} /* W9 */
