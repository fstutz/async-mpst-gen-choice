chan W2_W4 = [1] of { int };
chan W4_W1 = [1] of { int };
chan W4_W3 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W3_W2 = [1] of { int };
chan W1_W4 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W4_W2 = [1] of { int };
chan W2_W3 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W1_W3 = [1] of { int };

init
{
	run W1();
	run W2();
	run W3();
	run W4()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W2!1 -> goto state3
	fi
state3:
	if
	:: W1_W3!1 -> goto state4
	fi
state4:
	if
	:: W1_W4!1 -> goto state5
	fi
state5:
	if
	:: W2_W1?1 -> goto state6
	fi
state6:
	if
	:: W3_W1?1 -> goto state7
	fi
state7:
	if
	:: W4_W1?1 -> goto state8
	fi
state8:
	goto state2
} /* W1 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_W1!1 -> goto state4
	fi
state4:
	if
	:: W2_W3!1 -> goto state5
	fi
state5:
	if
	:: W2_W4!1 -> goto state6
	fi
state6:
	if
	:: W3_W2?1 -> goto state7
	fi
state7:
	if
	:: W4_W2?1 -> goto state8
	fi
state8:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W2_W3?1 -> goto state4
	fi
state4:
	if
	:: W3_W1!1 -> goto state5
	fi
state5:
	if
	:: W3_W2!1 -> goto state6
	fi
state6:
	if
	:: W3_W4!1 -> goto state7
	fi
state7:
	if
	:: W4_W3?1 -> goto state8
	fi
state8:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W1_W4?1 -> goto state3
	fi
state3:
	if
	:: W2_W4?1 -> goto state4
	fi
state4:
	if
	:: W3_W4?1 -> goto state5
	fi
state5:
	if
	:: W4_W1!1 -> goto state6
	fi
state6:
	if
	:: W4_W2!1 -> goto state7
	fi
state7:
	if
	:: W4_W3!1 -> goto state8
	fi
state8:
	goto state2
} /* W4 */
