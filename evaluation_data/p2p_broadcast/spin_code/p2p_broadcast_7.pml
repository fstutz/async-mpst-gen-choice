chan W7_W1 = [1] of { int };
chan W2_W4 = [1] of { int };
chan W3_W2 = [1] of { int };
chan W6_W5 = [1] of { int };
chan W7_W5 = [1] of { int };
chan W2_W6 = [1] of { int };
chan W2_W7 = [1] of { int };
chan W2_W5 = [1] of { int };
chan W7_W6 = [1] of { int };
chan W6_W1 = [1] of { int };
chan W5_W4 = [1] of { int };
chan W6_W7 = [1] of { int };
chan W7_W4 = [1] of { int };
chan W2_W1 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W4_W7 = [1] of { int };
chan W7_W3 = [1] of { int };
chan W4_W1 = [1] of { int };
chan W5_W3 = [1] of { int };
chan W1_W5 = [1] of { int };
chan W3_W6 = [1] of { int };
chan W1_W7 = [1] of { int };
chan W4_W3 = [1] of { int };
chan W5_W1 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W7_W2 = [1] of { int };
chan W5_W2 = [1] of { int };
chan W5_W6 = [1] of { int };
chan W6_W2 = [1] of { int };
chan W3_W5 = [1] of { int };
chan W1_W6 = [1] of { int };
chan W4_W5 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W3_W7 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W1_W4 = [1] of { int };
chan W6_W3 = [1] of { int };
chan W2_W3 = [1] of { int };
chan W4_W6 = [1] of { int };
chan W4_W2 = [1] of { int };
chan W6_W4 = [1] of { int };

init
{
	run W1();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W2!1 -> goto state3
	fi
state3:
	if
	:: W1_W3!1 -> goto state4
	fi
state4:
	if
	:: W1_W4!1 -> goto state5
	fi
state5:
	if
	:: W1_W5!1 -> goto state6
	fi
state6:
	if
	:: W1_W6!1 -> goto state7
	fi
state7:
	if
	:: W1_W7!1 -> goto state8
	fi
state8:
	if
	:: W2_W1?1 -> goto state9
	fi
state9:
	if
	:: W3_W1?1 -> goto state10
	fi
state10:
	if
	:: W4_W1?1 -> goto state11
	fi
state11:
	if
	:: W5_W1?1 -> goto state12
	fi
state12:
	if
	:: W6_W1?1 -> goto state13
	fi
state13:
	if
	:: W7_W1?1 -> goto state14
	fi
state14:
	goto state2
} /* W1 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_W1!1 -> goto state4
	fi
state4:
	if
	:: W2_W3!1 -> goto state5
	fi
state5:
	if
	:: W2_W4!1 -> goto state6
	fi
state6:
	if
	:: W2_W5!1 -> goto state7
	fi
state7:
	if
	:: W2_W6!1 -> goto state8
	fi
state8:
	if
	:: W2_W7!1 -> goto state9
	fi
state9:
	if
	:: W3_W2?1 -> goto state10
	fi
state10:
	if
	:: W4_W2?1 -> goto state11
	fi
state11:
	if
	:: W5_W2?1 -> goto state12
	fi
state12:
	if
	:: W6_W2?1 -> goto state13
	fi
state13:
	if
	:: W7_W2?1 -> goto state14
	fi
state14:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W2_W3?1 -> goto state4
	fi
state4:
	if
	:: W3_W1!1 -> goto state5
	fi
state5:
	if
	:: W3_W2!1 -> goto state6
	fi
state6:
	if
	:: W3_W4!1 -> goto state7
	fi
state7:
	if
	:: W3_W5!1 -> goto state8
	fi
state8:
	if
	:: W3_W6!1 -> goto state9
	fi
state9:
	if
	:: W3_W7!1 -> goto state10
	fi
state10:
	if
	:: W4_W3?1 -> goto state11
	fi
state11:
	if
	:: W5_W3?1 -> goto state12
	fi
state12:
	if
	:: W6_W3?1 -> goto state13
	fi
state13:
	if
	:: W7_W3?1 -> goto state14
	fi
state14:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W1_W4?1 -> goto state3
	fi
state3:
	if
	:: W2_W4?1 -> goto state4
	fi
state4:
	if
	:: W3_W4?1 -> goto state5
	fi
state5:
	if
	:: W4_W1!1 -> goto state6
	fi
state6:
	if
	:: W4_W2!1 -> goto state7
	fi
state7:
	if
	:: W4_W3!1 -> goto state8
	fi
state8:
	if
	:: W4_W5!1 -> goto state9
	fi
state9:
	if
	:: W4_W6!1 -> goto state10
	fi
state10:
	if
	:: W4_W7!1 -> goto state11
	fi
state11:
	if
	:: W5_W4?1 -> goto state12
	fi
state12:
	if
	:: W6_W4?1 -> goto state13
	fi
state13:
	if
	:: W7_W4?1 -> goto state14
	fi
state14:
	goto state2
} /* W4 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W2_W5?1 -> goto state4
	fi
state4:
	if
	:: W3_W5?1 -> goto state5
	fi
state5:
	if
	:: W4_W5?1 -> goto state6
	fi
state6:
	if
	:: W5_W1!1 -> goto state7
	fi
state7:
	if
	:: W5_W2!1 -> goto state8
	fi
state8:
	if
	:: W5_W3!1 -> goto state9
	fi
state9:
	if
	:: W5_W4!1 -> goto state10
	fi
state10:
	if
	:: W5_W6!1 -> goto state11
	fi
state11:
	if
	:: W5_W7!1 -> goto state12
	fi
state12:
	if
	:: W6_W5?1 -> goto state13
	fi
state13:
	if
	:: W7_W5?1 -> goto state14
	fi
state14:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: W1_W6?1 -> goto state3
	fi
state3:
	if
	:: W2_W6?1 -> goto state4
	fi
state4:
	if
	:: W3_W6?1 -> goto state5
	fi
state5:
	if
	:: W4_W6?1 -> goto state6
	fi
state6:
	if
	:: W5_W6?1 -> goto state7
	fi
state7:
	if
	:: W6_W1!1 -> goto state8
	fi
state8:
	if
	:: W6_W2!1 -> goto state9
	fi
state9:
	if
	:: W6_W3!1 -> goto state10
	fi
state10:
	if
	:: W6_W4!1 -> goto state11
	fi
state11:
	if
	:: W6_W5!1 -> goto state12
	fi
state12:
	if
	:: W6_W7!1 -> goto state13
	fi
state13:
	if
	:: W7_W6?1 -> goto state14
	fi
state14:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W1_W7?1 -> goto state3
	fi
state3:
	if
	:: W2_W7?1 -> goto state4
	fi
state4:
	if
	:: W3_W7?1 -> goto state5
	fi
state5:
	if
	:: W4_W7?1 -> goto state6
	fi
state6:
	if
	:: W5_W7?1 -> goto state7
	fi
state7:
	if
	:: W6_W7?1 -> goto state8
	fi
state8:
	if
	:: W7_W1!1 -> goto state9
	fi
state9:
	if
	:: W7_W2!1 -> goto state10
	fi
state10:
	if
	:: W7_W3!1 -> goto state11
	fi
state11:
	if
	:: W7_W4!1 -> goto state12
	fi
state12:
	if
	:: W7_W5!1 -> goto state13
	fi
state13:
	if
	:: W7_W6!1 -> goto state14
	fi
state14:
	goto state2
} /* W7 */
