chan W1_W3 = [1] of { int };
chan W2_W3 = [1] of { int };
chan W3_W2 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W3_W1 = [1] of { int };
chan W2_W1 = [1] of { int };

init
{
	run W1();
	run W2();
	run W3()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W2!1 -> goto state3
	fi
state3:
	if
	:: W1_W3!1 -> goto state4
	fi
state4:
	if
	:: W2_W1?1 -> goto state5
	fi
state5:
	if
	:: W3_W1?1 -> goto state6
	fi
state6:
	goto state2
} /* W1 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_W1!1 -> goto state4
	fi
state4:
	if
	:: W2_W3!1 -> goto state5
	fi
state5:
	if
	:: W3_W2?1 -> goto state6
	fi
state6:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W2_W3?1 -> goto state4
	fi
state4:
	if
	:: W3_W1!1 -> goto state5
	fi
state5:
	if
	:: W3_W2!1 -> goto state6
	fi
state6:
	goto state2
} /* W3 */
