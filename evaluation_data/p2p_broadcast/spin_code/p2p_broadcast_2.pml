chan W1_W2 = [1] of { int };
chan W2_W1 = [1] of { int };

init
{
	run W1();
	run W2()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W2!1 -> goto state3
	fi
state3:
	if
	:: W2_W1?1 -> goto state4
	fi
state4:
	goto state2
} /* W1 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_W1!1 -> goto state4
	fi
state4:
	goto state2
} /* W2 */
