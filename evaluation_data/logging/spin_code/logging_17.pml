chan Web_BE3 = [1] of { int };
chan Web_BE16 = [1] of { int };
chan Log_BE17 = [1] of { int };
chan Web_BE10 = [1] of { int };
chan Web_BE1 = [1] of { int };
chan BE1_Log = [1] of { int };
chan Log_BE2 = [1] of { int };
chan Web_BE4 = [1] of { int };
chan Web_BE8 = [1] of { int };
chan Log_BE11 = [1] of { int };
chan Web_BE6 = [1] of { int };
chan Web_BE12 = [1] of { int };
chan Web_BE9 = [1] of { int };
chan C_Web = [1] of { int };
chan BE9_Log = [1] of { int };
chan Log_BE7 = [1] of { int };
chan BE12_Log = [1] of { int };
chan BE15_Log = [1] of { int };
chan BE5_Log = [1] of { int };
chan BE1_C = [1] of { int };
chan Log_BE16 = [1] of { int };
chan BE3_Log = [1] of { int };
chan BE4_Log = [1] of { int };
chan Log_BE4 = [1] of { int };
chan BE13_Log = [1] of { int };
chan BE4_C = [1] of { int };
chan BE14_C = [1] of { int };
chan Web_C = [1] of { int };
chan Log_BE10 = [1] of { int };
chan BE12_C = [1] of { int };
chan Log_BE6 = [1] of { int };
chan BE8_Log = [1] of { int };
chan BE2_C = [1] of { int };
chan Web_BE15 = [1] of { int };
chan BE13_C = [1] of { int };
chan BE16_C = [1] of { int };
chan Log_BE9 = [1] of { int };
chan Web_BE14 = [1] of { int };
chan BE14_Log = [1] of { int };
chan Log_BE8 = [1] of { int };
chan BE16_Log = [1] of { int };
chan BE3_C = [1] of { int };
chan BE11_C = [1] of { int };
chan Log_BE3 = [1] of { int };
chan Web_BE17 = [1] of { int };
chan Log_BE1 = [1] of { int };
chan Web_BE2 = [1] of { int };
chan BE2_Log = [1] of { int };
chan BE10_Log = [1] of { int };
chan Log_BE13 = [1] of { int };
chan Web_BE7 = [1] of { int };
chan BE6_Log = [1] of { int };
chan BE15_C = [1] of { int };
chan BE17_C = [1] of { int };
chan Log_BE5 = [1] of { int };
chan Web_Log = [1] of { int };
chan BE7_C = [1] of { int };
chan BE10_C = [1] of { int };
chan Log_BE12 = [1] of { int };
chan Web_BE11 = [1] of { int };
chan BE6_C = [1] of { int };
chan BE11_Log = [1] of { int };
chan Web_BE13 = [1] of { int };
chan BE9_C = [1] of { int };
chan Log_BE14 = [1] of { int };
chan Log_Web = [1] of { int };
chan Web_BE5 = [1] of { int };
chan BE5_C = [1] of { int };
chan BE7_Log = [1] of { int };
chan Log_BE15 = [1] of { int };
chan BE8_C = [1] of { int };
chan BE17_Log = [1] of { int };

init
{
	run BE1();
	run BE10();
	run BE11();
	run BE12();
	run BE13();
	run BE14();
	run BE15();
	run BE16();
	run BE17();
	run BE2();
	run BE3();
	run BE4();
	run BE5();
	run BE6();
	run BE7();
	run BE8();
	run BE9();
	run C();
	run Log();
	run Web()
}

proctype BE1()
{
state1:
	goto state2
state2:
	if
	:: Web_BE1?1 -> goto state3
	fi
state3:
	if
	:: BE1_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE1?3 -> goto state5
	fi
state5:
	if
	:: BE1_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE1 */

proctype BE10()
{
state1:
	goto state2
state2:
	if
	:: Web_BE10?1 -> goto state3
	fi
state3:
	if
	:: BE10_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE10?3 -> goto state5
	fi
state5:
	if
	:: BE10_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE10 */

proctype BE11()
{
state1:
	goto state2
state2:
	if
	:: Web_BE11?1 -> goto state3
	fi
state3:
	if
	:: BE11_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE11?3 -> goto state5
	fi
state5:
	if
	:: BE11_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE11 */

proctype BE12()
{
state1:
	goto state2
state2:
	if
	:: Web_BE12?1 -> goto state3
	fi
state3:
	if
	:: BE12_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE12?3 -> goto state5
	fi
state5:
	if
	:: BE12_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE12 */

proctype BE13()
{
state1:
	goto state2
state2:
	if
	:: Web_BE13?1 -> goto state3
	fi
state3:
	if
	:: BE13_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE13?3 -> goto state5
	fi
state5:
	if
	:: BE13_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE13 */

proctype BE14()
{
state1:
	goto state2
state2:
	if
	:: Web_BE14?1 -> goto state3
	fi
state3:
	if
	:: BE14_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE14?3 -> goto state5
	fi
state5:
	if
	:: BE14_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE14 */

proctype BE15()
{
state1:
	goto state2
state2:
	if
	:: Web_BE15?1 -> goto state3
	fi
state3:
	if
	:: BE15_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE15?3 -> goto state5
	fi
state5:
	if
	:: BE15_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE15 */

proctype BE16()
{
state1:
	goto state2
state2:
	if
	:: Web_BE16?1 -> goto state3
	fi
state3:
	if
	:: BE16_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE16?3 -> goto state5
	fi
state5:
	if
	:: BE16_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE16 */

proctype BE17()
{
state1:
	goto state2
state2:
	if
	:: Web_BE17?1 -> goto state3
	fi
state3:
	if
	:: BE17_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE17?3 -> goto state5
	fi
state5:
	if
	:: BE17_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE17 */

proctype BE2()
{
state1:
	goto state2
state2:
	if
	:: Web_BE2?1 -> goto state3
	fi
state3:
	if
	:: BE2_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE2?3 -> goto state5
	fi
state5:
	if
	:: BE2_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE2 */

proctype BE3()
{
state1:
	goto state2
state2:
	if
	:: Web_BE3?1 -> goto state3
	fi
state3:
	if
	:: BE3_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE3?3 -> goto state5
	fi
state5:
	if
	:: BE3_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE3 */

proctype BE4()
{
state1:
	goto state2
state2:
	if
	:: Web_BE4?1 -> goto state3
	fi
state3:
	if
	:: BE4_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE4?3 -> goto state5
	fi
state5:
	if
	:: BE4_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE4 */

proctype BE5()
{
state1:
	goto state2
state2:
	if
	:: Web_BE5?1 -> goto state3
	fi
state3:
	if
	:: BE5_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE5?3 -> goto state5
	fi
state5:
	if
	:: BE5_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE5 */

proctype BE6()
{
state1:
	goto state2
state2:
	if
	:: Web_BE6?1 -> goto state3
	fi
state3:
	if
	:: BE6_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE6?3 -> goto state5
	fi
state5:
	if
	:: BE6_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE6 */

proctype BE7()
{
state1:
	goto state2
state2:
	if
	:: Web_BE7?1 -> goto state3
	fi
state3:
	if
	:: BE7_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE7?3 -> goto state5
	fi
state5:
	if
	:: BE7_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE7 */

proctype BE8()
{
state1:
	goto state2
state2:
	if
	:: Web_BE8?1 -> goto state3
	fi
state3:
	if
	:: BE8_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE8?3 -> goto state5
	fi
state5:
	if
	:: BE8_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE8 */

proctype BE9()
{
state1:
	goto state2
state2:
	if
	:: Web_BE9?1 -> goto state3
	fi
state3:
	if
	:: BE9_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE9?3 -> goto state5
	fi
state5:
	if
	:: BE9_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE9 */

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_Web!1 -> goto state3
	fi
state3:
	if
	:: BE2_C?4 -> goto state4
	:: BE3_C?4 -> goto state4
	:: BE5_C?4 -> goto state4
	:: BE7_C?4 -> goto state4
	:: BE9_C?4 -> goto state4
	:: BE13_C?4 -> goto state4
	:: BE8_C?4 -> goto state4
	:: Web_C?4 -> goto state4
	:: BE12_C?4 -> goto state4
	:: BE4_C?4 -> goto state4
	:: BE15_C?4 -> goto state4
	:: BE6_C?4 -> goto state4
	:: BE10_C?4 -> goto state4
	:: BE1_C?4 -> goto state4
	:: BE14_C?4 -> goto state4
	:: BE11_C?4 -> goto state4
	:: BE16_C?4 -> goto state4
	:: BE17_C?4 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype Log()
{
state1:
	goto state2
state2:
	if
	:: BE5_Log?2 -> goto state3
	:: BE15_Log?2 -> goto state4
	:: BE1_Log?2 -> goto state5
	:: BE10_Log?2 -> goto state6
	:: BE16_Log?2 -> goto state7
	:: BE12_Log?2 -> goto state8
	:: BE6_Log?2 -> goto state9
	:: BE8_Log?2 -> goto state10
	:: BE4_Log?2 -> goto state11
	:: Web_Log?2 -> goto state12
	:: BE11_Log?2 -> goto state13
	:: BE9_Log?2 -> goto state14
	:: BE13_Log?2 -> goto state15
	:: BE14_Log?2 -> goto state16
	:: BE7_Log?2 -> goto state17
	:: BE3_Log?2 -> goto state18
	:: BE2_Log?2 -> goto state19
	:: BE17_Log?2 -> goto state20
	fi
state3:
	if
	:: Log_BE5!3 -> goto state21
	fi
state21:
	goto state2
state4:
	if
	:: Log_BE15!3 -> goto state21
	fi
state5:
	if
	:: Log_BE1!3 -> goto state21
	fi
state6:
	if
	:: Log_BE10!3 -> goto state21
	fi
state7:
	if
	:: Log_BE16!3 -> goto state21
	fi
state8:
	if
	:: Log_BE12!3 -> goto state21
	fi
state9:
	if
	:: Log_BE6!3 -> goto state21
	fi
state10:
	if
	:: Log_BE8!3 -> goto state21
	fi
state11:
	if
	:: Log_BE4!3 -> goto state21
	fi
state12:
	if
	:: Log_Web!3 -> goto state21
	fi
state13:
	if
	:: Log_BE11!3 -> goto state21
	fi
state14:
	if
	:: Log_BE9!3 -> goto state21
	fi
state15:
	if
	:: Log_BE13!3 -> goto state21
	fi
state16:
	if
	:: Log_BE14!3 -> goto state21
	fi
state17:
	if
	:: Log_BE7!3 -> goto state21
	fi
state18:
	if
	:: Log_BE3!3 -> goto state21
	fi
state19:
	if
	:: Log_BE2!3 -> goto state21
	fi
state20:
	if
	:: Log_BE17!3 -> goto state21
	fi
} /* Log */

proctype Web()
{
state1:
	goto state2
state2:
	if
	:: C_Web?1 -> goto state3
	fi
state3:
	if
	:: Web_C!4 -> goto state4
	:: Web_BE1!1 -> goto state5
	:: Web_BE2!1 -> goto state5
	:: Web_BE3!1 -> goto state5
	:: Web_BE4!1 -> goto state5
	:: Web_BE5!1 -> goto state5
	:: Web_BE6!1 -> goto state5
	:: Web_BE7!1 -> goto state5
	:: Web_BE8!1 -> goto state5
	:: Web_BE9!1 -> goto state5
	:: Web_BE10!1 -> goto state5
	:: Web_BE11!1 -> goto state5
	:: Web_BE12!1 -> goto state5
	:: Web_BE13!1 -> goto state5
	:: Web_BE14!1 -> goto state5
	:: Web_BE15!1 -> goto state5
	:: Web_BE16!1 -> goto state5
	:: Web_BE17!1 -> goto state5
	fi
state4:
	if
	:: Web_Log!2 -> goto state6
	fi
state6:
	if
	:: Log_Web?3 -> goto state5
	fi
state5:
	goto state2
} /* Web */
