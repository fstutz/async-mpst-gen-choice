chan BE3_Log = [1] of { int };
chan BE4_Log = [1] of { int };
chan BE1_Log = [1] of { int };
chan Web_C = [1] of { int };
chan BE2_C = [1] of { int };
chan Log_BE2 = [1] of { int };
chan Web_Log = [1] of { int };
chan Log_Web = [1] of { int };
chan Web_BE3 = [1] of { int };
chan Web_BE4 = [1] of { int };
chan Web_BE1 = [1] of { int };
chan C_Web = [1] of { int };
chan BE2_Log = [1] of { int };
chan Log_BE1 = [1] of { int };
chan BE3_C = [1] of { int };
chan BE4_C = [1] of { int };
chan Log_BE3 = [1] of { int };
chan Log_BE4 = [1] of { int };
chan BE1_C = [1] of { int };
chan Web_BE2 = [1] of { int };

init
{
	run BE1();
	run BE2();
	run BE3();
	run BE4();
	run C();
	run Log();
	run Web()
}

proctype BE1()
{
state1:
	goto state2
state2:
	if
	:: Web_BE1?1 -> goto state3
	fi
state3:
	if
	:: BE1_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE1?3 -> goto state5
	fi
state5:
	if
	:: BE1_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE1 */

proctype BE2()
{
state1:
	goto state2
state2:
	if
	:: Web_BE2?1 -> goto state3
	fi
state3:
	if
	:: BE2_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE2?3 -> goto state5
	fi
state5:
	if
	:: BE2_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE2 */

proctype BE3()
{
state1:
	goto state2
state2:
	if
	:: Web_BE3?1 -> goto state3
	fi
state3:
	if
	:: BE3_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE3?3 -> goto state5
	fi
state5:
	if
	:: BE3_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE3 */

proctype BE4()
{
state1:
	goto state2
state2:
	if
	:: Web_BE4?1 -> goto state3
	fi
state3:
	if
	:: BE4_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE4?3 -> goto state5
	fi
state5:
	if
	:: BE4_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE4 */

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_Web!1 -> goto state3
	fi
state3:
	if
	:: BE1_C?4 -> goto state4
	:: Web_C?4 -> goto state4
	:: BE2_C?4 -> goto state4
	:: BE3_C?4 -> goto state4
	:: BE4_C?4 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype Log()
{
state1:
	goto state2
state2:
	if
	:: BE2_Log?2 -> goto state3
	:: Web_Log?2 -> goto state4
	:: BE1_Log?2 -> goto state5
	:: BE3_Log?2 -> goto state6
	:: BE4_Log?2 -> goto state7
	fi
state3:
	if
	:: Log_BE2!3 -> goto state8
	fi
state8:
	goto state2
state4:
	if
	:: Log_Web!3 -> goto state8
	fi
state5:
	if
	:: Log_BE1!3 -> goto state8
	fi
state6:
	if
	:: Log_BE3!3 -> goto state8
	fi
state7:
	if
	:: Log_BE4!3 -> goto state8
	fi
} /* Log */

proctype Web()
{
state1:
	goto state2
state2:
	if
	:: C_Web?1 -> goto state3
	fi
state3:
	if
	:: Web_C!4 -> goto state4
	:: Web_BE1!1 -> goto state5
	:: Web_BE2!1 -> goto state5
	:: Web_BE3!1 -> goto state5
	:: Web_BE4!1 -> goto state5
	fi
state4:
	if
	:: Web_Log!2 -> goto state6
	fi
state6:
	if
	:: Log_Web?3 -> goto state5
	fi
state5:
	goto state2
} /* Web */
