chan Web_BE1 = [1] of { int };
chan C_Web = [1] of { int };
chan BE1_Log = [1] of { int };
chan Web_C = [1] of { int };
chan Log_BE1 = [1] of { int };
chan Web_Log = [1] of { int };
chan Log_Web = [1] of { int };
chan BE1_C = [1] of { int };

init
{
	run BE1();
	run C();
	run Log();
	run Web()
}

proctype BE1()
{
state1:
	goto state2
state2:
	if
	:: Web_BE1?1 -> goto state3
	fi
state3:
	if
	:: BE1_Log!2 -> goto state4
	fi
state4:
	if
	:: Log_BE1?3 -> goto state5
	fi
state5:
	if
	:: BE1_C!4 -> goto state6
	fi
state6:
	goto state2
} /* BE1 */

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_Web!1 -> goto state3
	fi
state3:
	if
	:: Web_C?4 -> goto state4
	:: BE1_C?4 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype Log()
{
state1:
	goto state2
state2:
	if
	:: Web_Log?2 -> goto state3
	:: BE1_Log?2 -> goto state4
	fi
state3:
	if
	:: Log_Web!3 -> goto state5
	fi
state5:
	goto state2
state4:
	if
	:: Log_BE1!3 -> goto state5
	fi
} /* Log */

proctype Web()
{
state1:
	goto state2
state2:
	if
	:: C_Web?1 -> goto state3
	fi
state3:
	if
	:: Web_C!4 -> goto state4
	:: Web_BE1!1 -> goto state5
	fi
state4:
	if
	:: Web_Log!2 -> goto state6
	fi
state6:
	if
	:: Log_Web?3 -> goto state5
	fi
state5:
	goto state2
} /* Web */
