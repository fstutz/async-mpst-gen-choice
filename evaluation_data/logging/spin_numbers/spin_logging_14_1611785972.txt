> promela file    : ../logging/spin_code/logging_14.pml
> date            : 27-Jan-2021 23:19:33
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../logging/spin_code/logging_14.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 692 byte, depth reached 26, errors: 0
      131 states, stored
       23 states, matched
      154 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.090	equivalent memory usage for states (stored*(State-vector + overhead))
    1.125	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  538.839	total actual memory usage



pan: elapsed time 0 seconds

real	0m0.266s
user	0m0.044s
sys	0m0.219s
