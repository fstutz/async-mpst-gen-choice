> promela file    : ../logging/spin_code/logging_56.pml
> date            : 27-Jan-2021 21:07:06
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../logging/spin_code/logging_56.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m1000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 2540 byte, depth reached 68, errors: 0
      467 states, stored
       65 states, matched
      532 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    1.144	equivalent memory usage for states (stored*(State-vector + overhead))
    1.780	actual memory usage for states
    4.000	memory used for hash table (-w19)
   53.406	memory used for DFS stack (-m1000000)
   58.968	total actual memory usage



pan: elapsed time 0 seconds

real	0m0.051s
user	0m0.016s
sys	0m0.022s
