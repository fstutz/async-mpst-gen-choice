> promela file    : ../logging/spin_code/logging_52.pml
> date            : 27-Jan-2021 23:20:24
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../logging/spin_code/logging_52.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 2364 byte, depth reached 64, errors: 0
      435 states, stored
       61 states, matched
      496 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         1 (resolved)

Stats on memory usage (in Megabytes):
    0.992	equivalent memory usage for states (stored*(State-vector + overhead))
    1.793	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  539.620	total actual memory usage



pan: elapsed time 0.01 seconds

real	0m0.281s
user	0m0.077s
sys	0m0.193s
