#!/bin/bash

prefix="../"
pmlmidfix="/spin_code/"
nummidfix="/spin_numbers/"
begin=1
last=140
pmlpref="$prefix$1$pmlmidfix"
numpref="$prefix$1$nummidfix"
filename=$1

if [ "$1" = "map_reduce" ];
then
	filename="$1_k2"
	last=6
elif [ "$1" = "p2p_broadcast" ];
then
	last=8
	begin=2
elif [ "$1" == "tree_broadcast" ];
then
	last=15
	begin=2
elif [[ ("$1" = "load_balancer" || "$1" = "logging" || "$1" = "mem_cache") ]];
then
	:
else
	echo "No such parametric example!"
	exit 1
fi

for (( c=1;  c<=9; c++))
do
	for (( i=begin;  i<=$last; i++))
	do
		nowtime=$(($(date +%s%N)/1000000000))
		outfile="${numpref}spin_${filename}_${i}_${nowtime}.txt"
		timeout 10m runspin "${pmlpref}${filename}_${i}.pml" > $outfile
		if [ $? -eq 124 ];
		then
			echo "timed out" > $outfile
			echo "Timed out for ${1} at ${i}"
			break
		fi
	done
done
