> promela file    : ../mem_cache/spin_code/mem_cache_124.pml
> date            : 27-Jan-2021 23:31:25
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../mem_cache/spin_code/mem_cache_124.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 3268 byte, depth reached 132, errors: 0
      503 states, stored
      125 states, matched
      628 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    1.581	equivalent memory usage for states (stored*(State-vector + overhead))
    2.130	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  540.011	total actual memory usage



pan: elapsed time 0.02 seconds
pan: rate     25150 states/second

real	0m0.291s
user	0m0.045s
sys	0m0.230s
