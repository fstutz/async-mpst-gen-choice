> promela file    : ../mem_cache/spin_code/mem_cache_123.pml
> date            : 27-Jan-2021 23:46:12
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../mem_cache/spin_code/mem_cache_123.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 3244 byte, depth reached 131, errors: 0
      499 states, stored
      124 states, matched
      623 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    1.557	equivalent memory usage for states (stored*(State-vector + overhead))
    2.129	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  540.011	total actual memory usage



pan: elapsed time 0.01 seconds

real	0m0.298s
user	0m0.052s
sys	0m0.226s
