chan Mem1_Cache = [1] of { int };
chan Mem2_Cache = [1] of { int };
chan Cache_Mem1 = [1] of { int };
chan Cache_Mem2 = [1] of { int };
chan Cache_Mem3 = [1] of { int };
chan Mem3_Cache = [1] of { int };
chan Cache_Mem4 = [1] of { int };
chan Mem4_Cache = [1] of { int };
chan Cache_Client = [1] of { int };
chan Client_Cache = [1] of { int };

init
{
	run Cache();
	run Client();
	run Mem1();
	run Mem2();
	run Mem3();
	run Mem4()
}

proctype Cache()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache?1 -> goto state3
	fi
state3:
	if
	:: Cache_Client!2 -> goto state4
	:: Cache_Mem1!1 -> goto state5
	:: Cache_Mem2!1 -> goto state6
	:: Cache_Mem3!1 -> goto state7
	:: Cache_Mem4!1 -> goto state8
	fi
state4:
	goto state2
state5:
	if
	:: Mem1_Cache?2 -> goto state9
	fi
state9:
	if
	:: Cache_Client!2 -> goto state4
	fi
state6:
	if
	:: Mem2_Cache?2 -> goto state9
	fi
state7:
	if
	:: Mem3_Cache?2 -> goto state9
	fi
state8:
	if
	:: Mem4_Cache?2 -> goto state9
	fi
} /* Cache */

proctype Client()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache!1 -> goto state3
	fi
state3:
	if
	:: Cache_Client?2 -> goto state4
	fi
state4:
	goto state2
} /* Client */

proctype Mem1()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem1?1 -> goto state3
	fi
state3:
	if
	:: Mem1_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem1 */

proctype Mem2()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem2?1 -> goto state3
	fi
state3:
	if
	:: Mem2_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem2 */

proctype Mem3()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem3?1 -> goto state3
	fi
state3:
	if
	:: Mem3_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem3 */

proctype Mem4()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem4?1 -> goto state3
	fi
state3:
	if
	:: Mem4_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem4 */
