chan Cache_Mem25 = [1] of { int };
chan Cache_Mem10 = [1] of { int };
chan Cache_Mem33 = [1] of { int };
chan Mem3_Cache = [1] of { int };
chan Cache_Mem35 = [1] of { int };
chan Cache_Mem4 = [1] of { int };
chan Cache_Mem11 = [1] of { int };
chan Mem15_Cache = [1] of { int };
chan Cache_Mem17 = [1] of { int };
chan Mem19_Cache = [1] of { int };
chan Mem29_Cache = [1] of { int };
chan Mem13_Cache = [1] of { int };
chan Mem33_Cache = [1] of { int };
chan Cache_Mem5 = [1] of { int };
chan Mem6_Cache = [1] of { int };
chan Cache_Mem14 = [1] of { int };
chan Cache_Mem9 = [1] of { int };
chan Mem21_Cache = [1] of { int };
chan Cache_Mem1 = [1] of { int };
chan Cache_Mem2 = [1] of { int };
chan Cache_Mem3 = [1] of { int };
chan Cache_Mem20 = [1] of { int };
chan Mem7_Cache = [1] of { int };
chan Mem20_Cache = [1] of { int };
chan Mem23_Cache = [1] of { int };
chan Mem22_Cache = [1] of { int };
chan Mem17_Cache = [1] of { int };
chan Cache_Mem32 = [1] of { int };
chan Mem28_Cache = [1] of { int };
chan Mem4_Cache = [1] of { int };
chan Mem27_Cache = [1] of { int };
chan Mem32_Cache = [1] of { int };
chan Mem36_Cache = [1] of { int };
chan Cache_Mem19 = [1] of { int };
chan Cache_Mem30 = [1] of { int };
chan Cache_Mem28 = [1] of { int };
chan Cache_Mem8 = [1] of { int };
chan Cache_Mem21 = [1] of { int };
chan Mem8_Cache = [1] of { int };
chan Cache_Mem26 = [1] of { int };
chan Cache_Mem7 = [1] of { int };
chan Cache_Mem34 = [1] of { int };
chan Cache_Mem6 = [1] of { int };
chan Cache_Mem23 = [1] of { int };
chan Mem18_Cache = [1] of { int };
chan Mem35_Cache = [1] of { int };
chan Client_Cache = [1] of { int };
chan Cache_Mem16 = [1] of { int };
chan Cache_Mem38 = [1] of { int };
chan Cache_Mem27 = [1] of { int };
chan Mem24_Cache = [1] of { int };
chan Mem1_Cache = [1] of { int };
chan Mem2_Cache = [1] of { int };
chan Cache_Mem15 = [1] of { int };
chan Cache_Mem37 = [1] of { int };
chan Mem37_Cache = [1] of { int };
chan Mem10_Cache = [1] of { int };
chan Cache_Mem13 = [1] of { int };
chan Mem9_Cache = [1] of { int };
chan Mem14_Cache = [1] of { int };
chan Mem5_Cache = [1] of { int };
chan Cache_Mem18 = [1] of { int };
chan Cache_Mem31 = [1] of { int };
chan Mem25_Cache = [1] of { int };
chan Cache_Mem36 = [1] of { int };
chan Cache_Client = [1] of { int };
chan Cache_Mem12 = [1] of { int };
chan Mem12_Cache = [1] of { int };
chan Mem16_Cache = [1] of { int };
chan Mem26_Cache = [1] of { int };
chan Mem38_Cache = [1] of { int };
chan Cache_Mem22 = [1] of { int };
chan Mem31_Cache = [1] of { int };
chan Cache_Mem29 = [1] of { int };
chan Cache_Mem24 = [1] of { int };
chan Mem34_Cache = [1] of { int };
chan Mem30_Cache = [1] of { int };
chan Mem11_Cache = [1] of { int };

init
{
	run Cache();
	run Client();
	run Mem1();
	run Mem10();
	run Mem11();
	run Mem12();
	run Mem13();
	run Mem14();
	run Mem15();
	run Mem16();
	run Mem17();
	run Mem18();
	run Mem19();
	run Mem2();
	run Mem20();
	run Mem21();
	run Mem22();
	run Mem23();
	run Mem24();
	run Mem25();
	run Mem26();
	run Mem27();
	run Mem28();
	run Mem29();
	run Mem3();
	run Mem30();
	run Mem31();
	run Mem32();
	run Mem33();
	run Mem34();
	run Mem35();
	run Mem36();
	run Mem37();
	run Mem38();
	run Mem4();
	run Mem5();
	run Mem6();
	run Mem7();
	run Mem8();
	run Mem9()
}

proctype Cache()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache?1 -> goto state3
	fi
state3:
	if
	:: Cache_Client!2 -> goto state4
	:: Cache_Mem1!1 -> goto state5
	:: Cache_Mem2!1 -> goto state6
	:: Cache_Mem3!1 -> goto state7
	:: Cache_Mem4!1 -> goto state8
	:: Cache_Mem5!1 -> goto state9
	:: Cache_Mem6!1 -> goto state10
	:: Cache_Mem7!1 -> goto state11
	:: Cache_Mem8!1 -> goto state12
	:: Cache_Mem9!1 -> goto state13
	:: Cache_Mem10!1 -> goto state14
	:: Cache_Mem11!1 -> goto state15
	:: Cache_Mem12!1 -> goto state16
	:: Cache_Mem13!1 -> goto state17
	:: Cache_Mem14!1 -> goto state18
	:: Cache_Mem15!1 -> goto state19
	:: Cache_Mem16!1 -> goto state20
	:: Cache_Mem17!1 -> goto state21
	:: Cache_Mem18!1 -> goto state22
	:: Cache_Mem19!1 -> goto state23
	:: Cache_Mem20!1 -> goto state24
	:: Cache_Mem21!1 -> goto state25
	:: Cache_Mem22!1 -> goto state26
	:: Cache_Mem23!1 -> goto state27
	:: Cache_Mem24!1 -> goto state28
	:: Cache_Mem25!1 -> goto state29
	:: Cache_Mem26!1 -> goto state30
	:: Cache_Mem27!1 -> goto state31
	:: Cache_Mem28!1 -> goto state32
	:: Cache_Mem29!1 -> goto state33
	:: Cache_Mem30!1 -> goto state34
	:: Cache_Mem31!1 -> goto state35
	:: Cache_Mem32!1 -> goto state36
	:: Cache_Mem33!1 -> goto state37
	:: Cache_Mem34!1 -> goto state38
	:: Cache_Mem35!1 -> goto state39
	:: Cache_Mem36!1 -> goto state40
	:: Cache_Mem37!1 -> goto state41
	:: Cache_Mem38!1 -> goto state42
	fi
state4:
	goto state2
state5:
	if
	:: Mem1_Cache?2 -> goto state43
	fi
state43:
	if
	:: Cache_Client!2 -> goto state4
	fi
state6:
	if
	:: Mem2_Cache?2 -> goto state43
	fi
state7:
	if
	:: Mem3_Cache?2 -> goto state43
	fi
state8:
	if
	:: Mem4_Cache?2 -> goto state43
	fi
state9:
	if
	:: Mem5_Cache?2 -> goto state43
	fi
state10:
	if
	:: Mem6_Cache?2 -> goto state43
	fi
state11:
	if
	:: Mem7_Cache?2 -> goto state43
	fi
state12:
	if
	:: Mem8_Cache?2 -> goto state43
	fi
state13:
	if
	:: Mem9_Cache?2 -> goto state43
	fi
state14:
	if
	:: Mem10_Cache?2 -> goto state43
	fi
state15:
	if
	:: Mem11_Cache?2 -> goto state43
	fi
state16:
	if
	:: Mem12_Cache?2 -> goto state43
	fi
state17:
	if
	:: Mem13_Cache?2 -> goto state43
	fi
state18:
	if
	:: Mem14_Cache?2 -> goto state43
	fi
state19:
	if
	:: Mem15_Cache?2 -> goto state43
	fi
state20:
	if
	:: Mem16_Cache?2 -> goto state43
	fi
state21:
	if
	:: Mem17_Cache?2 -> goto state43
	fi
state22:
	if
	:: Mem18_Cache?2 -> goto state43
	fi
state23:
	if
	:: Mem19_Cache?2 -> goto state43
	fi
state24:
	if
	:: Mem20_Cache?2 -> goto state43
	fi
state25:
	if
	:: Mem21_Cache?2 -> goto state43
	fi
state26:
	if
	:: Mem22_Cache?2 -> goto state43
	fi
state27:
	if
	:: Mem23_Cache?2 -> goto state43
	fi
state28:
	if
	:: Mem24_Cache?2 -> goto state43
	fi
state29:
	if
	:: Mem25_Cache?2 -> goto state43
	fi
state30:
	if
	:: Mem26_Cache?2 -> goto state43
	fi
state31:
	if
	:: Mem27_Cache?2 -> goto state43
	fi
state32:
	if
	:: Mem28_Cache?2 -> goto state43
	fi
state33:
	if
	:: Mem29_Cache?2 -> goto state43
	fi
state34:
	if
	:: Mem30_Cache?2 -> goto state43
	fi
state35:
	if
	:: Mem31_Cache?2 -> goto state43
	fi
state36:
	if
	:: Mem32_Cache?2 -> goto state43
	fi
state37:
	if
	:: Mem33_Cache?2 -> goto state43
	fi
state38:
	if
	:: Mem34_Cache?2 -> goto state43
	fi
state39:
	if
	:: Mem35_Cache?2 -> goto state43
	fi
state40:
	if
	:: Mem36_Cache?2 -> goto state43
	fi
state41:
	if
	:: Mem37_Cache?2 -> goto state43
	fi
state42:
	if
	:: Mem38_Cache?2 -> goto state43
	fi
} /* Cache */

proctype Client()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache!1 -> goto state3
	fi
state3:
	if
	:: Cache_Client?2 -> goto state4
	fi
state4:
	goto state2
} /* Client */

proctype Mem1()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem1?1 -> goto state3
	fi
state3:
	if
	:: Mem1_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem1 */

proctype Mem10()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem10?1 -> goto state3
	fi
state3:
	if
	:: Mem10_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem10 */

proctype Mem11()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem11?1 -> goto state3
	fi
state3:
	if
	:: Mem11_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem11 */

proctype Mem12()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem12?1 -> goto state3
	fi
state3:
	if
	:: Mem12_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem12 */

proctype Mem13()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem13?1 -> goto state3
	fi
state3:
	if
	:: Mem13_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem13 */

proctype Mem14()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem14?1 -> goto state3
	fi
state3:
	if
	:: Mem14_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem14 */

proctype Mem15()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem15?1 -> goto state3
	fi
state3:
	if
	:: Mem15_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem15 */

proctype Mem16()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem16?1 -> goto state3
	fi
state3:
	if
	:: Mem16_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem16 */

proctype Mem17()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem17?1 -> goto state3
	fi
state3:
	if
	:: Mem17_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem17 */

proctype Mem18()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem18?1 -> goto state3
	fi
state3:
	if
	:: Mem18_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem18 */

proctype Mem19()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem19?1 -> goto state3
	fi
state3:
	if
	:: Mem19_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem19 */

proctype Mem2()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem2?1 -> goto state3
	fi
state3:
	if
	:: Mem2_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem2 */

proctype Mem20()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem20?1 -> goto state3
	fi
state3:
	if
	:: Mem20_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem20 */

proctype Mem21()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem21?1 -> goto state3
	fi
state3:
	if
	:: Mem21_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem21 */

proctype Mem22()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem22?1 -> goto state3
	fi
state3:
	if
	:: Mem22_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem22 */

proctype Mem23()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem23?1 -> goto state3
	fi
state3:
	if
	:: Mem23_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem23 */

proctype Mem24()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem24?1 -> goto state3
	fi
state3:
	if
	:: Mem24_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem24 */

proctype Mem25()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem25?1 -> goto state3
	fi
state3:
	if
	:: Mem25_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem25 */

proctype Mem26()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem26?1 -> goto state3
	fi
state3:
	if
	:: Mem26_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem26 */

proctype Mem27()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem27?1 -> goto state3
	fi
state3:
	if
	:: Mem27_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem27 */

proctype Mem28()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem28?1 -> goto state3
	fi
state3:
	if
	:: Mem28_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem28 */

proctype Mem29()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem29?1 -> goto state3
	fi
state3:
	if
	:: Mem29_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem29 */

proctype Mem3()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem3?1 -> goto state3
	fi
state3:
	if
	:: Mem3_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem3 */

proctype Mem30()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem30?1 -> goto state3
	fi
state3:
	if
	:: Mem30_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem30 */

proctype Mem31()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem31?1 -> goto state3
	fi
state3:
	if
	:: Mem31_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem31 */

proctype Mem32()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem32?1 -> goto state3
	fi
state3:
	if
	:: Mem32_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem32 */

proctype Mem33()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem33?1 -> goto state3
	fi
state3:
	if
	:: Mem33_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem33 */

proctype Mem34()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem34?1 -> goto state3
	fi
state3:
	if
	:: Mem34_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem34 */

proctype Mem35()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem35?1 -> goto state3
	fi
state3:
	if
	:: Mem35_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem35 */

proctype Mem36()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem36?1 -> goto state3
	fi
state3:
	if
	:: Mem36_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem36 */

proctype Mem37()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem37?1 -> goto state3
	fi
state3:
	if
	:: Mem37_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem37 */

proctype Mem38()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem38?1 -> goto state3
	fi
state3:
	if
	:: Mem38_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem38 */

proctype Mem4()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem4?1 -> goto state3
	fi
state3:
	if
	:: Mem4_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem4 */

proctype Mem5()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem5?1 -> goto state3
	fi
state3:
	if
	:: Mem5_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem5 */

proctype Mem6()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem6?1 -> goto state3
	fi
state3:
	if
	:: Mem6_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem6 */

proctype Mem7()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem7?1 -> goto state3
	fi
state3:
	if
	:: Mem7_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem7 */

proctype Mem8()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem8?1 -> goto state3
	fi
state3:
	if
	:: Mem8_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem8 */

proctype Mem9()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem9?1 -> goto state3
	fi
state3:
	if
	:: Mem9_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem9 */
