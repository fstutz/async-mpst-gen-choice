chan Cache_Mem1 = [1] of { int };
chan Cache_Client = [1] of { int };
chan Client_Cache = [1] of { int };
chan Mem1_Cache = [1] of { int };

init
{
	run Cache();
	run Client();
	run Mem1()
}

proctype Cache()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache?1 -> goto state3
	fi
state3:
	if
	:: Cache_Client!2 -> goto state4
	:: Cache_Mem1!1 -> goto state5
	fi
state4:
	goto state2
state5:
	if
	:: Mem1_Cache?2 -> goto state6
	fi
state6:
	if
	:: Cache_Client!2 -> goto state4
	fi
} /* Cache */

proctype Client()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache!1 -> goto state3
	fi
state3:
	if
	:: Cache_Client?2 -> goto state4
	fi
state4:
	goto state2
} /* Client */

proctype Mem1()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem1?1 -> goto state3
	fi
state3:
	if
	:: Mem1_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem1 */
