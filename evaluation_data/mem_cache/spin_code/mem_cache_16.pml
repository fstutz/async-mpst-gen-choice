chan Cache_Mem7 = [1] of { int };
chan Cache_Mem6 = [1] of { int };
chan Cache_Mem10 = [1] of { int };
chan Mem3_Cache = [1] of { int };
chan Cache_Mem4 = [1] of { int };
chan Cache_Mem11 = [1] of { int };
chan Mem15_Cache = [1] of { int };
chan Client_Cache = [1] of { int };
chan Cache_Mem16 = [1] of { int };
chan Mem1_Cache = [1] of { int };
chan Mem2_Cache = [1] of { int };
chan Mem13_Cache = [1] of { int };
chan Cache_Mem15 = [1] of { int };
chan Cache_Mem5 = [1] of { int };
chan Mem10_Cache = [1] of { int };
chan Mem6_Cache = [1] of { int };
chan Cache_Mem13 = [1] of { int };
chan Cache_Mem14 = [1] of { int };
chan Cache_Mem9 = [1] of { int };
chan Mem9_Cache = [1] of { int };
chan Mem14_Cache = [1] of { int };
chan Mem5_Cache = [1] of { int };
chan Cache_Mem1 = [1] of { int };
chan Cache_Mem2 = [1] of { int };
chan Cache_Mem3 = [1] of { int };
chan Mem7_Cache = [1] of { int };
chan Cache_Client = [1] of { int };
chan Cache_Mem12 = [1] of { int };
chan Mem12_Cache = [1] of { int };
chan Mem16_Cache = [1] of { int };
chan Mem4_Cache = [1] of { int };
chan Mem11_Cache = [1] of { int };
chan Cache_Mem8 = [1] of { int };
chan Mem8_Cache = [1] of { int };

init
{
	run Cache();
	run Client();
	run Mem1();
	run Mem10();
	run Mem11();
	run Mem12();
	run Mem13();
	run Mem14();
	run Mem15();
	run Mem16();
	run Mem2();
	run Mem3();
	run Mem4();
	run Mem5();
	run Mem6();
	run Mem7();
	run Mem8();
	run Mem9()
}

proctype Cache()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache?1 -> goto state3
	fi
state3:
	if
	:: Cache_Client!2 -> goto state4
	:: Cache_Mem1!1 -> goto state5
	:: Cache_Mem2!1 -> goto state6
	:: Cache_Mem3!1 -> goto state7
	:: Cache_Mem4!1 -> goto state8
	:: Cache_Mem5!1 -> goto state9
	:: Cache_Mem6!1 -> goto state10
	:: Cache_Mem7!1 -> goto state11
	:: Cache_Mem8!1 -> goto state12
	:: Cache_Mem9!1 -> goto state13
	:: Cache_Mem10!1 -> goto state14
	:: Cache_Mem11!1 -> goto state15
	:: Cache_Mem12!1 -> goto state16
	:: Cache_Mem13!1 -> goto state17
	:: Cache_Mem14!1 -> goto state18
	:: Cache_Mem15!1 -> goto state19
	:: Cache_Mem16!1 -> goto state20
	fi
state4:
	goto state2
state5:
	if
	:: Mem1_Cache?2 -> goto state21
	fi
state21:
	if
	:: Cache_Client!2 -> goto state4
	fi
state6:
	if
	:: Mem2_Cache?2 -> goto state21
	fi
state7:
	if
	:: Mem3_Cache?2 -> goto state21
	fi
state8:
	if
	:: Mem4_Cache?2 -> goto state21
	fi
state9:
	if
	:: Mem5_Cache?2 -> goto state21
	fi
state10:
	if
	:: Mem6_Cache?2 -> goto state21
	fi
state11:
	if
	:: Mem7_Cache?2 -> goto state21
	fi
state12:
	if
	:: Mem8_Cache?2 -> goto state21
	fi
state13:
	if
	:: Mem9_Cache?2 -> goto state21
	fi
state14:
	if
	:: Mem10_Cache?2 -> goto state21
	fi
state15:
	if
	:: Mem11_Cache?2 -> goto state21
	fi
state16:
	if
	:: Mem12_Cache?2 -> goto state21
	fi
state17:
	if
	:: Mem13_Cache?2 -> goto state21
	fi
state18:
	if
	:: Mem14_Cache?2 -> goto state21
	fi
state19:
	if
	:: Mem15_Cache?2 -> goto state21
	fi
state20:
	if
	:: Mem16_Cache?2 -> goto state21
	fi
} /* Cache */

proctype Client()
{
state1:
	goto state2
state2:
	if
	:: Client_Cache!1 -> goto state3
	fi
state3:
	if
	:: Cache_Client?2 -> goto state4
	fi
state4:
	goto state2
} /* Client */

proctype Mem1()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem1?1 -> goto state3
	fi
state3:
	if
	:: Mem1_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem1 */

proctype Mem10()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem10?1 -> goto state3
	fi
state3:
	if
	:: Mem10_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem10 */

proctype Mem11()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem11?1 -> goto state3
	fi
state3:
	if
	:: Mem11_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem11 */

proctype Mem12()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem12?1 -> goto state3
	fi
state3:
	if
	:: Mem12_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem12 */

proctype Mem13()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem13?1 -> goto state3
	fi
state3:
	if
	:: Mem13_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem13 */

proctype Mem14()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem14?1 -> goto state3
	fi
state3:
	if
	:: Mem14_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem14 */

proctype Mem15()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem15?1 -> goto state3
	fi
state3:
	if
	:: Mem15_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem15 */

proctype Mem16()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem16?1 -> goto state3
	fi
state3:
	if
	:: Mem16_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem16 */

proctype Mem2()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem2?1 -> goto state3
	fi
state3:
	if
	:: Mem2_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem2 */

proctype Mem3()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem3?1 -> goto state3
	fi
state3:
	if
	:: Mem3_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem3 */

proctype Mem4()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem4?1 -> goto state3
	fi
state3:
	if
	:: Mem4_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem4 */

proctype Mem5()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem5?1 -> goto state3
	fi
state3:
	if
	:: Mem5_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem5 */

proctype Mem6()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem6?1 -> goto state3
	fi
state3:
	if
	:: Mem6_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem6 */

proctype Mem7()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem7?1 -> goto state3
	fi
state3:
	if
	:: Mem7_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem7 */

proctype Mem8()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem8?1 -> goto state3
	fi
state3:
	if
	:: Mem8_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem8 */

proctype Mem9()
{
state1:
	goto state2
state2:
	if
	:: Cache_Mem9?1 -> goto state3
	fi
state3:
	if
	:: Mem9_Cache!2 -> goto state4
	fi
state4:
	goto state2
} /* Mem9 */
