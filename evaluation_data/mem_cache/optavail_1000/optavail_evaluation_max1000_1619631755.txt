#Size	#Para	 Time		 Diff		 Proc		 Diff
8		1	     0.308	     0.308	     0.103	     0.103
12		2	     0.351	     0.043	     0.088	    -0.015
16		3	     0.619	     0.268	     0.124	     0.036
20		4	     0.695	     0.076	     0.116	    -0.008
24		5	     0.942	     0.247	     0.135	     0.019
28		6	     1.193	     0.250	     0.149	     0.014
32		7	     1.540	     0.347	     0.171	     0.022
36		8	     1.794	     0.255	     0.179	     0.008
40		9	     2.191	     0.397	     0.199	     0.020
44		10	     3.379	     1.188	     0.282	     0.082
48		11	     2.933	    -0.446	     0.226	    -0.056
52		12	     3.369	     0.436	     0.241	     0.015
56		13	     3.792	     0.423	     0.253	     0.012
60		14	     4.380	     0.587	     0.274	     0.021
64		15	     5.889	     1.509	     0.346	     0.073
68		16	     5.430	    -0.459	     0.302	    -0.045
72		17	     5.999	     0.569	     0.316	     0.014
76		18	     6.681	     0.682	     0.334	     0.018
80		19	    17.071	    10.390	     0.813	     0.479
84		20	     8.054	    -9.017	     0.366	    -0.447
88		21	     8.690	     0.637	     0.378	     0.012
92		22	    10.432	     1.741	     0.435	     0.057
96		23	    10.238	    -0.193	     0.410	    -0.025
100		24	    10.998	     0.760	     0.423	     0.013
104		25	    11.878	     0.880	     0.440	     0.017
108		26	    12.707	     0.828	     0.454	     0.014
112		27	    13.721	     1.015	     0.473	     0.019
116		28	    14.781	     1.059	     0.493	     0.020
120		29	    16.411	     1.631	     0.529	     0.037
124		30	    16.549	     0.137	     0.517	    -0.012
128		31	    17.538	     0.989	     0.531	     0.014
132		32	    18.661	     1.123	     0.549	     0.017
136		33	    19.640	     0.979	     0.561	     0.012
140		34	    20.911	     1.271	     0.581	     0.020
144		35	    21.988	     1.077	     0.594	     0.013
148		36	    23.248	     1.260	     0.612	     0.018
152		37	    24.353	     1.105	     0.624	     0.013
156		38	    25.700	     1.348	     0.643	     0.018
160		39	    27.534	     1.834	     0.672	     0.029
164		40	    27.972	     0.438	     0.666	    -0.006
168		41	    29.678	     1.706	     0.690	     0.024
172		42	    31.504	     1.826	     0.716	     0.026
176		43	    32.868	     1.364	     0.730	     0.014
180		44	    33.607	     0.739	     0.731	     0.000
184		45	    36.069	     2.462	     0.767	     0.037
188		46	    36.714	     0.645	     0.765	    -0.003
192		47	    37.969	     1.255	     0.775	     0.010
196		48	    40.506	     2.537	     0.810	     0.035
200		49	    41.882	     1.376	     0.821	     0.011
204		50	    46.242	     4.360	     0.889	     0.068
208		51	    44.312	    -1.930	     0.836	    -0.053
212		52	    46.424	     2.111	     0.860	     0.024
216		53	    48.027	     1.603	     0.873	     0.014
220		54	    49.308	     1.281	     0.881	     0.007
224		55	    51.680	     2.372	     0.907	     0.026
228		56	    53.674	     1.994	     0.925	     0.019
232		57	    55.135	     1.462	     0.934	     0.009
236		58	    56.545	     1.410	     0.942	     0.008
240		59	    59.069	     2.524	     0.968	     0.026
244		60	    60.971	     1.902	     0.983	     0.015
248		61	    63.131	     2.160	     1.002	     0.019
252		62	    65.597	     2.465	     1.025	     0.023
256		63	    67.122	     1.526	     1.033	     0.008
260		64	    97.099	    29.977	     1.471	     0.439
264		65	    70.760	   -26.339	     1.056	    -0.415
268		66	    73.223	     2.463	     1.077	     0.021
272		67	    79.397	     6.174	     1.151	     0.074
276		68	    88.390	     8.993	     1.263	     0.112
280		69	    82.581	    -5.809	     1.163	    -0.100
284		70	    82.972	     0.391	     1.152	    -0.011
288		71	    86.454	     3.482	     1.184	     0.032
292		72	    88.807	     2.352	     1.200	     0.016
296		73	    90.569	     1.762	     1.208	     0.007
300		74	    93.715	     3.146	     1.233	     0.026
304		75	    95.447	     1.732	     1.240	     0.006
308		76	    97.790	     2.343	     1.254	     0.014
312		77	   101.720	     3.930	     1.288	     0.034
316		78	   102.528	     0.808	     1.282	    -0.006
320		79	   105.626	     3.098	     1.304	     0.022
324		80	   108.028	     2.402	     1.317	     0.013
328		81	   112.495	     4.467	     1.355	     0.038
332		82	   114.564	     2.069	     1.364	     0.008
336		83	   116.668	     2.104	     1.373	     0.009
340		84	   119.597	     2.929	     1.391	     0.018
344		85	   121.494	     1.898	     1.396	     0.006
348		86	   124.949	     3.455	     1.420	     0.023
352		87	   127.107	     2.158	     1.428	     0.008
356		88	   130.074	     2.967	     1.445	     0.017
360		89	   134.453	     4.379	     1.478	     0.032
364		90	   137.029	     2.576	     1.489	     0.012
368		91	   140.225	     3.197	     1.508	     0.018
372		92	   142.128	     1.903	     1.512	     0.004
376		93	   147.156	     5.028	     1.549	     0.037
380		94	   182.212	    35.057	     1.898	     0.349
384		95	   156.617	   -25.595	     1.615	    -0.283
388		96	   158.102	     1.485	     1.613	    -0.001
392		97	   161.921	     3.819	     1.636	     0.022
396		98	   166.186	     4.265	     1.662	     0.026
400		99	   168.777	     2.591	     1.671	     0.009
404		100	   198.683	    29.905	     1.948	     0.277
408		101	   176.373	   -22.310	     1.712	    -0.236
412		102	   179.950	     3.577	     1.730	     0.018
416		103	   183.105	     3.156	     1.744	     0.014
420		104	   185.575	     2.469	     1.751	     0.007
424		105	   241.992	    56.418	     2.262	     0.511
428		106	   193.457	   -48.536	     1.791	    -0.470
432		107	   198.023	     4.566	     1.817	     0.025
436		108	   203.794	     5.771	     1.853	     0.036
440		109	   209.056	     5.262	     1.883	     0.031
444		110	   211.805	     2.750	     1.891	     0.008
448		111	   240.589	    28.784	     2.129	     0.238
452		112	   220.819	   -19.770	     1.937	    -0.192
456		113	   224.581	     3.762	     1.953	     0.016
460		114	   227.740	     3.158	     1.963	     0.010
464		115	   231.183	     3.443	     1.976	     0.013
468		116	   264.882	    33.699	     2.245	     0.269
472		117	   239.513	   -25.368	     2.013	    -0.232
476		118	   241.786	     2.272	     2.015	     0.002
480		119	   247.144	     5.359	     2.043	     0.028
484		120	   251.201	     4.057	     2.059	     0.017
488		121	   278.203	    27.002	     2.262	     0.203
492		122	   259.402	   -18.801	     2.092	    -0.170
496		123	   264.178	     4.775	     2.113	     0.021
500		124	   267.939	     3.762	     2.127	     0.013
504		125	   271.497	     3.557	     2.138	     0.011
508		126	   300.891	    29.395	     2.351	     0.213
512		127	   281.788	   -19.104	     2.184	    -0.166
516		128	   284.388	     2.600	     2.188	     0.003
520		129	   289.923	     5.535	     2.213	     0.026
524		130	   293.571	     3.648	     2.224	     0.011
528		131	   297.237	     3.666	     2.235	     0.011
532		132	   303.284	     6.047	     2.263	     0.028
536		133	   306.950	     3.666	     2.274	     0.010
540		134	   312.451	     5.501	     2.297	     0.024
544		135	   319.837	     7.386	     2.335	     0.037
548		136	   322.196	     2.358	     2.335	     0.000
552		137	   326.054	     3.859	     2.346	     0.011
556		138	   330.274	     4.220	     2.359	     0.013
560		139	   360.497	    30.223	     2.557	     0.198
564		140	   342.817	   -17.680	     2.414	    -0.143
568		141	   346.734	     3.917	     2.425	     0.011
572		142	   350.985	     4.251	     2.437	     0.013
576		143	   359.384	     8.399	     2.479	     0.041
580		144	   361.212	     1.828	     2.474	    -0.004
584		145	   365.497	     4.286	     2.486	     0.012
588		146	   368.225	     2.728	     2.488	     0.002
592		147	   391.730	    23.505	     2.629	     0.141
596		148	   384.010	    -7.720	     2.560	    -0.069
600		149	   390.518	     6.508	     2.586	     0.026
604		150	   393.811	     3.293	     2.591	     0.005
608		151	   399.153	     5.342	     2.609	     0.018
612		152	   403.996	     4.843	     2.623	     0.015
616		153	   405.898	     1.901	     2.619	    -0.005
620		154	   429.537	    23.639	     2.753	     0.135
624		155	   416.828	   -12.708	     2.655	    -0.098
628		156	   422.490	     5.661	     2.674	     0.019
632		157	   472.702	    50.212	     2.973	     0.299
636		158	   466.722	    -5.980	     2.917	    -0.056
640		159	   436.882	   -29.840	     2.714	    -0.203
644		160	   488.675	    51.793	     3.017	     0.303
648		161	   448.399	   -40.276	     2.751	    -0.266
652		162	   453.266	     4.868	     2.764	     0.013
656		163	   460.250	     6.983	     2.789	     0.026
660		164	   465.454	     5.204	     2.804	     0.015
664		165	   471.840	     6.386	     2.825	     0.021
668		166	   476.994	     5.154	     2.839	     0.014
672		167	   481.592	     4.598	     2.850	     0.010
676		168	   488.882	     7.289	     2.876	     0.026
680		169	   501.495	    12.613	     2.933	     0.057
684		170	   500.073	    -1.422	     2.907	    -0.025
688		171	   511.222	    11.149	     2.955	     0.048
692		172	   539.582	    28.360	     3.101	     0.146
696		173	   517.536	   -22.046	     2.957	    -0.144
700		174	   522.089	     4.553	     2.966	     0.009
704		175	   531.159	     9.069	     3.001	     0.034
708		176	   536.250	     5.091	     3.013	     0.012
712		177	   542.462	     6.212	     3.031	     0.018
716		178	   548.370	     5.908	     3.046	     0.016
720		179	   553.430	     5.060	     3.058	     0.011
724		180	   557.218	     3.788	     3.062	     0.004
728		181	   567.618	    10.400	     3.102	     0.040
732		182	   573.376	     5.758	     3.116	     0.014
736		183	   579.012	     5.637	     3.130	     0.014
740		184	   585.586	     6.573	     3.148	     0.019
744		185	   596.811	    11.225	     3.192	     0.043
748		186	   597.084	     0.273	     3.176	    -0.016
752		187	   605.307	     8.223	     3.203	     0.027
756		188	   613.698	     8.391	     3.230	     0.027
760		189	   618.246	     4.548	     3.237	     0.007
764		190	   629.949	    11.703	     3.281	     0.044
768		191	   634.665	     4.717	     3.288	     0.007
772		192	   637.311	     2.646	     3.285	    -0.003
776		193	   643.045	     5.734	     3.298	     0.013
780		194	   654.031	    10.986	     3.337	     0.039
784		195	   660.076	     6.045	     3.351	     0.014
788		196	   664.799	     4.723	     3.358	     0.007
792		197	   674.744	     9.945	     3.391	     0.033
796		198	   675.801	     1.057	     3.379	    -0.012
800		199	   684.462	     8.661	     3.405	     0.026
804		200	   693.048	     8.587	     3.431	     0.026
808		201	   699.604	     6.555	     3.446	     0.015
812		202	   703.861	     4.258	     3.450	     0.004
816		203	   713.850	     9.989	     3.482	     0.032
820		204	   718.300	     4.450	     3.487	     0.005
824		205	   757.271	    38.971	     3.658	     0.171
828		206	   730.994	   -26.277	     3.514	    -0.144
832		207	   738.602	     7.608	     3.534	     0.020
836		208	   754.327	    15.725	     3.592	     0.058
840		209	   758.958	     4.631	     3.597	     0.005
844		210	   765.267	     6.309	     3.610	     0.013
848		211	   771.808	     6.541	     3.624	     0.014
852		212	   781.730	     9.923	     3.653	     0.029
856		213	   785.261	     3.530	     3.652	    -0.001
860		214	   792.281	     7.021	     3.668	     0.016
864		215	   804.394	    12.112	     3.707	     0.039
868		216	   811.739	     7.345	     3.724	     0.017
872		217	   813.420	     1.681	     3.714	    -0.009
876		218	   823.976	    10.556	     3.745	     0.031
880		219	   828.182	     4.206	     3.747	     0.002
884		220	   837.581	     9.400	     3.773	     0.025
888		221	   876.529	    38.948	     3.931	     0.158
892		222	   850.494	   -26.035	     3.797	    -0.134
896		223	   859.920	     9.426	     3.822	     0.025
900		224	   868.732	     8.812	     3.844	     0.022
904		225	   873.744	     5.012	     3.849	     0.005
908		226	   917.800	    44.056	     4.025	     0.176
912		227	   887.441	   -30.360	     3.875	    -0.150
916		228	   900.046	    12.606	     3.913	     0.038
920		229	   907.851	     7.805	     3.930	     0.017
924		230	   917.015	     9.164	     3.953	     0.023
928		231	   954.733	    37.718	     4.098	     0.145
932		232	   931.574	   -23.160	     3.981	    -0.116
936		233	   938.885	     7.312	     3.995	     0.014
940		234	   950.995	    12.109	     4.030	     0.034
944		235	   953.717	     2.722	     4.024	    -0.006
948		236	   996.817	    43.100	     4.188	     0.164
952		237	   964.062	   -32.754	     4.034	    -0.155
956		238	   982.641	    18.578	     4.094	     0.061
960		239	   987.284	     4.643	     4.097	     0.002
964		240	   997.530	    10.247	     4.122	     0.025
968		241	  1005.803	     8.273	     4.139	     0.017
972		242	  1010.674	     4.871	     4.142	     0.003
976		243	  1050.851	    40.176	     4.289	     0.147
980		244	  1033.639	   -17.212	     4.202	    -0.087
984		245	  1035.995	     2.357	     4.194	    -0.007
988		246	  1040.450	     4.455	     4.195	     0.001
992		247	  1056.766	    16.315	     4.244	     0.049
996		248	  1065.316	     8.551	     4.261	     0.017
1000		249	  1075.642	    10.326	     4.285	     0.024
1004		250	  1085.935	    10.293	     4.309	     0.024
