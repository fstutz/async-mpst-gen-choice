> promela file    : ../load_balancer/spin_code/load_balancer_109.pml
> date            : 27-Jan-2021 22:47:19
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../load_balancer/spin_code/load_balancer_109.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 2876 byte, depth reached 116, errors: 0
      441 states, stored
      109 states, matched
      550 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    1.221	equivalent memory usage for states (stored*(State-vector + overhead))
    1.779	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  539.620	total actual memory usage



pan: elapsed time 0.02 seconds
pan: rate     22050 states/second

real	0m0.282s
user	0m0.065s
sys	0m0.203s
