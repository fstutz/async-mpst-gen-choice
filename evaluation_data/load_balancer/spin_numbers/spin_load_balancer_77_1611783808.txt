> promela file    : ../load_balancer/spin_code/load_balancer_77.pml
> date            : 27-Jan-2021 22:43:30
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../load_balancer/spin_code/load_balancer_77.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 2044 byte, depth reached 84, errors: 0
      313 states, stored
       77 states, matched
      390 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.618	equivalent memory usage for states (stored*(State-vector + overhead))
    1.440	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  539.230	total actual memory usage



pan: elapsed time 0.01 seconds

real	0m0.278s
user	0m0.057s
sys	0m0.210s
