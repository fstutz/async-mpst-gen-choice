> promela file    : ../load_balancer/spin_code/load_balancer_76.pml
> date            : 27-Jan-2021 19:22:16
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../load_balancer/spin_code/load_balancer_76.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m100000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 2012 byte, depth reached 83, errors: 0
      309 states, stored
       76 states, matched
      385 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.601	equivalent memory usage for states (stored*(State-vector + overhead))
    1.440	actual memory usage for states
    4.000	memory used for hash table (-w19)
    5.341	memory used for DFS stack (-m100000)
   10.513	total actual memory usage



pan: elapsed time 0 seconds

real	0m0.020s
user	0m0.006s
sys	0m0.003s
