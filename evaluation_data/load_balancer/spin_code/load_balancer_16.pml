chan S_W6 = [1] of { int };
chan S_W13 = [1] of { int };
chan S_W1 = [1] of { int };
chan W7_C = [1] of { int };
chan S_W15 = [1] of { int };
chan W14_C = [1] of { int };
chan S_W11 = [1] of { int };
chan S_W4 = [1] of { int };
chan W16_C = [1] of { int };
chan C_S = [1] of { int };
chan W6_C = [1] of { int };
chan W13_C = [1] of { int };
chan S_W8 = [1] of { int };
chan W15_C = [1] of { int };
chan S_W16 = [1] of { int };
chan S_W3 = [1] of { int };
chan S_W10 = [1] of { int };
chan S_W2 = [1] of { int };
chan W4_C = [1] of { int };
chan S_W5 = [1] of { int };
chan W12_C = [1] of { int };
chan W3_C = [1] of { int };
chan S_W7 = [1] of { int };
chan W1_C = [1] of { int };
chan W2_C = [1] of { int };
chan W10_C = [1] of { int };
chan W8_C = [1] of { int };
chan S_W12 = [1] of { int };
chan W11_C = [1] of { int };
chan W5_C = [1] of { int };
chan W9_C = [1] of { int };
chan S_W9 = [1] of { int };
chan S_W14 = [1] of { int };

init
{
	run C();
	run S();
	run W1();
	run W10();
	run W11();
	run W12();
	run W13();
	run W14();
	run W15();
	run W16();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_S!1 -> goto state3
	fi
state3:
	if
	:: W6_C?2 -> goto state4
	:: W15_C?2 -> goto state4
	:: W12_C?2 -> goto state4
	:: W11_C?2 -> goto state4
	:: W2_C?2 -> goto state4
	:: W9_C?2 -> goto state4
	:: W13_C?2 -> goto state4
	:: W4_C?2 -> goto state4
	:: W10_C?2 -> goto state4
	:: W5_C?2 -> goto state4
	:: W8_C?2 -> goto state4
	:: W14_C?2 -> goto state4
	:: W7_C?2 -> goto state4
	:: W1_C?2 -> goto state4
	:: W3_C?2 -> goto state4
	:: W16_C?2 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype S()
{
state1:
	goto state2
state2:
	if
	:: C_S?1 -> goto state3
	fi
state3:
	if
	:: S_W1!1 -> goto state4
	:: S_W2!1 -> goto state4
	:: S_W3!1 -> goto state4
	:: S_W4!1 -> goto state4
	:: S_W5!1 -> goto state4
	:: S_W6!1 -> goto state4
	:: S_W7!1 -> goto state4
	:: S_W8!1 -> goto state4
	:: S_W9!1 -> goto state4
	:: S_W10!1 -> goto state4
	:: S_W11!1 -> goto state4
	:: S_W12!1 -> goto state4
	:: S_W13!1 -> goto state4
	:: S_W14!1 -> goto state4
	:: S_W15!1 -> goto state4
	:: S_W16!1 -> goto state4
	fi
state4:
	goto state2
} /* S */

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: S_W1?1 -> goto state3
	fi
state3:
	if
	:: W1_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W1 */

proctype W10()
{
state1:
	goto state2
state2:
	if
	:: S_W10?1 -> goto state3
	fi
state3:
	if
	:: W10_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W10 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: S_W11?1 -> goto state3
	fi
state3:
	if
	:: W11_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W11 */

proctype W12()
{
state1:
	goto state2
state2:
	if
	:: S_W12?1 -> goto state3
	fi
state3:
	if
	:: W12_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W12 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: S_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W13 */

proctype W14()
{
state1:
	goto state2
state2:
	if
	:: S_W14?1 -> goto state3
	fi
state3:
	if
	:: W14_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W14 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: S_W15?1 -> goto state3
	fi
state3:
	if
	:: W15_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W15 */

proctype W16()
{
state1:
	goto state2
state2:
	if
	:: S_W16?1 -> goto state3
	fi
state3:
	if
	:: W16_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W16 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: S_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: S_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: S_W4?1 -> goto state3
	fi
state3:
	if
	:: W4_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W4 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: S_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: S_W6?1 -> goto state3
	fi
state3:
	if
	:: W6_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: S_W7?1 -> goto state3
	fi
state3:
	if
	:: W7_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W7 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: S_W8?1 -> goto state3
	fi
state3:
	if
	:: W8_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W8 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: S_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W9 */
