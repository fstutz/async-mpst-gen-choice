chan S_W5 = [1] of { int };
chan S_W6 = [1] of { int };
chan S_W1 = [1] of { int };
chan W7_C = [1] of { int };
chan W3_C = [1] of { int };
chan S_W7 = [1] of { int };
chan W1_C = [1] of { int };
chan W2_C = [1] of { int };
chan W8_C = [1] of { int };
chan W5_C = [1] of { int };
chan S_W4 = [1] of { int };
chan C_S = [1] of { int };
chan W6_C = [1] of { int };
chan W9_C = [1] of { int };
chan S_W9 = [1] of { int };
chan S_W8 = [1] of { int };
chan S_W3 = [1] of { int };
chan S_W2 = [1] of { int };
chan W4_C = [1] of { int };

init
{
	run C();
	run S();
	run W1();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_S!1 -> goto state3
	fi
state3:
	if
	:: W6_C?2 -> goto state4
	:: W2_C?2 -> goto state4
	:: W4_C?2 -> goto state4
	:: W5_C?2 -> goto state4
	:: W8_C?2 -> goto state4
	:: W7_C?2 -> goto state4
	:: W1_C?2 -> goto state4
	:: W3_C?2 -> goto state4
	:: W9_C?2 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype S()
{
state1:
	goto state2
state2:
	if
	:: C_S?1 -> goto state3
	fi
state3:
	if
	:: S_W1!1 -> goto state4
	:: S_W2!1 -> goto state4
	:: S_W3!1 -> goto state4
	:: S_W4!1 -> goto state4
	:: S_W5!1 -> goto state4
	:: S_W6!1 -> goto state4
	:: S_W7!1 -> goto state4
	:: S_W8!1 -> goto state4
	:: S_W9!1 -> goto state4
	fi
state4:
	goto state2
} /* S */

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: S_W1?1 -> goto state3
	fi
state3:
	if
	:: W1_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W1 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: S_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: S_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: S_W4?1 -> goto state3
	fi
state3:
	if
	:: W4_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W4 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: S_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: S_W6?1 -> goto state3
	fi
state3:
	if
	:: W6_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: S_W7?1 -> goto state3
	fi
state3:
	if
	:: W7_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W7 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: S_W8?1 -> goto state3
	fi
state3:
	if
	:: W8_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W8 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: S_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W9 */
