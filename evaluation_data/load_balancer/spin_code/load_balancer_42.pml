chan S_W13 = [1] of { int };
chan W41_C = [1] of { int };
chan W7_C = [1] of { int };
chan S_W34 = [1] of { int };
chan W27_C = [1] of { int };
chan S_W15 = [1] of { int };
chan S_W35 = [1] of { int };
chan S_W25 = [1] of { int };
chan S_W11 = [1] of { int };
chan W36_C = [1] of { int };
chan W26_C = [1] of { int };
chan S_W26 = [1] of { int };
chan S_W19 = [1] of { int };
chan W16_C = [1] of { int };
chan S_W28 = [1] of { int };
chan W6_C = [1] of { int };
chan S_W3 = [1] of { int };
chan W40_C = [1] of { int };
chan W4_C = [1] of { int };
chan S_W39 = [1] of { int };
chan W1_C = [1] of { int };
chan W35_C = [1] of { int };
chan W2_C = [1] of { int };
chan W10_C = [1] of { int };
chan S_W32 = [1] of { int };
chan W39_C = [1] of { int };
chan W34_C = [1] of { int };
chan W11_C = [1] of { int };
chan S_W18 = [1] of { int };
chan S_W42 = [1] of { int };
chan S_W30 = [1] of { int };
chan S_W33 = [1] of { int };
chan W29_C = [1] of { int };
chan S_W6 = [1] of { int };
chan S_W1 = [1] of { int };
chan S_W23 = [1] of { int };
chan W38_C = [1] of { int };
chan S_W40 = [1] of { int };
chan W32_C = [1] of { int };
chan W14_C = [1] of { int };
chan S_W31 = [1] of { int };
chan W17_C = [1] of { int };
chan W30_C = [1] of { int };
chan S_W27 = [1] of { int };
chan W20_C = [1] of { int };
chan S_W21 = [1] of { int };
chan S_W41 = [1] of { int };
chan S_W29 = [1] of { int };
chan S_W20 = [1] of { int };
chan W24_C = [1] of { int };
chan W33_C = [1] of { int };
chan S_W4 = [1] of { int };
chan C_S = [1] of { int };
chan W13_C = [1] of { int };
chan S_W37 = [1] of { int };
chan S_W8 = [1] of { int };
chan W15_C = [1] of { int };
chan S_W16 = [1] of { int };
chan W23_C = [1] of { int };
chan W37_C = [1] of { int };
chan S_W2 = [1] of { int };
chan S_W10 = [1] of { int };
chan W28_C = [1] of { int };
chan S_W5 = [1] of { int };
chan W19_C = [1] of { int };
chan W12_C = [1] of { int };
chan W22_C = [1] of { int };
chan S_W38 = [1] of { int };
chan W3_C = [1] of { int };
chan S_W7 = [1] of { int };
chan W31_C = [1] of { int };
chan W18_C = [1] of { int };
chan W42_C = [1] of { int };
chan W8_C = [1] of { int };
chan S_W24 = [1] of { int };
chan S_W36 = [1] of { int };
chan S_W12 = [1] of { int };
chan W21_C = [1] of { int };
chan S_W22 = [1] of { int };
chan W5_C = [1] of { int };
chan W25_C = [1] of { int };
chan W9_C = [1] of { int };
chan S_W9 = [1] of { int };
chan S_W17 = [1] of { int };
chan S_W14 = [1] of { int };

init
{
	run C();
	run S();
	run W1();
	run W10();
	run W11();
	run W12();
	run W13();
	run W14();
	run W15();
	run W16();
	run W17();
	run W18();
	run W19();
	run W2();
	run W20();
	run W21();
	run W22();
	run W23();
	run W24();
	run W25();
	run W26();
	run W27();
	run W28();
	run W29();
	run W3();
	run W30();
	run W31();
	run W32();
	run W33();
	run W34();
	run W35();
	run W36();
	run W37();
	run W38();
	run W39();
	run W4();
	run W40();
	run W41();
	run W42();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_S!1 -> goto state3
	fi
state3:
	if
	:: W18_C?2 -> goto state4
	:: W11_C?2 -> goto state4
	:: W21_C?2 -> goto state4
	:: W2_C?2 -> goto state4
	:: W9_C?2 -> goto state4
	:: W25_C?2 -> goto state4
	:: W31_C?2 -> goto state4
	:: W36_C?2 -> goto state4
	:: W32_C?2 -> goto state4
	:: W8_C?2 -> goto state4
	:: W39_C?2 -> goto state4
	:: W29_C?2 -> goto state4
	:: W30_C?2 -> goto state4
	:: W6_C?2 -> goto state4
	:: W12_C?2 -> goto state4
	:: W23_C?2 -> goto state4
	:: W37_C?2 -> goto state4
	:: W19_C?2 -> goto state4
	:: W4_C?2 -> goto state4
	:: W27_C?2 -> goto state4
	:: W10_C?2 -> goto state4
	:: W5_C?2 -> goto state4
	:: W34_C?2 -> goto state4
	:: W16_C?2 -> goto state4
	:: W15_C?2 -> goto state4
	:: W1_C?2 -> goto state4
	:: W22_C?2 -> goto state4
	:: W13_C?2 -> goto state4
	:: W38_C?2 -> goto state4
	:: W17_C?2 -> goto state4
	:: W28_C?2 -> goto state4
	:: W20_C?2 -> goto state4
	:: W33_C?2 -> goto state4
	:: W26_C?2 -> goto state4
	:: W24_C?2 -> goto state4
	:: W35_C?2 -> goto state4
	:: W41_C?2 -> goto state4
	:: W14_C?2 -> goto state4
	:: W7_C?2 -> goto state4
	:: W40_C?2 -> goto state4
	:: W3_C?2 -> goto state4
	:: W42_C?2 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype S()
{
state1:
	goto state2
state2:
	if
	:: C_S?1 -> goto state3
	fi
state3:
	if
	:: S_W1!1 -> goto state4
	:: S_W2!1 -> goto state4
	:: S_W3!1 -> goto state4
	:: S_W4!1 -> goto state4
	:: S_W5!1 -> goto state4
	:: S_W6!1 -> goto state4
	:: S_W7!1 -> goto state4
	:: S_W8!1 -> goto state4
	:: S_W9!1 -> goto state4
	:: S_W10!1 -> goto state4
	:: S_W11!1 -> goto state4
	:: S_W12!1 -> goto state4
	:: S_W13!1 -> goto state4
	:: S_W14!1 -> goto state4
	:: S_W15!1 -> goto state4
	:: S_W16!1 -> goto state4
	:: S_W17!1 -> goto state4
	:: S_W18!1 -> goto state4
	:: S_W19!1 -> goto state4
	:: S_W20!1 -> goto state4
	:: S_W21!1 -> goto state4
	:: S_W22!1 -> goto state4
	:: S_W23!1 -> goto state4
	:: S_W24!1 -> goto state4
	:: S_W25!1 -> goto state4
	:: S_W26!1 -> goto state4
	:: S_W27!1 -> goto state4
	:: S_W28!1 -> goto state4
	:: S_W29!1 -> goto state4
	:: S_W30!1 -> goto state4
	:: S_W31!1 -> goto state4
	:: S_W32!1 -> goto state4
	:: S_W33!1 -> goto state4
	:: S_W34!1 -> goto state4
	:: S_W35!1 -> goto state4
	:: S_W36!1 -> goto state4
	:: S_W37!1 -> goto state4
	:: S_W38!1 -> goto state4
	:: S_W39!1 -> goto state4
	:: S_W40!1 -> goto state4
	:: S_W41!1 -> goto state4
	:: S_W42!1 -> goto state4
	fi
state4:
	goto state2
} /* S */

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: S_W1?1 -> goto state3
	fi
state3:
	if
	:: W1_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W1 */

proctype W10()
{
state1:
	goto state2
state2:
	if
	:: S_W10?1 -> goto state3
	fi
state3:
	if
	:: W10_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W10 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: S_W11?1 -> goto state3
	fi
state3:
	if
	:: W11_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W11 */

proctype W12()
{
state1:
	goto state2
state2:
	if
	:: S_W12?1 -> goto state3
	fi
state3:
	if
	:: W12_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W12 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: S_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W13 */

proctype W14()
{
state1:
	goto state2
state2:
	if
	:: S_W14?1 -> goto state3
	fi
state3:
	if
	:: W14_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W14 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: S_W15?1 -> goto state3
	fi
state3:
	if
	:: W15_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W15 */

proctype W16()
{
state1:
	goto state2
state2:
	if
	:: S_W16?1 -> goto state3
	fi
state3:
	if
	:: W16_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W16 */

proctype W17()
{
state1:
	goto state2
state2:
	if
	:: S_W17?1 -> goto state3
	fi
state3:
	if
	:: W17_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W17 */

proctype W18()
{
state1:
	goto state2
state2:
	if
	:: S_W18?1 -> goto state3
	fi
state3:
	if
	:: W18_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W18 */

proctype W19()
{
state1:
	goto state2
state2:
	if
	:: S_W19?1 -> goto state3
	fi
state3:
	if
	:: W19_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W19 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: S_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W2 */

proctype W20()
{
state1:
	goto state2
state2:
	if
	:: S_W20?1 -> goto state3
	fi
state3:
	if
	:: W20_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W20 */

proctype W21()
{
state1:
	goto state2
state2:
	if
	:: S_W21?1 -> goto state3
	fi
state3:
	if
	:: W21_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W21 */

proctype W22()
{
state1:
	goto state2
state2:
	if
	:: S_W22?1 -> goto state3
	fi
state3:
	if
	:: W22_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W22 */

proctype W23()
{
state1:
	goto state2
state2:
	if
	:: S_W23?1 -> goto state3
	fi
state3:
	if
	:: W23_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W23 */

proctype W24()
{
state1:
	goto state2
state2:
	if
	:: S_W24?1 -> goto state3
	fi
state3:
	if
	:: W24_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W24 */

proctype W25()
{
state1:
	goto state2
state2:
	if
	:: S_W25?1 -> goto state3
	fi
state3:
	if
	:: W25_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W25 */

proctype W26()
{
state1:
	goto state2
state2:
	if
	:: S_W26?1 -> goto state3
	fi
state3:
	if
	:: W26_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W26 */

proctype W27()
{
state1:
	goto state2
state2:
	if
	:: S_W27?1 -> goto state3
	fi
state3:
	if
	:: W27_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W27 */

proctype W28()
{
state1:
	goto state2
state2:
	if
	:: S_W28?1 -> goto state3
	fi
state3:
	if
	:: W28_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W28 */

proctype W29()
{
state1:
	goto state2
state2:
	if
	:: S_W29?1 -> goto state3
	fi
state3:
	if
	:: W29_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W29 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: S_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W30()
{
state1:
	goto state2
state2:
	if
	:: S_W30?1 -> goto state3
	fi
state3:
	if
	:: W30_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W30 */

proctype W31()
{
state1:
	goto state2
state2:
	if
	:: S_W31?1 -> goto state3
	fi
state3:
	if
	:: W31_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W31 */

proctype W32()
{
state1:
	goto state2
state2:
	if
	:: S_W32?1 -> goto state3
	fi
state3:
	if
	:: W32_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W32 */

proctype W33()
{
state1:
	goto state2
state2:
	if
	:: S_W33?1 -> goto state3
	fi
state3:
	if
	:: W33_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W33 */

proctype W34()
{
state1:
	goto state2
state2:
	if
	:: S_W34?1 -> goto state3
	fi
state3:
	if
	:: W34_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W34 */

proctype W35()
{
state1:
	goto state2
state2:
	if
	:: S_W35?1 -> goto state3
	fi
state3:
	if
	:: W35_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W35 */

proctype W36()
{
state1:
	goto state2
state2:
	if
	:: S_W36?1 -> goto state3
	fi
state3:
	if
	:: W36_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W36 */

proctype W37()
{
state1:
	goto state2
state2:
	if
	:: S_W37?1 -> goto state3
	fi
state3:
	if
	:: W37_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W37 */

proctype W38()
{
state1:
	goto state2
state2:
	if
	:: S_W38?1 -> goto state3
	fi
state3:
	if
	:: W38_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W38 */

proctype W39()
{
state1:
	goto state2
state2:
	if
	:: S_W39?1 -> goto state3
	fi
state3:
	if
	:: W39_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W39 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: S_W4?1 -> goto state3
	fi
state3:
	if
	:: W4_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W4 */

proctype W40()
{
state1:
	goto state2
state2:
	if
	:: S_W40?1 -> goto state3
	fi
state3:
	if
	:: W40_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W40 */

proctype W41()
{
state1:
	goto state2
state2:
	if
	:: S_W41?1 -> goto state3
	fi
state3:
	if
	:: W41_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W41 */

proctype W42()
{
state1:
	goto state2
state2:
	if
	:: S_W42?1 -> goto state3
	fi
state3:
	if
	:: W42_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W42 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: S_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: S_W6?1 -> goto state3
	fi
state3:
	if
	:: W6_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: S_W7?1 -> goto state3
	fi
state3:
	if
	:: W7_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W7 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: S_W8?1 -> goto state3
	fi
state3:
	if
	:: W8_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W8 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: S_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W9 */
