chan S_W1 = [1] of { int };
chan W3_C = [1] of { int };
chan W1_C = [1] of { int };
chan S_W4 = [1] of { int };
chan C_S = [1] of { int };
chan W2_C = [1] of { int };
chan S_W3 = [1] of { int };
chan S_W2 = [1] of { int };
chan W4_C = [1] of { int };

init
{
	run C();
	run S();
	run W1();
	run W2();
	run W3();
	run W4()
}

proctype C()
{
state1:
	goto state2
state2:
	if
	:: C_S!1 -> goto state3
	fi
state3:
	if
	:: W1_C?2 -> goto state4
	:: W2_C?2 -> goto state4
	:: W3_C?2 -> goto state4
	:: W4_C?2 -> goto state4
	fi
state4:
	goto state2
} /* C */

proctype S()
{
state1:
	goto state2
state2:
	if
	:: C_S?1 -> goto state3
	fi
state3:
	if
	:: S_W1!1 -> goto state4
	:: S_W2!1 -> goto state4
	:: S_W3!1 -> goto state4
	:: S_W4!1 -> goto state4
	fi
state4:
	goto state2
} /* S */

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: S_W1?1 -> goto state3
	fi
state3:
	if
	:: W1_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W1 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: S_W2?1 -> goto state3
	fi
state3:
	if
	:: W2_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: S_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: S_W4?1 -> goto state3
	fi
state3:
	if
	:: W4_C!2 -> goto state4
	fi
state4:
	goto state2
} /* W4 */
