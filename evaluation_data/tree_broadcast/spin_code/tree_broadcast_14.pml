chan W1_W5 = [1] of { int };
chan W13_W15 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W5_W6 = [1] of { int };
chan W7_W8 = [1] of { int };
chan W9_W11 = [1] of { int };
chan W9_W10 = [1] of { int };
chan W9_W13 = [1] of { int };
chan W1_W9 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W11_W12 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W1_W3 = [1] of { int };

init
{
	run W1();
	run W10();
	run W11();
	run W12();
	run W13();
	run W15();
	run W2();
	run W3();
	run W4();
	run W5();
	run W6();
	run W7();
	run W8();
	run W9()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W9!1 -> goto state3
	fi
state3:
	if
	:: W1_W5!1 -> goto state4
	fi
state4:
	if
	:: W1_W3!1 -> goto state5
	fi
state5:
	if
	:: W1_W2!1 -> goto state6
	fi
state6:
	goto state2
} /* W1 */

proctype W10()
{
state1:
	goto state2
state2:
	if
	:: W9_W10?1 -> goto state3
	fi
state3:
	goto state2
} /* W10 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: W9_W11?1 -> goto state3
	fi
state3:
	if
	:: W11_W12!1 -> goto state4
	fi
state4:
	goto state2
} /* W11 */

proctype W12()
{
state1:
	goto state2
state2:
	if
	:: W11_W12?1 -> goto state3
	fi
state3:
	goto state2
} /* W12 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: W9_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_W15!1 -> goto state4
	fi
state4:
	goto state2
} /* W13 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: W13_W15?1 -> goto state3
	fi
state3:
	goto state2
} /* W15 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	goto state2
} /* W2 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_W4!1 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W3_W4?1 -> goto state3
	fi
state3:
	goto state2
} /* W4 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_W7!1 -> goto state4
	fi
state4:
	if
	:: W5_W6!1 -> goto state5
	fi
state5:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: W5_W6?1 -> goto state3
	fi
state3:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W5_W7?1 -> goto state3
	fi
state3:
	if
	:: W7_W8!1 -> goto state4
	fi
state4:
	goto state2
} /* W7 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: W7_W8?1 -> goto state3
	fi
state3:
	goto state2
} /* W8 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: W1_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_W13!1 -> goto state4
	fi
state4:
	if
	:: W9_W11!1 -> goto state5
	fi
state5:
	if
	:: W9_W10!1 -> goto state6
	fi
state6:
	goto state2
} /* W9 */
