chan W27_W28 = [1] of { int };
chan W73_W75 = [1] of { int };
chan W61_W63 = [1] of { int };
chan W33_W34 = [1] of { int };
chan W35_W36 = [1] of { int };
chan W51_W52 = [1] of { int };
chan W93_W95 = [1] of { int };
chan W65_W66 = [1] of { int };
chan W113_W121 = [1] of { int };
chan W97_W101 = [1] of { int };
chan W39_W40 = [1] of { int };
chan W17_W25 = [1] of { int };
chan W91_W92 = [1] of { int };
chan W61_W62 = [1] of { int };
chan W49_W57 = [1] of { int };
chan W7_W8 = [1] of { int };
chan W41_W43 = [1] of { int };
chan W21_W22 = [1] of { int };
chan W11_W12 = [1] of { int };
chan W113_W117 = [1] of { int };
chan W75_W76 = [1] of { int };
chan W55_W56 = [1] of { int };
chan W25_W27 = [1] of { int };
chan W1_W65 = [1] of { int };
chan W63_W64 = [1] of { int };
chan W77_W79 = [1] of { int };
chan W17_W21 = [1] of { int };
chan W37_W39 = [1] of { int };
chan W65_W67 = [1] of { int };
chan W41_W42 = [1] of { int };
chan W97_W99 = [1] of { int };
chan W67_W68 = [1] of { int };
chan W89_W91 = [1] of { int };
chan W57_W61 = [1] of { int };
chan W57_W58 = [1] of { int };
chan W13_W14 = [1] of { int };
chan W1_W17 = [1] of { int };
chan W101_W103 = [1] of { int };
chan W17_W18 = [1] of { int };
chan W53_W55 = [1] of { int };
chan W81_W82 = [1] of { int };
chan W81_W85 = [1] of { int };
chan W33_W41 = [1] of { int };
chan W89_W93 = [1] of { int };
chan W1_W9 = [1] of { int };
chan W43_W44 = [1] of { int };
chan W77_W78 = [1] of { int };
chan W1_W33 = [1] of { int };
chan W125_W127 = [1] of { int };
chan W33_W37 = [1] of { int };
chan W31_W32 = [1] of { int };
chan W45_W46 = [1] of { int };
chan W15_W16 = [1] of { int };
chan W121_W123 = [1] of { int };
chan W13_W15 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W69_W71 = [1] of { int };
chan W113_W115 = [1] of { int };
chan W9_W11 = [1] of { int };
chan W73_W74 = [1] of { int };
chan W65_W81 = [1] of { int };
chan W89_W90 = [1] of { int };
chan W105_W107 = [1] of { int };
chan W49_W51 = [1] of { int };
chan W81_W89 = [1] of { int };
chan W81_W83 = [1] of { int };
chan W49_W50 = [1] of { int };
chan W49_W53 = [1] of { int };
chan W17_W19 = [1] of { int };
chan W21_W23 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W29_W31 = [1] of { int };
chan W1_W5 = [1] of { int };
chan W65_W69 = [1] of { int };
chan W83_W84 = [1] of { int };
chan W47_W48 = [1] of { int };
chan W109_W111 = [1] of { int };
chan W53_W54 = [1] of { int };
chan W59_W60 = [1] of { int };
chan W69_W70 = [1] of { int };
chan W9_W10 = [1] of { int };
chan W33_W49 = [1] of { int };
chan W65_W73 = [1] of { int };
chan W57_W59 = [1] of { int };
chan W97_W113 = [1] of { int };
chan W117_W119 = [1] of { int };
chan W85_W87 = [1] of { int };
chan W25_W26 = [1] of { int };
chan W5_W6 = [1] of { int };
chan W23_W24 = [1] of { int };
chan W73_W77 = [1] of { int };
chan W9_W13 = [1] of { int };
chan W97_W105 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W45_W47 = [1] of { int };
chan W93_W94 = [1] of { int };
chan W37_W38 = [1] of { int };
chan W29_W30 = [1] of { int };
chan W105_W109 = [1] of { int };
chan W121_W125 = [1] of { int };
chan W87_W88 = [1] of { int };
chan W25_W29 = [1] of { int };
chan W41_W45 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W33_W35 = [1] of { int };
chan W19_W20 = [1] of { int };
chan W65_W97 = [1] of { int };
chan W79_W80 = [1] of { int };
chan W85_W86 = [1] of { int };
chan W71_W72 = [1] of { int };

init
{
	run W1();
	run W10();
	run W101();
	run W103();
	run W105();
	run W107();
	run W109();
	run W11();
	run W111();
	run W113();
	run W115();
	run W117();
	run W119();
	run W12();
	run W121();
	run W123();
	run W125();
	run W127();
	run W13();
	run W14();
	run W15();
	run W16();
	run W17();
	run W18();
	run W19();
	run W2();
	run W20();
	run W21();
	run W22();
	run W23();
	run W24();
	run W25();
	run W26();
	run W27();
	run W28();
	run W29();
	run W3();
	run W30();
	run W31();
	run W32();
	run W33();
	run W34();
	run W35();
	run W36();
	run W37();
	run W38();
	run W39();
	run W4();
	run W40();
	run W41();
	run W42();
	run W43();
	run W44();
	run W45();
	run W46();
	run W47();
	run W48();
	run W49();
	run W5();
	run W50();
	run W51();
	run W52();
	run W53();
	run W54();
	run W55();
	run W56();
	run W57();
	run W58();
	run W59();
	run W6();
	run W60();
	run W61();
	run W62();
	run W63();
	run W64();
	run W65();
	run W66();
	run W67();
	run W68();
	run W69();
	run W7();
	run W70();
	run W71();
	run W72();
	run W73();
	run W74();
	run W75();
	run W76();
	run W77();
	run W78();
	run W79();
	run W8();
	run W80();
	run W81();
	run W82();
	run W83();
	run W84();
	run W85();
	run W86();
	run W87();
	run W88();
	run W89();
	run W9();
	run W90();
	run W91();
	run W92();
	run W93();
	run W94();
	run W95();
	run W97();
	run W99()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W65!1 -> goto state3
	fi
state3:
	if
	:: W1_W33!1 -> goto state4
	fi
state4:
	if
	:: W1_W17!1 -> goto state5
	fi
state5:
	if
	:: W1_W9!1 -> goto state6
	fi
state6:
	if
	:: W1_W5!1 -> goto state7
	fi
state7:
	if
	:: W1_W3!1 -> goto state8
	fi
state8:
	if
	:: W1_W2!1 -> goto state9
	fi
state9:
	goto state2
} /* W1 */

proctype W10()
{
state1:
	goto state2
state2:
	if
	:: W9_W10?1 -> goto state3
	fi
state3:
	goto state2
} /* W10 */

proctype W101()
{
state1:
	goto state2
state2:
	if
	:: W97_W101?1 -> goto state3
	fi
state3:
	if
	:: W101_W103!1 -> goto state4
	fi
state4:
	goto state2
} /* W101 */

proctype W103()
{
state1:
	goto state2
state2:
	if
	:: W101_W103?1 -> goto state3
	fi
state3:
	goto state2
} /* W103 */

proctype W105()
{
state1:
	goto state2
state2:
	if
	:: W97_W105?1 -> goto state3
	fi
state3:
	if
	:: W105_W109!1 -> goto state4
	fi
state4:
	if
	:: W105_W107!1 -> goto state5
	fi
state5:
	goto state2
} /* W105 */

proctype W107()
{
state1:
	goto state2
state2:
	if
	:: W105_W107?1 -> goto state3
	fi
state3:
	goto state2
} /* W107 */

proctype W109()
{
state1:
	goto state2
state2:
	if
	:: W105_W109?1 -> goto state3
	fi
state3:
	if
	:: W109_W111!1 -> goto state4
	fi
state4:
	goto state2
} /* W109 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: W9_W11?1 -> goto state3
	fi
state3:
	if
	:: W11_W12!1 -> goto state4
	fi
state4:
	goto state2
} /* W11 */

proctype W111()
{
state1:
	goto state2
state2:
	if
	:: W109_W111?1 -> goto state3
	fi
state3:
	goto state2
} /* W111 */

proctype W113()
{
state1:
	goto state2
state2:
	if
	:: W97_W113?1 -> goto state3
	fi
state3:
	if
	:: W113_W121!1 -> goto state4
	fi
state4:
	if
	:: W113_W117!1 -> goto state5
	fi
state5:
	if
	:: W113_W115!1 -> goto state6
	fi
state6:
	goto state2
} /* W113 */

proctype W115()
{
state1:
	goto state2
state2:
	if
	:: W113_W115?1 -> goto state3
	fi
state3:
	goto state2
} /* W115 */

proctype W117()
{
state1:
	goto state2
state2:
	if
	:: W113_W117?1 -> goto state3
	fi
state3:
	if
	:: W117_W119!1 -> goto state4
	fi
state4:
	goto state2
} /* W117 */

proctype W119()
{
state1:
	goto state2
state2:
	if
	:: W117_W119?1 -> goto state3
	fi
state3:
	goto state2
} /* W119 */

proctype W12()
{
state1:
	goto state2
state2:
	if
	:: W11_W12?1 -> goto state3
	fi
state3:
	goto state2
} /* W12 */

proctype W121()
{
state1:
	goto state2
state2:
	if
	:: W113_W121?1 -> goto state3
	fi
state3:
	if
	:: W121_W125!1 -> goto state4
	fi
state4:
	if
	:: W121_W123!1 -> goto state5
	fi
state5:
	goto state2
} /* W121 */

proctype W123()
{
state1:
	goto state2
state2:
	if
	:: W121_W123?1 -> goto state3
	fi
state3:
	goto state2
} /* W123 */

proctype W125()
{
state1:
	goto state2
state2:
	if
	:: W121_W125?1 -> goto state3
	fi
state3:
	if
	:: W125_W127!1 -> goto state4
	fi
state4:
	goto state2
} /* W125 */

proctype W127()
{
state1:
	goto state2
state2:
	if
	:: W125_W127?1 -> goto state3
	fi
state3:
	goto state2
} /* W127 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: W9_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_W15!1 -> goto state4
	fi
state4:
	if
	:: W13_W14!1 -> goto state5
	fi
state5:
	goto state2
} /* W13 */

proctype W14()
{
state1:
	goto state2
state2:
	if
	:: W13_W14?1 -> goto state3
	fi
state3:
	goto state2
} /* W14 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: W13_W15?1 -> goto state3
	fi
state3:
	if
	:: W15_W16!1 -> goto state4
	fi
state4:
	goto state2
} /* W15 */

proctype W16()
{
state1:
	goto state2
state2:
	if
	:: W15_W16?1 -> goto state3
	fi
state3:
	goto state2
} /* W16 */

proctype W17()
{
state1:
	goto state2
state2:
	if
	:: W1_W17?1 -> goto state3
	fi
state3:
	if
	:: W17_W25!1 -> goto state4
	fi
state4:
	if
	:: W17_W21!1 -> goto state5
	fi
state5:
	if
	:: W17_W19!1 -> goto state6
	fi
state6:
	if
	:: W17_W18!1 -> goto state7
	fi
state7:
	goto state2
} /* W17 */

proctype W18()
{
state1:
	goto state2
state2:
	if
	:: W17_W18?1 -> goto state3
	fi
state3:
	goto state2
} /* W18 */

proctype W19()
{
state1:
	goto state2
state2:
	if
	:: W17_W19?1 -> goto state3
	fi
state3:
	if
	:: W19_W20!1 -> goto state4
	fi
state4:
	goto state2
} /* W19 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	goto state2
} /* W2 */

proctype W20()
{
state1:
	goto state2
state2:
	if
	:: W19_W20?1 -> goto state3
	fi
state3:
	goto state2
} /* W20 */

proctype W21()
{
state1:
	goto state2
state2:
	if
	:: W17_W21?1 -> goto state3
	fi
state3:
	if
	:: W21_W23!1 -> goto state4
	fi
state4:
	if
	:: W21_W22!1 -> goto state5
	fi
state5:
	goto state2
} /* W21 */

proctype W22()
{
state1:
	goto state2
state2:
	if
	:: W21_W22?1 -> goto state3
	fi
state3:
	goto state2
} /* W22 */

proctype W23()
{
state1:
	goto state2
state2:
	if
	:: W21_W23?1 -> goto state3
	fi
state3:
	if
	:: W23_W24!1 -> goto state4
	fi
state4:
	goto state2
} /* W23 */

proctype W24()
{
state1:
	goto state2
state2:
	if
	:: W23_W24?1 -> goto state3
	fi
state3:
	goto state2
} /* W24 */

proctype W25()
{
state1:
	goto state2
state2:
	if
	:: W17_W25?1 -> goto state3
	fi
state3:
	if
	:: W25_W29!1 -> goto state4
	fi
state4:
	if
	:: W25_W27!1 -> goto state5
	fi
state5:
	if
	:: W25_W26!1 -> goto state6
	fi
state6:
	goto state2
} /* W25 */

proctype W26()
{
state1:
	goto state2
state2:
	if
	:: W25_W26?1 -> goto state3
	fi
state3:
	goto state2
} /* W26 */

proctype W27()
{
state1:
	goto state2
state2:
	if
	:: W25_W27?1 -> goto state3
	fi
state3:
	if
	:: W27_W28!1 -> goto state4
	fi
state4:
	goto state2
} /* W27 */

proctype W28()
{
state1:
	goto state2
state2:
	if
	:: W27_W28?1 -> goto state3
	fi
state3:
	goto state2
} /* W28 */

proctype W29()
{
state1:
	goto state2
state2:
	if
	:: W25_W29?1 -> goto state3
	fi
state3:
	if
	:: W29_W31!1 -> goto state4
	fi
state4:
	if
	:: W29_W30!1 -> goto state5
	fi
state5:
	goto state2
} /* W29 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_W4!1 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W30()
{
state1:
	goto state2
state2:
	if
	:: W29_W30?1 -> goto state3
	fi
state3:
	goto state2
} /* W30 */

proctype W31()
{
state1:
	goto state2
state2:
	if
	:: W29_W31?1 -> goto state3
	fi
state3:
	if
	:: W31_W32!1 -> goto state4
	fi
state4:
	goto state2
} /* W31 */

proctype W32()
{
state1:
	goto state2
state2:
	if
	:: W31_W32?1 -> goto state3
	fi
state3:
	goto state2
} /* W32 */

proctype W33()
{
state1:
	goto state2
state2:
	if
	:: W1_W33?1 -> goto state3
	fi
state3:
	if
	:: W33_W49!1 -> goto state4
	fi
state4:
	if
	:: W33_W41!1 -> goto state5
	fi
state5:
	if
	:: W33_W37!1 -> goto state6
	fi
state6:
	if
	:: W33_W35!1 -> goto state7
	fi
state7:
	if
	:: W33_W34!1 -> goto state8
	fi
state8:
	goto state2
} /* W33 */

proctype W34()
{
state1:
	goto state2
state2:
	if
	:: W33_W34?1 -> goto state3
	fi
state3:
	goto state2
} /* W34 */

proctype W35()
{
state1:
	goto state2
state2:
	if
	:: W33_W35?1 -> goto state3
	fi
state3:
	if
	:: W35_W36!1 -> goto state4
	fi
state4:
	goto state2
} /* W35 */

proctype W36()
{
state1:
	goto state2
state2:
	if
	:: W35_W36?1 -> goto state3
	fi
state3:
	goto state2
} /* W36 */

proctype W37()
{
state1:
	goto state2
state2:
	if
	:: W33_W37?1 -> goto state3
	fi
state3:
	if
	:: W37_W39!1 -> goto state4
	fi
state4:
	if
	:: W37_W38!1 -> goto state5
	fi
state5:
	goto state2
} /* W37 */

proctype W38()
{
state1:
	goto state2
state2:
	if
	:: W37_W38?1 -> goto state3
	fi
state3:
	goto state2
} /* W38 */

proctype W39()
{
state1:
	goto state2
state2:
	if
	:: W37_W39?1 -> goto state3
	fi
state3:
	if
	:: W39_W40!1 -> goto state4
	fi
state4:
	goto state2
} /* W39 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W3_W4?1 -> goto state3
	fi
state3:
	goto state2
} /* W4 */

proctype W40()
{
state1:
	goto state2
state2:
	if
	:: W39_W40?1 -> goto state3
	fi
state3:
	goto state2
} /* W40 */

proctype W41()
{
state1:
	goto state2
state2:
	if
	:: W33_W41?1 -> goto state3
	fi
state3:
	if
	:: W41_W45!1 -> goto state4
	fi
state4:
	if
	:: W41_W43!1 -> goto state5
	fi
state5:
	if
	:: W41_W42!1 -> goto state6
	fi
state6:
	goto state2
} /* W41 */

proctype W42()
{
state1:
	goto state2
state2:
	if
	:: W41_W42?1 -> goto state3
	fi
state3:
	goto state2
} /* W42 */

proctype W43()
{
state1:
	goto state2
state2:
	if
	:: W41_W43?1 -> goto state3
	fi
state3:
	if
	:: W43_W44!1 -> goto state4
	fi
state4:
	goto state2
} /* W43 */

proctype W44()
{
state1:
	goto state2
state2:
	if
	:: W43_W44?1 -> goto state3
	fi
state3:
	goto state2
} /* W44 */

proctype W45()
{
state1:
	goto state2
state2:
	if
	:: W41_W45?1 -> goto state3
	fi
state3:
	if
	:: W45_W47!1 -> goto state4
	fi
state4:
	if
	:: W45_W46!1 -> goto state5
	fi
state5:
	goto state2
} /* W45 */

proctype W46()
{
state1:
	goto state2
state2:
	if
	:: W45_W46?1 -> goto state3
	fi
state3:
	goto state2
} /* W46 */

proctype W47()
{
state1:
	goto state2
state2:
	if
	:: W45_W47?1 -> goto state3
	fi
state3:
	if
	:: W47_W48!1 -> goto state4
	fi
state4:
	goto state2
} /* W47 */

proctype W48()
{
state1:
	goto state2
state2:
	if
	:: W47_W48?1 -> goto state3
	fi
state3:
	goto state2
} /* W48 */

proctype W49()
{
state1:
	goto state2
state2:
	if
	:: W33_W49?1 -> goto state3
	fi
state3:
	if
	:: W49_W57!1 -> goto state4
	fi
state4:
	if
	:: W49_W53!1 -> goto state5
	fi
state5:
	if
	:: W49_W51!1 -> goto state6
	fi
state6:
	if
	:: W49_W50!1 -> goto state7
	fi
state7:
	goto state2
} /* W49 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_W7!1 -> goto state4
	fi
state4:
	if
	:: W5_W6!1 -> goto state5
	fi
state5:
	goto state2
} /* W5 */

proctype W50()
{
state1:
	goto state2
state2:
	if
	:: W49_W50?1 -> goto state3
	fi
state3:
	goto state2
} /* W50 */

proctype W51()
{
state1:
	goto state2
state2:
	if
	:: W49_W51?1 -> goto state3
	fi
state3:
	if
	:: W51_W52!1 -> goto state4
	fi
state4:
	goto state2
} /* W51 */

proctype W52()
{
state1:
	goto state2
state2:
	if
	:: W51_W52?1 -> goto state3
	fi
state3:
	goto state2
} /* W52 */

proctype W53()
{
state1:
	goto state2
state2:
	if
	:: W49_W53?1 -> goto state3
	fi
state3:
	if
	:: W53_W55!1 -> goto state4
	fi
state4:
	if
	:: W53_W54!1 -> goto state5
	fi
state5:
	goto state2
} /* W53 */

proctype W54()
{
state1:
	goto state2
state2:
	if
	:: W53_W54?1 -> goto state3
	fi
state3:
	goto state2
} /* W54 */

proctype W55()
{
state1:
	goto state2
state2:
	if
	:: W53_W55?1 -> goto state3
	fi
state3:
	if
	:: W55_W56!1 -> goto state4
	fi
state4:
	goto state2
} /* W55 */

proctype W56()
{
state1:
	goto state2
state2:
	if
	:: W55_W56?1 -> goto state3
	fi
state3:
	goto state2
} /* W56 */

proctype W57()
{
state1:
	goto state2
state2:
	if
	:: W49_W57?1 -> goto state3
	fi
state3:
	if
	:: W57_W61!1 -> goto state4
	fi
state4:
	if
	:: W57_W59!1 -> goto state5
	fi
state5:
	if
	:: W57_W58!1 -> goto state6
	fi
state6:
	goto state2
} /* W57 */

proctype W58()
{
state1:
	goto state2
state2:
	if
	:: W57_W58?1 -> goto state3
	fi
state3:
	goto state2
} /* W58 */

proctype W59()
{
state1:
	goto state2
state2:
	if
	:: W57_W59?1 -> goto state3
	fi
state3:
	if
	:: W59_W60!1 -> goto state4
	fi
state4:
	goto state2
} /* W59 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: W5_W6?1 -> goto state3
	fi
state3:
	goto state2
} /* W6 */

proctype W60()
{
state1:
	goto state2
state2:
	if
	:: W59_W60?1 -> goto state3
	fi
state3:
	goto state2
} /* W60 */

proctype W61()
{
state1:
	goto state2
state2:
	if
	:: W57_W61?1 -> goto state3
	fi
state3:
	if
	:: W61_W63!1 -> goto state4
	fi
state4:
	if
	:: W61_W62!1 -> goto state5
	fi
state5:
	goto state2
} /* W61 */

proctype W62()
{
state1:
	goto state2
state2:
	if
	:: W61_W62?1 -> goto state3
	fi
state3:
	goto state2
} /* W62 */

proctype W63()
{
state1:
	goto state2
state2:
	if
	:: W61_W63?1 -> goto state3
	fi
state3:
	if
	:: W63_W64!1 -> goto state4
	fi
state4:
	goto state2
} /* W63 */

proctype W64()
{
state1:
	goto state2
state2:
	if
	:: W63_W64?1 -> goto state3
	fi
state3:
	goto state2
} /* W64 */

proctype W65()
{
state1:
	goto state2
state2:
	if
	:: W1_W65?1 -> goto state3
	fi
state3:
	if
	:: W65_W97!1 -> goto state4
	fi
state4:
	if
	:: W65_W81!1 -> goto state5
	fi
state5:
	if
	:: W65_W73!1 -> goto state6
	fi
state6:
	if
	:: W65_W69!1 -> goto state7
	fi
state7:
	if
	:: W65_W67!1 -> goto state8
	fi
state8:
	if
	:: W65_W66!1 -> goto state9
	fi
state9:
	goto state2
} /* W65 */

proctype W66()
{
state1:
	goto state2
state2:
	if
	:: W65_W66?1 -> goto state3
	fi
state3:
	goto state2
} /* W66 */

proctype W67()
{
state1:
	goto state2
state2:
	if
	:: W65_W67?1 -> goto state3
	fi
state3:
	if
	:: W67_W68!1 -> goto state4
	fi
state4:
	goto state2
} /* W67 */

proctype W68()
{
state1:
	goto state2
state2:
	if
	:: W67_W68?1 -> goto state3
	fi
state3:
	goto state2
} /* W68 */

proctype W69()
{
state1:
	goto state2
state2:
	if
	:: W65_W69?1 -> goto state3
	fi
state3:
	if
	:: W69_W71!1 -> goto state4
	fi
state4:
	if
	:: W69_W70!1 -> goto state5
	fi
state5:
	goto state2
} /* W69 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W5_W7?1 -> goto state3
	fi
state3:
	if
	:: W7_W8!1 -> goto state4
	fi
state4:
	goto state2
} /* W7 */

proctype W70()
{
state1:
	goto state2
state2:
	if
	:: W69_W70?1 -> goto state3
	fi
state3:
	goto state2
} /* W70 */

proctype W71()
{
state1:
	goto state2
state2:
	if
	:: W69_W71?1 -> goto state3
	fi
state3:
	if
	:: W71_W72!1 -> goto state4
	fi
state4:
	goto state2
} /* W71 */

proctype W72()
{
state1:
	goto state2
state2:
	if
	:: W71_W72?1 -> goto state3
	fi
state3:
	goto state2
} /* W72 */

proctype W73()
{
state1:
	goto state2
state2:
	if
	:: W65_W73?1 -> goto state3
	fi
state3:
	if
	:: W73_W77!1 -> goto state4
	fi
state4:
	if
	:: W73_W75!1 -> goto state5
	fi
state5:
	if
	:: W73_W74!1 -> goto state6
	fi
state6:
	goto state2
} /* W73 */

proctype W74()
{
state1:
	goto state2
state2:
	if
	:: W73_W74?1 -> goto state3
	fi
state3:
	goto state2
} /* W74 */

proctype W75()
{
state1:
	goto state2
state2:
	if
	:: W73_W75?1 -> goto state3
	fi
state3:
	if
	:: W75_W76!1 -> goto state4
	fi
state4:
	goto state2
} /* W75 */

proctype W76()
{
state1:
	goto state2
state2:
	if
	:: W75_W76?1 -> goto state3
	fi
state3:
	goto state2
} /* W76 */

proctype W77()
{
state1:
	goto state2
state2:
	if
	:: W73_W77?1 -> goto state3
	fi
state3:
	if
	:: W77_W79!1 -> goto state4
	fi
state4:
	if
	:: W77_W78!1 -> goto state5
	fi
state5:
	goto state2
} /* W77 */

proctype W78()
{
state1:
	goto state2
state2:
	if
	:: W77_W78?1 -> goto state3
	fi
state3:
	goto state2
} /* W78 */

proctype W79()
{
state1:
	goto state2
state2:
	if
	:: W77_W79?1 -> goto state3
	fi
state3:
	if
	:: W79_W80!1 -> goto state4
	fi
state4:
	goto state2
} /* W79 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: W7_W8?1 -> goto state3
	fi
state3:
	goto state2
} /* W8 */

proctype W80()
{
state1:
	goto state2
state2:
	if
	:: W79_W80?1 -> goto state3
	fi
state3:
	goto state2
} /* W80 */

proctype W81()
{
state1:
	goto state2
state2:
	if
	:: W65_W81?1 -> goto state3
	fi
state3:
	if
	:: W81_W89!1 -> goto state4
	fi
state4:
	if
	:: W81_W85!1 -> goto state5
	fi
state5:
	if
	:: W81_W83!1 -> goto state6
	fi
state6:
	if
	:: W81_W82!1 -> goto state7
	fi
state7:
	goto state2
} /* W81 */

proctype W82()
{
state1:
	goto state2
state2:
	if
	:: W81_W82?1 -> goto state3
	fi
state3:
	goto state2
} /* W82 */

proctype W83()
{
state1:
	goto state2
state2:
	if
	:: W81_W83?1 -> goto state3
	fi
state3:
	if
	:: W83_W84!1 -> goto state4
	fi
state4:
	goto state2
} /* W83 */

proctype W84()
{
state1:
	goto state2
state2:
	if
	:: W83_W84?1 -> goto state3
	fi
state3:
	goto state2
} /* W84 */

proctype W85()
{
state1:
	goto state2
state2:
	if
	:: W81_W85?1 -> goto state3
	fi
state3:
	if
	:: W85_W87!1 -> goto state4
	fi
state4:
	if
	:: W85_W86!1 -> goto state5
	fi
state5:
	goto state2
} /* W85 */

proctype W86()
{
state1:
	goto state2
state2:
	if
	:: W85_W86?1 -> goto state3
	fi
state3:
	goto state2
} /* W86 */

proctype W87()
{
state1:
	goto state2
state2:
	if
	:: W85_W87?1 -> goto state3
	fi
state3:
	if
	:: W87_W88!1 -> goto state4
	fi
state4:
	goto state2
} /* W87 */

proctype W88()
{
state1:
	goto state2
state2:
	if
	:: W87_W88?1 -> goto state3
	fi
state3:
	goto state2
} /* W88 */

proctype W89()
{
state1:
	goto state2
state2:
	if
	:: W81_W89?1 -> goto state3
	fi
state3:
	if
	:: W89_W93!1 -> goto state4
	fi
state4:
	if
	:: W89_W91!1 -> goto state5
	fi
state5:
	if
	:: W89_W90!1 -> goto state6
	fi
state6:
	goto state2
} /* W89 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: W1_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_W13!1 -> goto state4
	fi
state4:
	if
	:: W9_W11!1 -> goto state5
	fi
state5:
	if
	:: W9_W10!1 -> goto state6
	fi
state6:
	goto state2
} /* W9 */

proctype W90()
{
state1:
	goto state2
state2:
	if
	:: W89_W90?1 -> goto state3
	fi
state3:
	goto state2
} /* W90 */

proctype W91()
{
state1:
	goto state2
state2:
	if
	:: W89_W91?1 -> goto state3
	fi
state3:
	if
	:: W91_W92!1 -> goto state4
	fi
state4:
	goto state2
} /* W91 */

proctype W92()
{
state1:
	goto state2
state2:
	if
	:: W91_W92?1 -> goto state3
	fi
state3:
	goto state2
} /* W92 */

proctype W93()
{
state1:
	goto state2
state2:
	if
	:: W89_W93?1 -> goto state3
	fi
state3:
	if
	:: W93_W95!1 -> goto state4
	fi
state4:
	if
	:: W93_W94!1 -> goto state5
	fi
state5:
	goto state2
} /* W93 */

proctype W94()
{
state1:
	goto state2
state2:
	if
	:: W93_W94?1 -> goto state3
	fi
state3:
	goto state2
} /* W94 */

proctype W95()
{
state1:
	goto state2
state2:
	if
	:: W93_W95?1 -> goto state3
	fi
state3:
	goto state2
} /* W95 */

proctype W97()
{
state1:
	goto state2
state2:
	if
	:: W65_W97?1 -> goto state3
	fi
state3:
	if
	:: W97_W113!1 -> goto state4
	fi
state4:
	if
	:: W97_W105!1 -> goto state5
	fi
state5:
	if
	:: W97_W101!1 -> goto state6
	fi
state6:
	if
	:: W97_W99!1 -> goto state7
	fi
state7:
	goto state2
} /* W97 */

proctype W99()
{
state1:
	goto state2
state2:
	if
	:: W97_W99?1 -> goto state3
	fi
state3:
	goto state2
} /* W99 */
