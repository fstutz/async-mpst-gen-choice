chan W1_W5 = [1] of { int };
chan W13_W15 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W9_W11 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W1_W9 = [1] of { int };
chan W9_W13 = [1] of { int };

init
{
	run W1();
	run W11();
	run W13();
	run W15();
	run W3();
	run W5();
	run W7();
	run W9()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W9!1 -> goto state3
	fi
state3:
	if
	:: W1_W5!1 -> goto state4
	fi
state4:
	if
	:: W1_W3!1 -> goto state5
	fi
state5:
	goto state2
} /* W1 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: W9_W11?1 -> goto state3
	fi
state3:
	goto state2
} /* W11 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: W9_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_W15!1 -> goto state4
	fi
state4:
	goto state2
} /* W13 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: W13_W15?1 -> goto state3
	fi
state3:
	goto state2
} /* W15 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	goto state2
} /* W3 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_W7!1 -> goto state4
	fi
state4:
	goto state2
} /* W5 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W5_W7?1 -> goto state3
	fi
state3:
	goto state2
} /* W7 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: W1_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_W13!1 -> goto state4
	fi
state4:
	if
	:: W9_W11!1 -> goto state5
	fi
state5:
	goto state2
} /* W9 */
