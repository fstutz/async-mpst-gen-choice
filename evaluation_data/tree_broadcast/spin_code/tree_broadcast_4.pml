chan W1_W5 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W5_W7 = [1] of { int };

init
{
	run W1();
	run W3();
	run W5();
	run W7()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W5!1 -> goto state3
	fi
state3:
	if
	:: W1_W3!1 -> goto state4
	fi
state4:
	goto state2
} /* W1 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	goto state2
} /* W3 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_W7!1 -> goto state4
	fi
state4:
	goto state2
} /* W5 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W5_W7?1 -> goto state3
	fi
state3:
	goto state2
} /* W7 */
