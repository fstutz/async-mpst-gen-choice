chan W13_W15 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W5_W6 = [1] of { int };
chan W9_W11 = [1] of { int };
chan W9_W13 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W1_W17 = [1] of { int };
chan W17_W19 = [1] of { int };
chan W17_W25 = [1] of { int };
chan W21_W23 = [1] of { int };
chan W29_W31 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W1_W5 = [1] of { int };
chan W25_W29 = [1] of { int };
chan W1_W9 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W25_W27 = [1] of { int };
chan W17_W21 = [1] of { int };

init
{
	run W1();
	run W11();
	run W13();
	run W15();
	run W17();
	run W19();
	run W2();
	run W21();
	run W23();
	run W25();
	run W27();
	run W29();
	run W3();
	run W31();
	run W4();
	run W5();
	run W6();
	run W7();
	run W9()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W17!1 -> goto state3
	fi
state3:
	if
	:: W1_W9!1 -> goto state4
	fi
state4:
	if
	:: W1_W5!1 -> goto state5
	fi
state5:
	if
	:: W1_W3!1 -> goto state6
	fi
state6:
	if
	:: W1_W2!1 -> goto state7
	fi
state7:
	goto state2
} /* W1 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: W9_W11?1 -> goto state3
	fi
state3:
	goto state2
} /* W11 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: W9_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_W15!1 -> goto state4
	fi
state4:
	goto state2
} /* W13 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: W13_W15?1 -> goto state3
	fi
state3:
	goto state2
} /* W15 */

proctype W17()
{
state1:
	goto state2
state2:
	if
	:: W1_W17?1 -> goto state3
	fi
state3:
	if
	:: W17_W25!1 -> goto state4
	fi
state4:
	if
	:: W17_W21!1 -> goto state5
	fi
state5:
	if
	:: W17_W19!1 -> goto state6
	fi
state6:
	goto state2
} /* W17 */

proctype W19()
{
state1:
	goto state2
state2:
	if
	:: W17_W19?1 -> goto state3
	fi
state3:
	goto state2
} /* W19 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	goto state2
} /* W2 */

proctype W21()
{
state1:
	goto state2
state2:
	if
	:: W17_W21?1 -> goto state3
	fi
state3:
	if
	:: W21_W23!1 -> goto state4
	fi
state4:
	goto state2
} /* W21 */

proctype W23()
{
state1:
	goto state2
state2:
	if
	:: W21_W23?1 -> goto state3
	fi
state3:
	goto state2
} /* W23 */

proctype W25()
{
state1:
	goto state2
state2:
	if
	:: W17_W25?1 -> goto state3
	fi
state3:
	if
	:: W25_W29!1 -> goto state4
	fi
state4:
	if
	:: W25_W27!1 -> goto state5
	fi
state5:
	goto state2
} /* W25 */

proctype W27()
{
state1:
	goto state2
state2:
	if
	:: W25_W27?1 -> goto state3
	fi
state3:
	goto state2
} /* W27 */

proctype W29()
{
state1:
	goto state2
state2:
	if
	:: W25_W29?1 -> goto state3
	fi
state3:
	if
	:: W29_W31!1 -> goto state4
	fi
state4:
	goto state2
} /* W29 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_W4!1 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W31()
{
state1:
	goto state2
state2:
	if
	:: W29_W31?1 -> goto state3
	fi
state3:
	goto state2
} /* W31 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W3_W4?1 -> goto state3
	fi
state3:
	goto state2
} /* W4 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_W7!1 -> goto state4
	fi
state4:
	if
	:: W5_W6!1 -> goto state5
	fi
state5:
	goto state2
} /* W5 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: W5_W6?1 -> goto state3
	fi
state3:
	goto state2
} /* W6 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W5_W7?1 -> goto state3
	fi
state3:
	goto state2
} /* W7 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: W1_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_W13!1 -> goto state4
	fi
state4:
	if
	:: W9_W11!1 -> goto state5
	fi
state5:
	goto state2
} /* W9 */
