chan W1_W3 = [1] of { int };

init
{
	run W1();
	run W3()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W3!1 -> goto state3
	fi
state3:
	goto state2
} /* W1 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	goto state2
} /* W3 */
