chan W27_W28 = [1] of { int };
chan W13_W15 = [1] of { int };
chan W1_W2 = [1] of { int };
chan W61_W63 = [1] of { int };
chan W9_W11 = [1] of { int };
chan W35_W36 = [1] of { int };
chan W33_W34 = [1] of { int };
chan W49_W51 = [1] of { int };
chan W39_W40 = [1] of { int };
chan W49_W53 = [1] of { int };
chan W17_W25 = [1] of { int };
chan W17_W19 = [1] of { int };
chan W21_W23 = [1] of { int };
chan W1_W3 = [1] of { int };
chan W29_W31 = [1] of { int };
chan W49_W57 = [1] of { int };
chan W1_W5 = [1] of { int };
chan W7_W8 = [1] of { int };
chan W41_W43 = [1] of { int };
chan W9_W10 = [1] of { int };
chan W21_W22 = [1] of { int };
chan W33_W49 = [1] of { int };
chan W11_W12 = [1] of { int };
chan W57_W59 = [1] of { int };
chan W25_W27 = [1] of { int };
chan W25_W26 = [1] of { int };
chan W17_W21 = [1] of { int };
chan W37_W39 = [1] of { int };
chan W41_W42 = [1] of { int };
chan W5_W6 = [1] of { int };
chan W23_W24 = [1] of { int };
chan W9_W13 = [1] of { int };
chan W5_W7 = [1] of { int };
chan W57_W61 = [1] of { int };
chan W45_W47 = [1] of { int };
chan W13_W14 = [1] of { int };
chan W37_W38 = [1] of { int };
chan W1_W17 = [1] of { int };
chan W29_W30 = [1] of { int };
chan W17_W18 = [1] of { int };
chan W53_W55 = [1] of { int };
chan W25_W29 = [1] of { int };
chan W41_W45 = [1] of { int };
chan W33_W41 = [1] of { int };
chan W1_W9 = [1] of { int };
chan W3_W4 = [1] of { int };
chan W33_W35 = [1] of { int };
chan W43_W44 = [1] of { int };
chan W1_W33 = [1] of { int };
chan W33_W37 = [1] of { int };
chan W19_W20 = [1] of { int };
chan W31_W32 = [1] of { int };
chan W15_W16 = [1] of { int };

init
{
	run W1();
	run W10();
	run W11();
	run W12();
	run W13();
	run W14();
	run W15();
	run W16();
	run W17();
	run W18();
	run W19();
	run W2();
	run W20();
	run W21();
	run W22();
	run W23();
	run W24();
	run W25();
	run W26();
	run W27();
	run W28();
	run W29();
	run W3();
	run W30();
	run W31();
	run W32();
	run W33();
	run W34();
	run W35();
	run W36();
	run W37();
	run W38();
	run W39();
	run W4();
	run W40();
	run W41();
	run W42();
	run W43();
	run W44();
	run W45();
	run W47();
	run W49();
	run W5();
	run W51();
	run W53();
	run W55();
	run W57();
	run W59();
	run W6();
	run W61();
	run W63();
	run W7();
	run W8();
	run W9()
}

proctype W1()
{
state1:
	goto state2
state2:
	if
	:: W1_W33!1 -> goto state3
	fi
state3:
	if
	:: W1_W17!1 -> goto state4
	fi
state4:
	if
	:: W1_W9!1 -> goto state5
	fi
state5:
	if
	:: W1_W5!1 -> goto state6
	fi
state6:
	if
	:: W1_W3!1 -> goto state7
	fi
state7:
	if
	:: W1_W2!1 -> goto state8
	fi
state8:
	goto state2
} /* W1 */

proctype W10()
{
state1:
	goto state2
state2:
	if
	:: W9_W10?1 -> goto state3
	fi
state3:
	goto state2
} /* W10 */

proctype W11()
{
state1:
	goto state2
state2:
	if
	:: W9_W11?1 -> goto state3
	fi
state3:
	if
	:: W11_W12!1 -> goto state4
	fi
state4:
	goto state2
} /* W11 */

proctype W12()
{
state1:
	goto state2
state2:
	if
	:: W11_W12?1 -> goto state3
	fi
state3:
	goto state2
} /* W12 */

proctype W13()
{
state1:
	goto state2
state2:
	if
	:: W9_W13?1 -> goto state3
	fi
state3:
	if
	:: W13_W15!1 -> goto state4
	fi
state4:
	if
	:: W13_W14!1 -> goto state5
	fi
state5:
	goto state2
} /* W13 */

proctype W14()
{
state1:
	goto state2
state2:
	if
	:: W13_W14?1 -> goto state3
	fi
state3:
	goto state2
} /* W14 */

proctype W15()
{
state1:
	goto state2
state2:
	if
	:: W13_W15?1 -> goto state3
	fi
state3:
	if
	:: W15_W16!1 -> goto state4
	fi
state4:
	goto state2
} /* W15 */

proctype W16()
{
state1:
	goto state2
state2:
	if
	:: W15_W16?1 -> goto state3
	fi
state3:
	goto state2
} /* W16 */

proctype W17()
{
state1:
	goto state2
state2:
	if
	:: W1_W17?1 -> goto state3
	fi
state3:
	if
	:: W17_W25!1 -> goto state4
	fi
state4:
	if
	:: W17_W21!1 -> goto state5
	fi
state5:
	if
	:: W17_W19!1 -> goto state6
	fi
state6:
	if
	:: W17_W18!1 -> goto state7
	fi
state7:
	goto state2
} /* W17 */

proctype W18()
{
state1:
	goto state2
state2:
	if
	:: W17_W18?1 -> goto state3
	fi
state3:
	goto state2
} /* W18 */

proctype W19()
{
state1:
	goto state2
state2:
	if
	:: W17_W19?1 -> goto state3
	fi
state3:
	if
	:: W19_W20!1 -> goto state4
	fi
state4:
	goto state2
} /* W19 */

proctype W2()
{
state1:
	goto state2
state2:
	if
	:: W1_W2?1 -> goto state3
	fi
state3:
	goto state2
} /* W2 */

proctype W20()
{
state1:
	goto state2
state2:
	if
	:: W19_W20?1 -> goto state3
	fi
state3:
	goto state2
} /* W20 */

proctype W21()
{
state1:
	goto state2
state2:
	if
	:: W17_W21?1 -> goto state3
	fi
state3:
	if
	:: W21_W23!1 -> goto state4
	fi
state4:
	if
	:: W21_W22!1 -> goto state5
	fi
state5:
	goto state2
} /* W21 */

proctype W22()
{
state1:
	goto state2
state2:
	if
	:: W21_W22?1 -> goto state3
	fi
state3:
	goto state2
} /* W22 */

proctype W23()
{
state1:
	goto state2
state2:
	if
	:: W21_W23?1 -> goto state3
	fi
state3:
	if
	:: W23_W24!1 -> goto state4
	fi
state4:
	goto state2
} /* W23 */

proctype W24()
{
state1:
	goto state2
state2:
	if
	:: W23_W24?1 -> goto state3
	fi
state3:
	goto state2
} /* W24 */

proctype W25()
{
state1:
	goto state2
state2:
	if
	:: W17_W25?1 -> goto state3
	fi
state3:
	if
	:: W25_W29!1 -> goto state4
	fi
state4:
	if
	:: W25_W27!1 -> goto state5
	fi
state5:
	if
	:: W25_W26!1 -> goto state6
	fi
state6:
	goto state2
} /* W25 */

proctype W26()
{
state1:
	goto state2
state2:
	if
	:: W25_W26?1 -> goto state3
	fi
state3:
	goto state2
} /* W26 */

proctype W27()
{
state1:
	goto state2
state2:
	if
	:: W25_W27?1 -> goto state3
	fi
state3:
	if
	:: W27_W28!1 -> goto state4
	fi
state4:
	goto state2
} /* W27 */

proctype W28()
{
state1:
	goto state2
state2:
	if
	:: W27_W28?1 -> goto state3
	fi
state3:
	goto state2
} /* W28 */

proctype W29()
{
state1:
	goto state2
state2:
	if
	:: W25_W29?1 -> goto state3
	fi
state3:
	if
	:: W29_W31!1 -> goto state4
	fi
state4:
	if
	:: W29_W30!1 -> goto state5
	fi
state5:
	goto state2
} /* W29 */

proctype W3()
{
state1:
	goto state2
state2:
	if
	:: W1_W3?1 -> goto state3
	fi
state3:
	if
	:: W3_W4!1 -> goto state4
	fi
state4:
	goto state2
} /* W3 */

proctype W30()
{
state1:
	goto state2
state2:
	if
	:: W29_W30?1 -> goto state3
	fi
state3:
	goto state2
} /* W30 */

proctype W31()
{
state1:
	goto state2
state2:
	if
	:: W29_W31?1 -> goto state3
	fi
state3:
	if
	:: W31_W32!1 -> goto state4
	fi
state4:
	goto state2
} /* W31 */

proctype W32()
{
state1:
	goto state2
state2:
	if
	:: W31_W32?1 -> goto state3
	fi
state3:
	goto state2
} /* W32 */

proctype W33()
{
state1:
	goto state2
state2:
	if
	:: W1_W33?1 -> goto state3
	fi
state3:
	if
	:: W33_W49!1 -> goto state4
	fi
state4:
	if
	:: W33_W41!1 -> goto state5
	fi
state5:
	if
	:: W33_W37!1 -> goto state6
	fi
state6:
	if
	:: W33_W35!1 -> goto state7
	fi
state7:
	if
	:: W33_W34!1 -> goto state8
	fi
state8:
	goto state2
} /* W33 */

proctype W34()
{
state1:
	goto state2
state2:
	if
	:: W33_W34?1 -> goto state3
	fi
state3:
	goto state2
} /* W34 */

proctype W35()
{
state1:
	goto state2
state2:
	if
	:: W33_W35?1 -> goto state3
	fi
state3:
	if
	:: W35_W36!1 -> goto state4
	fi
state4:
	goto state2
} /* W35 */

proctype W36()
{
state1:
	goto state2
state2:
	if
	:: W35_W36?1 -> goto state3
	fi
state3:
	goto state2
} /* W36 */

proctype W37()
{
state1:
	goto state2
state2:
	if
	:: W33_W37?1 -> goto state3
	fi
state3:
	if
	:: W37_W39!1 -> goto state4
	fi
state4:
	if
	:: W37_W38!1 -> goto state5
	fi
state5:
	goto state2
} /* W37 */

proctype W38()
{
state1:
	goto state2
state2:
	if
	:: W37_W38?1 -> goto state3
	fi
state3:
	goto state2
} /* W38 */

proctype W39()
{
state1:
	goto state2
state2:
	if
	:: W37_W39?1 -> goto state3
	fi
state3:
	if
	:: W39_W40!1 -> goto state4
	fi
state4:
	goto state2
} /* W39 */

proctype W4()
{
state1:
	goto state2
state2:
	if
	:: W3_W4?1 -> goto state3
	fi
state3:
	goto state2
} /* W4 */

proctype W40()
{
state1:
	goto state2
state2:
	if
	:: W39_W40?1 -> goto state3
	fi
state3:
	goto state2
} /* W40 */

proctype W41()
{
state1:
	goto state2
state2:
	if
	:: W33_W41?1 -> goto state3
	fi
state3:
	if
	:: W41_W45!1 -> goto state4
	fi
state4:
	if
	:: W41_W43!1 -> goto state5
	fi
state5:
	if
	:: W41_W42!1 -> goto state6
	fi
state6:
	goto state2
} /* W41 */

proctype W42()
{
state1:
	goto state2
state2:
	if
	:: W41_W42?1 -> goto state3
	fi
state3:
	goto state2
} /* W42 */

proctype W43()
{
state1:
	goto state2
state2:
	if
	:: W41_W43?1 -> goto state3
	fi
state3:
	if
	:: W43_W44!1 -> goto state4
	fi
state4:
	goto state2
} /* W43 */

proctype W44()
{
state1:
	goto state2
state2:
	if
	:: W43_W44?1 -> goto state3
	fi
state3:
	goto state2
} /* W44 */

proctype W45()
{
state1:
	goto state2
state2:
	if
	:: W41_W45?1 -> goto state3
	fi
state3:
	if
	:: W45_W47!1 -> goto state4
	fi
state4:
	goto state2
} /* W45 */

proctype W47()
{
state1:
	goto state2
state2:
	if
	:: W45_W47?1 -> goto state3
	fi
state3:
	goto state2
} /* W47 */

proctype W49()
{
state1:
	goto state2
state2:
	if
	:: W33_W49?1 -> goto state3
	fi
state3:
	if
	:: W49_W57!1 -> goto state4
	fi
state4:
	if
	:: W49_W53!1 -> goto state5
	fi
state5:
	if
	:: W49_W51!1 -> goto state6
	fi
state6:
	goto state2
} /* W49 */

proctype W5()
{
state1:
	goto state2
state2:
	if
	:: W1_W5?1 -> goto state3
	fi
state3:
	if
	:: W5_W7!1 -> goto state4
	fi
state4:
	if
	:: W5_W6!1 -> goto state5
	fi
state5:
	goto state2
} /* W5 */

proctype W51()
{
state1:
	goto state2
state2:
	if
	:: W49_W51?1 -> goto state3
	fi
state3:
	goto state2
} /* W51 */

proctype W53()
{
state1:
	goto state2
state2:
	if
	:: W49_W53?1 -> goto state3
	fi
state3:
	if
	:: W53_W55!1 -> goto state4
	fi
state4:
	goto state2
} /* W53 */

proctype W55()
{
state1:
	goto state2
state2:
	if
	:: W53_W55?1 -> goto state3
	fi
state3:
	goto state2
} /* W55 */

proctype W57()
{
state1:
	goto state2
state2:
	if
	:: W49_W57?1 -> goto state3
	fi
state3:
	if
	:: W57_W61!1 -> goto state4
	fi
state4:
	if
	:: W57_W59!1 -> goto state5
	fi
state5:
	goto state2
} /* W57 */

proctype W59()
{
state1:
	goto state2
state2:
	if
	:: W57_W59?1 -> goto state3
	fi
state3:
	goto state2
} /* W59 */

proctype W6()
{
state1:
	goto state2
state2:
	if
	:: W5_W6?1 -> goto state3
	fi
state3:
	goto state2
} /* W6 */

proctype W61()
{
state1:
	goto state2
state2:
	if
	:: W57_W61?1 -> goto state3
	fi
state3:
	if
	:: W61_W63!1 -> goto state4
	fi
state4:
	goto state2
} /* W61 */

proctype W63()
{
state1:
	goto state2
state2:
	if
	:: W61_W63?1 -> goto state3
	fi
state3:
	goto state2
} /* W63 */

proctype W7()
{
state1:
	goto state2
state2:
	if
	:: W5_W7?1 -> goto state3
	fi
state3:
	if
	:: W7_W8!1 -> goto state4
	fi
state4:
	goto state2
} /* W7 */

proctype W8()
{
state1:
	goto state2
state2:
	if
	:: W7_W8?1 -> goto state3
	fi
state3:
	goto state2
} /* W8 */

proctype W9()
{
state1:
	goto state2
state2:
	if
	:: W1_W9?1 -> goto state3
	fi
state3:
	if
	:: W9_W13!1 -> goto state4
	fi
state4:
	if
	:: W9_W11!1 -> goto state5
	fi
state5:
	if
	:: W9_W10!1 -> goto state6
	fi
state6:
	goto state2
} /* W9 */
