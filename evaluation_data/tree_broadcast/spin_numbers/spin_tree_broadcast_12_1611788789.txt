> promela file    : ../tree_broadcast/spin_code/tree_broadcast_12.pml
> date            : 28-Jan-2021 00:06:30
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_12.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 212 byte, depth reached 294841, errors: 0
   589836 states, stored
  2949121 states, matched
  3538957 transitions (= stored+matched)
        0 atomic steps
hash conflicts:   1017117 (resolved)

Stats on memory usage (in Megabytes):
  135.003	equivalent memory usage for states (stored*(State-vector + overhead))
   91.003	actual memory usage for states (compression: 67.41%)
         	state-vector as stored = 134 byte + 28 byte overhead
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  628.683	total actual memory usage



pan: elapsed time 3.41 seconds
pan: rate 172972.43 states/second

real	0m3.690s
user	0m3.413s
sys	0m0.269s
