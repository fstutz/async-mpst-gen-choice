> promela file    : ../tree_broadcast/spin_code/tree_broadcast_8.pml
> date            : 28-Jan-2021 00:06:26
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_8.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 140 byte, depth reached 2465, errors: 0
     4616 states, stored
    13825 states, matched
    18441 transitions (= stored+matched)
        0 atomic steps
hash conflicts:        53 (resolved)

Stats on memory usage (in Megabytes):
    0.740	equivalent memory usage for states (stored*(State-vector + overhead))
    1.554	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  539.230	total actual memory usage



pan: elapsed time 0.01 seconds

real	0m0.287s
user	0m0.081s
sys	0m0.197s
