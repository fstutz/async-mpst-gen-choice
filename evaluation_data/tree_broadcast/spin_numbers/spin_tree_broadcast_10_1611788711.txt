> promela file    : ../tree_broadcast/spin_code/tree_broadcast_10.pml
> date            : 28-Jan-2021 00:05:11
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_10.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 172 byte, depth reached 23559, errors: 0
    49162 states, stored
   196609 states, matched
   245771 transitions (= stored+matched)
        0 atomic steps
hash conflicts:      5846 (resolved)

Stats on memory usage (in Megabytes):
    9.377	equivalent memory usage for states (stored*(State-vector + overhead))
    7.413	actual memory usage for states (compression: 79.06%)
         	state-vector as stored = 130 byte + 28 byte overhead
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  545.089	total actual memory usage



pan: elapsed time 0.2 seconds
pan: rate    245810 states/second

real	0m0.469s
user	0m0.240s
sys	0m0.224s
