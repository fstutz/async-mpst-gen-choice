> promela file    : ../tree_broadcast/spin_code/tree_broadcast_13.pml
> date            : 28-Jan-2021 00:06:34
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_13.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n
Depth=  635482 States=    1e+06 Transitions= 4.34e+06 Memory=   698.995	t=     4.55 R=   2e+05

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 228 byte, depth reached 816663, errors: 0
  1572877 states, stored
  8650753 states, matched
 10223630 transitions (= stored+matched)
        0 atomic steps
hash conflicts:   8266745 (resolved)

Stats on memory usage (in Megabytes):
  384.003	equivalent memory usage for states (stored*(State-vector + overhead))
  253.121	actual memory usage for states (compression: 65.92%)
         	state-vector as stored = 141 byte + 28 byte overhead
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  790.792	total actual memory usage



pan: elapsed time 11.2 seconds
pan: rate 140060.28 states/second

real	0m11.508s
user	0m11.157s
sys	0m0.344s
