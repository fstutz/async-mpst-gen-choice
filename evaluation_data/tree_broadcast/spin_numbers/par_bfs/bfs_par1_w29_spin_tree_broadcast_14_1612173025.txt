> promela file    : ../tree_broadcast/spin_code/tree_broadcast_14.pml
> date            : 01-Feb-2021 10:50:25
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_14.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m100000000 -c1 -w29 -u1 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 1 core
cpu00 Depth=      53 States= 1.07e+06 Transitions= 6.84e+06 Memory=     0.391	SharedMLeft= 1476 t=   10.8 R=  1e+05
cpu00 Depth=      61 States= 2.15e+06 Transitions= 1.43e+07 Memory=     0.391	SharedMLeft= 1436 t=   20.1 R=  1e+05
cpu00 Depth=      67 States=  3.1e+06 Transitions= 2.11e+07 Memory=     0.391	SharedMLeft= 1411 t=   28.2 R=  1e+05
cpu00 Depth=      74 States= 4.09e+06 Transitions= 2.82e+07 Memory=     0.391	SharedMLeft= 1389 t=   36.4 R=  1e+05
cpu00 Depth=      82 States= 5.03e+06 Transitions=  3.5e+07 Memory=     0.391	SharedMLeft= 1367 t=   44.2 R=  1e+05
cpu00 Depth=      98 States= 6.01e+06 Transitions= 4.21e+07 Memory=     0.391	SharedMLeft= 1345 t=   52.4 R=  1e+05

(Spin Version 6.5.1 -- 31 July 2020)
	+ Multi-Core (using 1 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 244 byte, depth reached 137, errors: 0
  6291470 states, stored
	6.29147e+06 nominal states (stored-atomic)
 37748737 states, matched
 44040207 transitions (= stored+matched)
        0 atomic steps
hash conflicts:    258166 (resolved)
the hash table is 1.17 % filled (hint: rerun with -w23 to reduce runtime)

Stats on memory usage (in Megabytes):
 1584.004	equivalent memory usage for states (stored*(State-vector + overhead))
 6656.007	shared memory used for hash table (-w29)
    0.391	total non-shared memory usage

 6853.294	total shared memory usage


pan: elapsed time 54.6 seconds
pan: rate 115165.11 states/second
pan: releasing shared memory...done

real	0m55.572s
user	0m51.130s
sys	0m4.429s
