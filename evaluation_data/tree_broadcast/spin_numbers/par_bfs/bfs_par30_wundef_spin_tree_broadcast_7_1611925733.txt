> promela file    : ../tree_broadcast/spin_code/tree_broadcast_7.pml
> date            : 29-Jan-2021 14:08:53
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_7.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -u30 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 30 cores

(Spin Version 6.5.1 -- 31 July 2020)
	+ Multi-Core (using 30 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 124 byte, depth reached 43, errors: 0
     1159 states, stored
	    1159 nominal states (stored-atomic)
     2881 states, matched
     4040 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)
the hash table is 0.00691 % filled

Stats on memory usage (in Megabytes):
    0.159	equivalent memory usage for states (stored*(State-vector + overhead))
  208.004	shared memory used for hash table (-w24)
   11.719	other (proc and chan stacks)
   11.719	total non-shared memory usage

 1538.821	total shared memory usage


pan: elapsed time 0.01 seconds
pan: releasing shared memory...done

real	0m1.018s
user	0m0.004s
sys	0m0.008s
