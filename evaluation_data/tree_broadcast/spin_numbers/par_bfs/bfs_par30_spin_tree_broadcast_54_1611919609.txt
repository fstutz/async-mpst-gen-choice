> promela file    : ../tree_broadcast/spin_code/tree_broadcast_54.pml
> date            : 29-Jan-2021 12:26:50
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_54.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -u30 -w19 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 30 cores
cpu00: hash table is full

(Spin Version 6.5.1 -- 31 July 2020)
Warning: Search incomplete
	+ Multi-Core (using 30 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 924 byte, depth reached 71, errors: 0
   524288 states, stored
  3450734 states lost (lack of queue memory)
	  524288 nominal states (stored-atomic)
  4654249 states, matched
  5178537 transitions (= stored+matched)
        0 atomic steps
hash conflicts:   1003272 (resolved)
the hash table is 100 % filled

Stats on memory usage (in Megabytes):
  472.000	equivalent memory usage for states (stored*(State-vector + overhead))
    6.527	shared memory used for hash table (-w19)
   11.719	other (proc and chan stacks)
   11.719	total non-shared memory usage

 1720.813	total shared memory usage


pan: elapsed time 0.57 seconds
pan: rate 919803.51 states/second
pan: releasing shared memory...done

real	0m1.611s
user	0m0.523s
sys	0m0.080s
