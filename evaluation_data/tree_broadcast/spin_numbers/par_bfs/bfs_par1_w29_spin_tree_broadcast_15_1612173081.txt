> promela file    : ../tree_broadcast/spin_code/tree_broadcast_15.pml
> date            : 01-Feb-2021 10:51:21
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_15.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m100000000 -c1 -w29 -u1 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 1 core
cpu00 Depth=      49 States= 1.14e+06 Transitions= 7.37e+06 Memory=     0.391	SharedMLeft= 1455 t=   12.1 R=  9e+04
cpu00 Depth=      54 States= 2.18e+06 Transitions= 1.47e+07 Memory=     0.391	SharedMLeft= 1403 t=   21.7 R=  1e+05
cpu00 Depth=      58 States=  3.3e+06 Transitions= 2.28e+07 Memory=     0.391	SharedMLeft= 1356 t=   31.7 R=  1e+05
cpu00 Depth=      61 States= 4.32e+06 Transitions= 3.03e+07 Memory=     0.391	SharedMLeft= 1316 t=   40.8 R=  1e+05
cpu00 Depth=      63 States= 5.09e+06 Transitions=  3.6e+07 Memory=     0.391	SharedMLeft= 1285 t=   47.7 R=  1e+05
cpu00 Depth=      66 States= 6.36e+06 Transitions= 4.55e+07 Memory=     0.391	SharedMLeft= 1244 t=   59.1 R=  1e+05
cpu00 Depth=      68 States= 7.25e+06 Transitions= 5.22e+07 Memory=     0.391	SharedMLeft= 1218 t=   67.2 R=  1e+05
cpu00 Depth=      70 States= 8.15e+06 Transitions=  5.9e+07 Memory=     0.391	SharedMLeft= 1194 t=   75.4 R=  1e+05
cpu00 Depth=      72 States= 9.04e+06 Transitions= 6.59e+07 Memory=     0.391	SharedMLeft= 1174 t=   83.7 R=  1e+05
cpu00 Depth=      75 States= 1.03e+07 Transitions= 7.58e+07 Memory=     0.391	SharedMLeft= 1144 t=   95.7 R=  1e+05
cpu00 Depth=      77 States= 1.12e+07 Transitions= 8.22e+07 Memory=     0.391	SharedMLeft= 1126 t=    103 R=  1e+05
cpu00 Depth=      80 States= 1.23e+07 Transitions= 9.13e+07 Memory=     0.391	SharedMLeft= 1099 t=    114 R=  1e+05
cpu00 Depth=      82 States= 1.31e+07 Transitions=  9.7e+07 Memory=     0.391	SharedMLeft= 1082 t=    121 R=  1e+05
cpu00 Depth=      85 States= 1.41e+07 Transitions= 1.05e+08 Memory=     0.391	SharedMLeft= 1057 t=    131 R=  1e+05
cpu00 Depth=      88 States= 1.51e+07 Transitions= 1.13e+08 Memory=     0.391	SharedMLeft= 1035 t=    140 R=  1e+05
cpu00 Depth=      92 States= 1.62e+07 Transitions= 1.21e+08 Memory=     0.391	SharedMLeft= 1011 t=    150 R=  1e+05
cpu00 Depth=      97 States= 1.71e+07 Transitions= 1.28e+08 Memory=     0.391	SharedMLeft=  989 t=    159 R=  1e+05
cpu00 Depth=     105 States= 1.81e+07 Transitions= 1.35e+08 Memory=     0.391	SharedMLeft=  968 t=    168 R=  1e+05

(Spin Version 6.5.1 -- 31 July 2020)
	+ Multi-Core (using 1 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 260 byte, depth reached 150, errors: 0
 18874383 states, stored
	1.88744e+07 nominal states (stored-atomic)
1.2268339e+08 states, matched
1.4155778e+08 transitions (= stored+matched)
        0 atomic steps
hash conflicts:   2689107 (resolved)
the hash table is 3.52 % filled (hint: rerun with -w25 to reduce runtime)

Stats on memory usage (in Megabytes):
 5040.004	equivalent memory usage for states (stored*(State-vector + overhead))
 6656.008	shared memory used for hash table (-w29)
    0.391	total non-shared memory usage

 7242.470	total shared memory usage


pan: elapsed time 175 seconds
pan: rate 107626.06 states/second
pan: releasing shared memory...done

real	2m56.344s
user	2m51.407s
sys	0m4.920s
