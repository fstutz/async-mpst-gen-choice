> promela file    : ../tree_broadcast/spin_code/tree_broadcast_5.pml
> date            : 01-Feb-2021 10:49:57
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_5.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m100000000 -c1 -w29 -u1 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 1 core

(Spin Version 6.5.1 -- 31 July 2020)
	+ Multi-Core (using 1 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 92 byte, depth reached 23, errors: 0
      101 states, stored
	     101 nominal states (stored-atomic)
      145 states, matched
      246 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)
the hash table is 1.88e-05 % filled

Stats on memory usage (in Megabytes):
    0.011	equivalent memory usage for states (stored*(State-vector + overhead))
 6656.003	shared memory used for hash table (-w29)
    0.391	total non-shared memory usage

 6656.092	total shared memory usage


pan: elapsed time 0 seconds
pan: releasing shared memory...done

real	0m0.010s
user	0m0.000s
sys	0m0.004s
