> promela file    : ../tree_broadcast/spin_code/tree_broadcast_10.pml
> date            : 01-Feb-2021 10:41:47
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_10.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m100000000 -c1 -w23 -u1 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 1 core

(Spin Version 6.5.1 -- 31 July 2020)
	+ Multi-Core (using 1 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 172 byte, depth reached 81, errors: 0
    49162 states, stored
	   49162 nominal states (stored-atomic)
   196609 states, matched
   245771 transitions (= stored+matched)
        0 atomic steps
hash conflicts:       604 (resolved)
the hash table is 0.586 % filled

Stats on memory usage (in Megabytes):
    9.002	equivalent memory usage for states (stored*(State-vector + overhead))
  104.005	shared memory used for hash table (-w23)
    0.391	total non-shared memory usage

  105.728	total shared memory usage


pan: elapsed time 0.27 seconds
pan: rate 182081.48 states/second
pan: releasing shared memory...done

real	0m0.291s
user	0m0.207s
sys	0m0.078s
