> promela file    : ../tree_broadcast/spin_code/tree_broadcast_16.pml
> date            : 29-Jan-2021 14:22:36
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_16.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DDFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -u30 -w19 -n
Depth=  899451 States=    1e+06 Transitions= 3.45e+06 Memory=   729.464	t=     4.45 R=   2e+05
Depth= 1758315 States=    2e+06 Transitions= 7.39e+06 Memory=   920.089	t=       10 R=   2e+05
pan: resizing hashtable to -w21..  done
Depth= 2602442 States=    3e+06 Transitions= 1.15e+07 Memory=  1126.714	t=     15.8 R=   2e+05
Depth= 3436700 States=    4e+06 Transitions= 1.58e+07 Memory=  1317.339	t=     21.7 R=   2e+05
Depth= 4253116 States=    5e+06 Transitions= 2.02e+07 Memory=  1507.964	t=       28 R=   2e+05
pan: resizing hashtable to -w23..  done
Depth= 5068345 States=    6e+06 Transitions= 2.47e+07 Memory=  1761.027	t=     34.5 R=   2e+05
Depth= 5873996 States=    7e+06 Transitions= 2.93e+07 Memory=  1951.652	t=     40.6 R=   2e+05
Depth= 6665303 States=    8e+06 Transitions= 3.39e+07 Memory=  2142.277	t=     46.9 R=   2e+05
Depth= 7453224 States=    9e+06 Transitions= 3.87e+07 Memory=  2333.292	t=     53.3 R=   2e+05
Depth= 8232652 States=    1e+07 Transitions= 4.34e+07 Memory=  2523.917	t=     59.9 R=   2e+05
Depth= 9006368 States=  1.1e+07 Transitions= 4.83e+07 Memory=  2714.542	t=     66.5 R=   2e+05
Depth= 9777826 States=  1.2e+07 Transitions= 5.31e+07 Memory=  2905.167	t=     73.3 R=   2e+05
error: max search depth too small
Depth= 9999999 States=  1.3e+07 Transitions= 6.05e+07 Memory=  3096.183	t=     81.3 R=   2e+05
Depth= 9999999 States=  1.4e+07 Transitions= 6.83e+07 Memory=  3286.808	t=       90 R=   2e+05
Depth= 9999999 States=  1.5e+07 Transitions= 7.67e+07 Memory=  3477.433	t=     99.2 R=   2e+05
Depth= 9999999 States=  1.6e+07 Transitions= 8.45e+07 Memory=  3668.058	t=      108 R=   1e+05
Depth= 9999999 States=  1.7e+07 Transitions= 9.25e+07 Memory=  3859.073	t=      117 R=   1e+05
pan: resizing hashtable to -w25..  done
Depth= 9999999 States=  1.8e+07 Transitions=    1e+08 Memory=  4297.886	t=      128 R=   1e+05
Depth= 9999999 States=  1.9e+07 Transitions= 1.09e+08 Memory=  4488.902	t=      138 R=   1e+05
Depth= 9999999 States=    2e+07 Transitions= 1.17e+08 Memory=  4679.527	t=      146 R=   1e+05
Depth= 9999999 States=  2.1e+07 Transitions= 1.25e+08 Memory=  4870.152	t=      156 R=   1e+05
Depth= 9999999 States=  2.2e+07 Transitions= 1.33e+08 Memory=  5060.777	t=      165 R=   1e+05
Depth= 9999999 States=  2.3e+07 Transitions= 1.42e+08 Memory=  5251.792	t=      175 R=   1e+05
Depth= 9999999 States=  2.4e+07 Transitions=  1.5e+08 Memory=  5442.417	t=      184 R=   1e+05
Depth= 9999999 States=  2.5e+07 Transitions= 1.59e+08 Memory=  5633.042	t=      194 R=   1e+05
Depth= 9999999 States=  2.6e+07 Transitions= 1.67e+08 Memory=  5824.058	t=      204 R=   1e+05
Depth= 9999999 States=  2.7e+07 Transitions= 1.75e+08 Memory=  6014.683	t=      213 R=   1e+05
Depth= 9999999 States=  2.8e+07 Transitions= 1.83e+08 Memory=  6205.308	t=      223 R=   1e+05
Depth= 9999999 States=  2.9e+07 Transitions= 1.91e+08 Memory=  6395.933	t=      232 R=   1e+05
Depth= 9999999 States=    3e+07 Transitions= 1.99e+08 Memory=  6586.948	t=      241 R=   1e+05
Depth= 9999999 States=  3.1e+07 Transitions= 2.08e+08 Memory=  6777.573	t=      251 R=   1e+05
Depth= 9999999 States=  3.2e+07 Transitions= 2.16e+08 Memory=  6968.198	t=      261 R=   1e+05
Depth= 9999999 States=  3.3e+07 Transitions= 2.24e+08 Memory=  7159.214	t=      271 R=   1e+05
