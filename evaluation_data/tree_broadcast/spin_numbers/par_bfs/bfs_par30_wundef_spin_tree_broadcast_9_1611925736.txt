> promela file    : ../tree_broadcast/spin_code/tree_broadcast_9.pml
> date            : 29-Jan-2021 14:08:56
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_9.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS_PAR -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -u30 -n
cpu00: largest shared memory segment: 8192 MB
pan: using 30 cores

(Spin Version 6.5.1 -- 31 July 2020)
	+ Multi-Core (using 30 cores)
	+ Breadth-First Search
	+ Partial Order Reduction

Hash-Compact 4 search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 156 byte, depth reached 70, errors: 0
    12297 states, stored
	   12297 nominal states (stored-atomic)
    43009 states, matched
    55306 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         2 (resolved)
the hash table is 0.0733 % filled

Stats on memory usage (in Megabytes):
    2.064	equivalent memory usage for states (stored*(State-vector + overhead))
  208.005	shared memory used for hash table (-w24)
   11.719	other (proc and chan stacks)
   11.719	total non-shared memory usage

 1539.551	total shared memory usage


pan: elapsed time 0.04 seconds
pan: rate    307425 states/second
pan: releasing shared memory...done

real	0m1.056s
user	0m0.023s
sys	0m0.027s
