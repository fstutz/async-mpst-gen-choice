> promela file    : ../tree_broadcast/spin_code/tree_broadcast_14.pml
> date            : 28-Jan-2021 00:05:29
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_14.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n
Depth=  785705 States=    1e+06 Transitions= 3.76e+06 Memory=   706.417	t=     4.23 R=   2e+05
Depth= 1467173 States=    2e+06 Transitions= 8.31e+06 Memory=   874.386	t=     9.84 R=   2e+05
pan: resizing hashtable to -w21..  done
Depth= 2067560 States=    3e+06 Transitions= 1.33e+07 Memory=  1057.964	t=     15.8 R=   2e+05
Depth= 2588663 States=    4e+06 Transitions= 1.86e+07 Memory=  1225.933	t=     22.1 R=   2e+05
Depth= 3030324 States=    5e+06 Transitions= 2.44e+07 Memory=  1393.511	t=     28.9 R=   2e+05
pan: resizing hashtable to -w23..  done
Depth= 3331836 States=    6e+06 Transitions= 3.06e+07 Memory=  1623.527	t=     36.5 R=   2e+05

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 244 byte, depth reached 3358998, errors: 0
  6291470 states, stored
 37748737 states, matched
 44040207 transitions (= stored+matched)
        0 atomic steps
hash conflicts:  23339411 (resolved)

Stats on memory usage (in Megabytes):
 1632.004	equivalent memory usage for states (stored*(State-vector + overhead))
 1075.027	actual memory usage for states (compression: 65.87%)
         	state-vector as stored = 151 byte + 28 byte overhead
   64.000	memory used for hash table (-w23)
  534.058	memory used for DFS stack (-m10000000)
 1672.355	total actual memory usage



pan: elapsed time 52.3 seconds
pan: rate 120203.86 states/second

real	0m52.640s
user	0m51.584s
sys	0m1.046s
