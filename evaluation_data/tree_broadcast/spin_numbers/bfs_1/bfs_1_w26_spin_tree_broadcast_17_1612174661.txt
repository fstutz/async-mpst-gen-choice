> promela file    : ../tree_broadcast/spin_code/tree_broadcast_17.pml
> date            : 01-Feb-2021 11:17:41
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_17.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w26 -n
Depth=      10 States=       11 Transitions=       11 Memory=   512.781	
Depth=      20 States=       25 Transitions=       26 Memory=   512.781	
Depth=      30 States= 5.64e+03 Transitions= 2.04e+04 Memory=   514.734	t=     0.03 R=   2e+05
Depth=      40 States= 2.28e+05 Transitions= 1.29e+06 Memory=   587.781	t=      1.6 R=   1e+05
Depth=      45 States=    1e+06 Transitions= 6.31e+06 Memory=   832.312	t=     7.94 R=   1e+05
Depth=      48 States=    2e+06 Transitions= 1.33e+07 Memory=  1131.141	t=     16.8 R=   1e+05
Depth=      50 States= 2.58e+06 Transitions= 1.76e+07 Memory=  1295.594	t=     22.2 R=   1e+05
Depth=      51 States= 3.11e+06 Transitions= 2.15e+07 Memory=  1445.984	t=     27.2 R=   1e+05
Depth=      52 States=    4e+06 Transitions=  2.8e+07 Memory=  1699.891	t=     35.5 R=   1e+05
Depth=      53 States=    5e+06 Transitions= 3.57e+07 Memory=  1972.938	t=     45.2 R=   1e+05
Depth=      55 States=    6e+06 Transitions= 4.34e+07 Memory=  2243.250	t=       55 R=   1e+05
Depth=      56 States=    7e+06 Transitions= 5.11e+07 Memory=  2513.172	t=     64.8 R=   1e+05
Depth=      57 States=    8e+06 Transitions= 5.89e+07 Memory=  2778.406	t=     74.7 R=   1e+05
Depth=      58 States=    9e+06 Transitions= 6.68e+07 Memory=  3041.688	t=     84.8 R=   1e+05
Depth=      59 States=    1e+07 Transitions= 7.49e+07 Memory=  3300.672	t=       95 R=   1e+05
Depth=      59 States=  1.1e+07 Transitions= 8.28e+07 Memory=  3562.781	t=      105 R=   1e+05
Depth=      60 States= 1.12e+07 Transitions= 8.44e+07 Memory=  3613.172	t=      107 R=   1e+05
Depth=      61 States= 1.25e+07 Transitions= 9.47e+07 Memory=  3949.500	t=      120 R=   1e+05
Depth=      61 States=  1.3e+07 Transitions= 9.86e+07 Memory=  4084.266	t=      125 R=   1e+05
Depth=      62 States=  1.4e+07 Transitions= 1.07e+08 Memory=  4345.984	t=      136 R=   1e+05
Depth=      62 States=  1.5e+07 Transitions= 1.15e+08 Memory=  4606.531	t=      146 R=   1e+05
Depth=      63 States=  1.6e+07 Transitions= 1.23e+08 Memory=  4867.859	t=      156 R=   1e+05
Depth=      64 States=  1.7e+07 Transitions= 1.31e+08 Memory=  5128.406	t=      166 R=   1e+05
Depth=      64 States=  1.8e+07 Transitions= 1.39e+08 Memory=  5391.688	t=      177 R=   1e+05
Depth=      65 States=  1.9e+07 Transitions= 1.47e+08 Memory=  5654.188	t=      187 R=   1e+05
Depth=      65 States=    2e+07 Transitions= 1.55e+08 Memory=  5918.250	t=      197 R=   1e+05
Depth=      66 States=  2.1e+07 Transitions= 1.63e+08 Memory=  6180.750	t=      207 R=   1e+05
Depth=      66 States=  2.2e+07 Transitions= 1.71e+08 Memory=  6445.594	t=      218 R=   1e+05
Depth=      67 States=  2.3e+07 Transitions= 1.79e+08 Memory=  6707.703	t=      228 R=   1e+05
Depth=      67 States=  2.4e+07 Transitions= 1.87e+08 Memory=  6971.766	t=      239 R=   1e+05
Depth=      68 States=  2.5e+07 Transitions= 1.95e+08 Memory=  7233.094	t=      249 R=   1e+05
Depth=      68 States=  2.6e+07 Transitions= 2.03e+08 Memory=  7496.375	t=      260 R=   1e+05
Depth=      69 States=  2.7e+07 Transitions= 2.12e+08 Memory=  7754.969	t=      270 R=   1e+05
Depth=      69 States=  2.8e+07 Transitions= 2.19e+08 Memory=  8022.547	t=      280 R=   1e+05
pan: reached -DMEMLIM bound
	8.58961e+09 bytes used
	409600 bytes more needed
	8.58993e+09 bytes limit
hint: to reduce memory, recompile with
  -DCOLLAPSE # good, fast compression, or
  -DMA=292   # better/slower compression, or
  -DHC # hash-compaction, approximation
  -DBITSTATE # supertrace, approximation

(Spin Version 6.5.1 -- 31 July 2020)
Warning: Search not completed
	+ Breadth-First Search
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 292 byte, depth reached 69, errors: 0
 28662086 states, stored
	2.86621e+07 nominal states (stored-atomic)
1.9648442e+08 states, matched
2.2514651e+08 transitions (= stored+matched)
        0 atomic steps
hash conflicts:  21128299 (resolved)

Stats on memory usage (in Megabytes):
 8746.974	equivalent memory usage for states (stored*(State-vector + overhead))
 7682.057	actual memory usage for states (compression: 87.83%)
         	state-vector as stored = 253 byte + 28 byte overhead
  512.000	memory used for hash table (-w26)
    2.369	memory lost to fragmentation
 8191.688	total actual memory usage



pan: elapsed time 288 seconds
pan: rate 99576.452 states/second

real	4m48.259s
user	4m42.317s
sys	0m5.926s
