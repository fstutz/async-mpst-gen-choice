> promela file    : ../tree_broadcast/spin_code/tree_broadcast_8.pml
> date            : 01-Feb-2021 11:09:06
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_8.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w26 -n
Depth=      10 States=       12 Transitions=       12 Memory=   512.781	
Depth=      20 States=      441 Transitions= 1.26e+03 Memory=   512.781	
Depth=      30 States= 2.32e+03 Transitions= 8.52e+03 Memory=   512.781	
Depth=      40 States= 3.98e+03 Transitions= 1.57e+04 Memory=   513.172	
Depth=      50 States= 4.54e+03 Transitions= 1.81e+04 Memory=   513.172	

(Spin Version 6.5.1 -- 31 July 2020)
	+ Breadth-First Search
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 140 byte, depth reached 59, errors: 0
     4616 states, stored
	    4616 nominal states (stored-atomic)
    13825 states, matched
    18441 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.740	equivalent memory usage for states (stored*(State-vector + overhead))
    1.554	actual memory usage for states
  512.000	memory used for hash table (-w26)
  513.172	total actual memory usage



pan: elapsed time 0.01 seconds

real	0m0.256s
user	0m0.053s
sys	0m0.198s
