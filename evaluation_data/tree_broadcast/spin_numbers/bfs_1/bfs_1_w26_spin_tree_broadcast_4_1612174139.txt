> promela file    : ../tree_broadcast/spin_code/tree_broadcast_4.pml
> date            : 01-Feb-2021 11:08:59
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_4.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w26 -n
Depth=      10 States=       23 Transitions=       35 Memory=   512.781	

(Spin Version 6.5.1 -- 31 July 2020)
	+ Breadth-First Search
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 76 byte, depth reached 16, errors: 0
       36 states, stored
	      36 nominal states (stored-atomic)
       33 states, matched
       69 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.004	equivalent memory usage for states (stored*(State-vector + overhead))
    1.168	actual memory usage for states
  512.000	memory used for hash table (-w26)
  512.781	total actual memory usage



pan: elapsed time 0 seconds

real	0m0.241s
user	0m0.048s
sys	0m0.188s
