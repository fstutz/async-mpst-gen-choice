> promela file    : ../tree_broadcast/spin_code/tree_broadcast_16.pml
> date            : 01-Feb-2021 11:12:54
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_16.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w26 -n
Depth=      10 States=       11 Transitions=       11 Memory=   512.781	
Depth=      20 States=       33 Transitions=       38 Memory=   512.781	
Depth=      30 States= 7.43e+03 Transitions= 2.82e+04 Memory=   515.125	t=     0.04 R=   2e+05
Depth=      40 States= 2.16e+05 Transitions=  1.2e+06 Memory=   580.359	t=     1.38 R=   2e+05
Depth=      46 States=    1e+06 Transitions= 6.26e+06 Memory=   810.438	t=     7.26 R=   1e+05
Depth=      50 States= 1.92e+06 Transitions= 1.27e+07 Memory=  1063.562	t=     14.8 R=   1e+05
Depth=      51 States= 2.27e+06 Transitions= 1.52e+07 Memory=  1156.531	t=     17.7 R=   1e+05
Depth=      52 States=    3e+06 Transitions= 2.05e+07 Memory=  1351.062	t=     23.9 R=   1e+05
Depth=      54 States=    4e+06 Transitions= 2.79e+07 Memory=  1610.828	t=     32.5 R=   1e+05
Depth=      56 States=    5e+06 Transitions= 3.53e+07 Memory=  1868.641	t=     41.2 R=   1e+05
Depth=      58 States=    6e+06 Transitions= 4.28e+07 Memory=  2123.328	t=     50.1 R=   1e+05
Depth=      59 States=    7e+06 Transitions= 5.03e+07 Memory=  2377.234	t=       59 R=   1e+05
Depth=      60 States=  7.3e+06 Transitions= 5.27e+07 Memory=  2450.281	t=     61.8 R=   1e+05
Depth=      61 States= 8.09e+06 Transitions= 5.87e+07 Memory=  2651.062	t=     68.8 R=   1e+05
Depth=      62 States=    9e+06 Transitions= 6.56e+07 Memory=  2880.750	t=     76.9 R=   1e+05
Depth=      63 States=    1e+07 Transitions= 7.32e+07 Memory=  3133.875	t=     85.8 R=   1e+05
Depth=      64 States=  1.1e+07 Transitions= 8.08e+07 Memory=  3385.828	t=     94.9 R=   1e+05
Depth=      65 States=  1.2e+07 Transitions= 8.86e+07 Memory=  3635.047	t=      104 R=   1e+05
Depth=      66 States=  1.3e+07 Transitions= 9.64e+07 Memory=  3882.312	t=      113 R=   1e+05
Depth=      66 States=  1.4e+07 Transitions= 1.04e+08 Memory=  4130.750	t=      123 R=   1e+05
Depth=      67 States=  1.5e+07 Transitions= 1.12e+08 Memory=  4379.578	t=      132 R=   1e+05
Depth=      68 States=  1.6e+07 Transitions=  1.2e+08 Memory=  4626.453	t=      141 R=   1e+05
Depth=      69 States=  1.7e+07 Transitions= 1.28e+08 Memory=  4868.250	t=      150 R=   1e+05
Depth=      70 States= 1.78e+07 Transitions= 1.34e+08 Memory=  5063.172	t=      158 R=   1e+05
Depth=      71 States= 1.92e+07 Transitions= 1.45e+08 Memory=  5387.000	t=      171 R=   1e+05
Depth=      71 States=    2e+07 Transitions= 1.51e+08 Memory=  5591.297	t=      179 R=   1e+05
Depth=      72 States=  2.1e+07 Transitions= 1.59e+08 Memory=  5828.797	t=      188 R=   1e+05
Depth=      73 States=  2.2e+07 Transitions= 1.68e+08 Memory=  6061.219	t=      198 R=   1e+05
Depth=      73 States=  2.3e+07 Transitions= 1.75e+08 Memory=  6299.500	t=      207 R=   1e+05
Depth=      74 States=  2.4e+07 Transitions= 1.83e+08 Memory=  6533.875	t=      217 R=   1e+05
Depth=      75 States=  2.5e+07 Transitions= 1.92e+08 Memory=  6765.125	t=      226 R=   1e+05
Depth=      75 States=  2.6e+07 Transitions=    2e+08 Memory=  6999.500	t=      236 R=   1e+05
Depth=      76 States=  2.7e+07 Transitions= 2.08e+08 Memory=  7230.750	t=      246 R=   1e+05
Depth=      77 States=  2.8e+07 Transitions= 2.16e+08 Memory=  7461.219	t=      255 R=   1e+05
Depth=      77 States=  2.9e+07 Transitions= 2.24e+08 Memory=  7692.469	t=      265 R=   1e+05
Depth=      78 States=    3e+07 Transitions= 2.32e+08 Memory=  7921.375	t=      275 R=   1e+05
Depth=      79 States=  3.1e+07 Transitions=  2.4e+08 Memory=  8151.062	t=      285 R=   1e+05
pan: reached -DMEMLIM bound
	8.58961e+09 bytes used
	409600 bytes more needed
	8.58993e+09 bytes limit
hint: to reduce memory, recompile with
  -DCOLLAPSE # good, fast compression, or
  -DMA=276   # better/slower compression, or
  -DHC # hash-compaction, approximation
  -DBITSTATE # supertrace, approximation

(Spin Version 6.5.1 -- 31 July 2020)
Warning: Search not completed
	+ Breadth-First Search
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 276 byte, depth reached 79, errors: 0
 31177522 states, stored
	3.11775e+07 nominal states (stored-atomic)
2.1046556e+08 states, matched
2.4164308e+08 transitions (= stored+matched)
        0 atomic steps
hash conflicts:  25391745 (resolved)

Stats on memory usage (in Megabytes):
 9038.893	equivalent memory usage for states (stored*(State-vector + overhead))
 7682.413	actual memory usage for states (compression: 84.99%)
         	state-vector as stored = 230 byte + 28 byte overhead
  512.000	memory used for hash table (-w26)
    2.725	memory lost to fragmentation
 8191.688	total actual memory usage



pan: elapsed time 286 seconds
pan: rate 108852.46 states/second

real	4m46.844s
user	4m40.685s
sys	0m6.144s
