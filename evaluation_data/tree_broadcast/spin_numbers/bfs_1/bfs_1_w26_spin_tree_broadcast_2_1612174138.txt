> promela file    : ../tree_broadcast/spin_code/tree_broadcast_2.pml
> date            : 01-Feb-2021 11:08:58
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_2.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DBFS -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w26 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Breadth-First Search
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 36 byte, depth reached 3, errors: 0
        4 states, stored
	       4 nominal states (stored-atomic)
        1 states, matched
        5 transitions (= stored+matched)
        0 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.000	equivalent memory usage for states (stored*(State-vector + overhead))
    1.170	actual memory usage for states
  512.000	memory used for hash table (-w26)
  512.781	total actual memory usage



pan: elapsed time 0 seconds

real	0m0.244s
user	0m0.028s
sys	0m0.210s
