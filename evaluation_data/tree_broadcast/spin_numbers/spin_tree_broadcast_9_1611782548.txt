> promela file    : ../tree_broadcast/spin_code/tree_broadcast_9.pml
> date            : 27-Jan-2021 22:22:28
> spin version    : Spin Version 6.5.1 -- 31 July 2020
> gcc version     : 8
> runspin command : runspin ../tree_broadcast/spin_code/tree_broadcast_9.pml
> config source   : default commands for a safety verification run
> spin command    : spin -a -b
> gcc command     : gcc -o pan -DVECTORSZ=4096 -DSAFETY -DMEMLIM=8192 pan.c
> pan command     : ./pan -m10000000 -c1 -w19 -n

(Spin Version 6.5.1 -- 31 July 2020)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	cycle checks       	- (disabled by -DSAFETY)
	invalid end states	+

State-vector 156 byte, depth reached 5913, errors: 0
    12297 states, stored
    43009 states, matched
    55306 transitions (= stored+matched)
        0 atomic steps
hash conflicts:       361 (resolved)

Stats on memory usage (in Megabytes):
    2.158	equivalent memory usage for states (stored*(State-vector + overhead))
    2.335	actual memory usage for states
    4.000	memory used for hash table (-w19)
  534.058	memory used for DFS stack (-m10000000)
  540.011	total actual memory usage



pan: elapsed time 0.04 seconds
pan: rate    307425 states/second

real	0m0.312s
user	0m0.097s
sys	0m0.210s
