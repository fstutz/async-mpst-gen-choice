import resource
import sys


# increases recursion depth and RAM
def increase_rec_depth_and_RAM(ram, rec_limit):
    resource.setrlimit(resource.RLIMIT_STACK, [ram, -1])
    # increase recursion limit 10times compared to default
    sys.setrecursionlimit(rec_limit)
