DEFAULT_NUM_RUNS = 10
MAX_NUM_RUNS = 1000

DEFAULT_MAX_SIZE_OVERHEAD = 250
DEFAULT_MAX_SIZE_STATE_EXP = 175
TIMEOUT_SCALE = 1000*60*60      # 60 minutes

PREFIX_TYPES = "global_types/projectability/"
PREFIX_PARAMETRIC_TYPES = "global_types/parametric/"
PREFIX_EVAL_CLASSICAL = "evaluation_data/projectability_classical/"
PREFIX_EVAL_SUBSET = "evaluation_data/projectability_subset/"
PREFIX_EVAL_STATE_EXP = "evaluation_data/state_explosion/"
PREFIX_EVAL_OVERHEAD = "evaluation_data/overhead/"

EXAMPLE_NAME_FILENAME_MAP = {
    "Instr. Contr. Prot. A": "instrument_control_protocol_A",
    "Instr. Contr. Prot. B": "instrument_control_protocol_B",
    "Multi Party Game": "multi_party_game",
    "OAuth2": "OAuth2",
    "Streaming": "streaming",
    "Non-Compatible Merge": "non_compatible",
    "Spring-Hibernate": "spring_hibernate",
    "Group Present": "group_present",
    "Late Learning": "late_learning",
    "Load Balancer (n=10)": "load_balancer_10",
    "Logging (n=10)": "logging_10",
    "2 Buyer Prot.": "two_buyer_protocol",
    "2B-Prot. - Omit No": "two_buyer_protocol_omit_no",
    "2B-Prot. - Subscription": "two_buyer_protocol_subscription",
    "2B-Prot. - Inner Rec.": "two_buyer_protocol_inner_recursion",
    "Odd-Even": "odd_even",
    "Motivation RCV Violated (Gr)": "motivation_rcv_validity_negative",
    "Motivation RCV Satisfied (Gr')": "motivation_rcv_validity_positive",
    "Motivation SND Violated (Gs)": "motivation_snd_validity_negative",
    "Motivation SND Satisfied (Gs')": "motivation_snd_validity_positive",
    "Example Folded (Gfold)": "example_folded",
    "Example Unfolded (Gunf)": "example_unfolded",
}

MAP_EXAMPLE_NAME_GROUND_TRUTH = {
    "Instr. Contr. Prot. A": "yes",
    "Instr. Contr. Prot. B": "yes",
    "Multi Party Game": "yes",
    "OAuth2": "yes",
    "Streaming": "yes",
    "Non-Compatible Merge": "yes",
    "Spring-Hibernate": "yes",
    "Group Present": "yes",
    "Late Learning": "yes",
    "Load Balancer (n=10)": "yes",
    "Logging (n=10)": "yes",
    "2 Buyer Prot.": "yes",
    "2B-Prot. - Omit No": "yes",
    "2B-Prot. - Subscription": "yes",
    "2B-Prot. - Inner Rec.": "yes",
    "Odd-Even": "yes",
    "Motivation RCV Violated (Gr)": "no",
    "Motivation RCV Satisfied (Gr')": "yes",
    "Motivation SND Violated (Gs)": "no",
    "Motivation SND Satisfied (Gs')": "yes",
    "Example Folded (Gfold)": "yes",
    "Example Unfolded (Gunf)": "yes"
}