# run_evaluation_overhead_avail.py

import os
from evaluation_functionality.helper_functions import increase_rec_depth_and_RAM
from global_types.parametric.ParametricGlobalTypes import *
from evaluation_functionality.data_processing.processing_projection_data import *
from evaluation_functionality.data_processing.plotting_overhead import *

from evaluation_functionality.evaluation_config import PREFIX_EVAL_OVERHEAD, PREFIX_PARAMETRIC_TYPES, TIMEOUT_SCALE

EXAMPLES_OVERHEAD_LIST = {
    ParametricKinds.MAP_REDUCE,
    ParametricKinds.MEM_CACHE,
    ParametricKinds.P2P_BROADCAST,
    ParametricKinds.TREE_BROADCAST
}

NUM_KEYS = 2
NUM_PARAM = 250
NUM_RUNS = 1

def run_overhead_availability_analysis(num_size, num_runs=NUM_RUNS):
    all_eval_folder_path = PREFIX_EVAL_OVERHEAD + "evaluation_" + \
                           str(num_runs) + "x_<" + \
                           str(num_size) + "_" + \
                           str(trunc(time.time())) + "/"
    os.mkdir(all_eval_folder_path)
    # 1) compute the data for all projections
    for example in EXAMPLES_OVERHEAD_LIST:
        snd = NUM_KEYS if ParametricKinds.has_snd(example) else None
        # a) write global type representations up to NUM_SIZE
        write_global_types_up_to_size(example, num_size, snd)
        # b) specific eval folder
        spec_eval_folder_path = all_eval_folder_path + example.value + "/"
        os.mkdir(spec_eval_folder_path)
        spec_optavail_eval_folder_path = spec_eval_folder_path + "optional_avail/"
        os.mkdir(spec_optavail_eval_folder_path)
        spec_noavail_eval_folder_path = spec_eval_folder_path + "no_avail/"
        os.mkdir(spec_noavail_eval_folder_path)
        # b) project types up to size, once with checking availability and once without
        os.environ['AVAIL_CHECK'] = "TRUE"
        project_up_to_size(example, spec_optavail_eval_folder_path,
                           num_runs, num_size, snd)
        os.environ['AVAIL_CHECK'] = "FALSE"
        project_up_to_size(example, spec_noavail_eval_folder_path,
                           num_runs, num_size, snd)
        os.environ['AVAIL_CHECK'] = "TRUE"
        # c) create mappings
        write_param_file_to_file(spec_optavail_eval_folder_path, example)
        write_param_file_to_file(spec_noavail_eval_folder_path, example)
        # d) compute averages
        write_mst_averages_to_file(spec_optavail_eval_folder_path, example)
        write_mst_averages_to_file(spec_noavail_eval_folder_path, example)
    # reset the availability check to true
    return all_eval_folder_path


def write_global_types_up_to_size(whichone, size, snd=None):
    increase_rec_depth_and_RAM(ram=1000*1024*1024, rec_limit=10000)
    ParametricGlobalTypes.write_representation_up_to_size_w_kind(whichone, size, snd)


def project_up_to_size(whichone, eval_path, num_of_runs, num_size, snd=None):
    increase_rec_depth_and_RAM(ram=1000*1024*1024, rec_limit=10000)
    for i in range(num_of_runs):
        wrapper_projections_up_to_size(whichone, eval_path, num_size, snd)


def wrapper_projections_up_to_size(kind, eval_path, max_size, snd=None):
    # asserts that global types have been produced before
    res_file_prefix = eval_path
    filename_prefix = PREFIX_PARAMETRIC_TYPES + kind.value + "/" + kind.value + "_"
    start = 1
    if kind == ParametricKinds.P2P_BROADCAST or kind == ParametricKinds.TREE_BROADCAST:
        start = 2
    if snd is not None:
        res_file_name = res_file_prefix + 'evaluation_max_' + str(max_size) + '_' + str(snd) + '_' + \
                        str(trunc(time.time())) + '.txt'
        filename_prefix += "k" + str(snd) + "_"
    else:
        res_file_name = res_file_prefix + 'evaluation_max' + str(max_size) + '_' + \
                        str(trunc(time.time())) + '.txt'
    text_file = open(res_file_name, "w+")
    res = '#Size\t#Para\t Time\t\t Diff\t\t Proc\t\t Diff\n'
    text_file.write(res)
    last_time_overall = 0
    last_time_proc = 0.0
    i = start
    filename = filename_prefix + str(i) + ".gt"
    tplv_type = get_tplv_gt_from_file(filename)
    while tplv_type.get_size() <= max_size:
        filename = filename_prefix + str(i) + ".gt"
        tplv_type = get_tplv_gt_from_file(filename)
        start = time.time()
        for proc in tplv_type.get_procs():
            proj = tplv_type.project_onto(proc)
            assert proj is not None
        end = time.time()
        this_time_overall = 1000 * (end-start)
        this_time_proc = this_time_overall / float(len(tplv_type.get_procs()))
        diff_time_overall = this_time_overall - last_time_overall
        diff_time_proc = this_time_proc - last_time_proc
        res = str(tplv_type.get_size()) + '\t\t' + str(i) + \
              '\t' + get_unified_repr_floats(this_time_overall) + \
              '\t' + get_unified_repr_floats(diff_time_overall) + \
              '\t' + get_unified_repr_floats(this_time_proc) + \
              '\t' + get_unified_repr_floats(diff_time_proc) + '\n'
        text_file.write(res)
        text_file.flush()
        # break for timeout
        # TODO: set timeout somewhere else
        if this_time_overall >= TIMEOUT_SCALE:
            break
        last_time_overall = this_time_overall
        last_time_proc = this_time_proc
        i += 1
    text_file.close()


def out_test_write_spin_repr_up_to():
    increase_rec_depth_and_RAM(ram=1000*1024*1024, rec_limit=10000)
    ParametricGlobalTypes.write_spin_repr(ParametricKinds.LOAD_BALANCER)
    ParametricGlobalTypes.write_spin_repr(ParametricKinds.LOGGING)
    ParametricGlobalTypes.write_spin_repr(ParametricKinds.MEM_CACHE)
    ParametricGlobalTypes.write_spin_repr(ParametricKinds.P2P_BROADCAST)
    ParametricGlobalTypes.write_spin_repr(ParametricKinds.TREE_BROADCAST)
    ParametricGlobalTypes.write_spin_repr(ParametricKinds.MAP_REDUCE)
