#!/bin/bash

# Default values
NUM_RUNS=10

# Variable to store if all python programs went fine
ALL_SUCCESSFUL=true

# get prefix for output if provided
PREFIX="evaluation_results/"

# define the results
FILE_SUBSET=${PREFIX}"table_projection_subset.txt"
FILE_CLASSICAL=${PREFIX}"table_projection_classical.txt"
FILE_STATE_EXP=${PREFIX}"plot_state_space_explosion.pdf"

echo "1) Computing results for subset projection."

# Call for subset and check if file should be overwritten
python3 main.py --option projectability_subset --num_runs ${NUM_RUNS} --save_to ${FILE_SUBSET} --overwrite --no_output

# Check if last was successful
if [ $? != 0 ]; then
  ALL_SUCCESSFUL=false
fi

echo "2) Computing results for classical projection."

# Call for subset and check if file should be overwritten
python3 main.py --option projectability_classical --num_runs ${NUM_RUNS} --save_to ${FILE_CLASSICAL} --overwrite --no_output

# Check if last was successful
if [ $? != 0 ]; then
  ALL_SUCCESSFUL=false
fi

echo "3) Computing results for state space explosion analysis."

# Call for subset and check if file should be overwritten
python3 main.py --option state_explosion --save_to ${FILE_STATE_EXP} --overwrite --no_output

# Check if last was successful
if [ $? != 0 ]; then
  ALL_SUCCESSFUL=false
fi

if [ ${ALL_SUCCESSFUL} == true ]; then
  echo "Computing requested results was successful."
else
  echo "Some errors occurred during the computation."
fi
