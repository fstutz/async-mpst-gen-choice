from global_types.parametric.ParametricGlobalTypes import ParametricKinds
import glob


# one function to get [param, time] from SPIN in file, parameter kind (directory and prefix)
def write_spin_averages_to_file(kind, start, upto_num):
    # mem or rec overwrites others (that do not exist for us) and assumes higher instances also fail for the same reason
    directory = "../evaluation_data/" + kind.value + "/spin_numbers/"
    prefix = "spin_" + kind.value + "_"
    if kind == ParametricKinds.MAP_REDUCE:
        prefix += "k2_"
    out_filename = directory + "a_summary_" + kind.value + ".txt"
    out_file = open(out_filename, "w+")
    res = ""
    indiv_res = "old"
    for i in range(start, upto_num+1):
        pattern = directory + prefix + str(i) + '_*.txt'
        list_of_index = glob.glob(pattern)
        if len(list_of_index) == 0:
            raise Exception("Why?")
        combined = 0
        for f in list_of_index:
            indiv_res = get_result_from_file(f)
            if indiv_res != "mem" and indiv_res != "rec" and indiv_res != "chan":
                combined += indiv_res
            else:
                break
        if combined != 0:
            res += str(i) + '\t' + str(combined/float(len(list_of_index)))
        else:
            if indiv_res == 'old':
                raise Exception("This should not occur!")
            # res += str(i) + '\t' + indiv_res
            break
        res += '\n'
    out_file.write(res)
    out_file.close()


def get_result_from_file(file):
    read_file = open(file, 'r')
    for line in read_file:
        if "real" in line:
            time_str = line[5:]
            time_split = time_str.split('m')
            time_ms = int(time_split[0]) * 60000.0 + float(time_split[1][:-2]) * 1000.0
            return time_ms
        elif "DMEMLIM bound" in line:
            return "mem"
        elif "error: max search depth" in line:
            return "rec"
        elif "too many channel types" in line:
            return "chan"
    raise Exception("SPIN file without time!")


def write_avg_spin_results():
    write_spin_averages_to_file(ParametricKinds.LOAD_BALANCER, 1, 200)
    write_spin_averages_to_file(ParametricKinds.LOGGING, 1, 200)
    write_spin_averages_to_file(ParametricKinds.MEM_CACHE, 1, 200)
    write_spin_averages_to_file(ParametricKinds.MAP_REDUCE, 1, 200)
    write_spin_averages_to_file(ParametricKinds.P2P_BROADCAST, 2, 200)
    write_spin_averages_to_file(ParametricKinds.TREE_BROADCAST, 2, 200)

