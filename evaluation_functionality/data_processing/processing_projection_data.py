from global_types.parametric.ParametricGlobalTypes import ParametricKinds
from parsing.InputParser import get_tplv_gt_from_file
import time
from math import trunc
import numpy as np
import glob


def write_avg_mst_results(folder_name):
    write_mst_averages_to_file(ParametricKinds.LOAD_BALANCER, folder_name)
    write_mst_averages_to_file(ParametricKinds.LOGGING, folder_name)
    write_mst_averages_to_file(ParametricKinds.MAP_REDUCE, folder_name)
    write_mst_averages_to_file(ParametricKinds.MEM_CACHE, folder_name)
    write_mst_averages_to_file(ParametricKinds.P2P_BROADCAST, folder_name)
    write_mst_averages_to_file(ParametricKinds.TREE_BROADCAST, folder_name)


def write_param_size(folder_name):
    write_param_file_to_file(ParametricKinds.LOAD_BALANCER, folder_name)
    write_param_file_to_file(ParametricKinds.LOGGING, folder_name)
    write_param_file_to_file(ParametricKinds.MAP_REDUCE, folder_name)
    write_param_file_to_file(ParametricKinds.MEM_CACHE, folder_name)
    write_param_file_to_file(ParametricKinds.P2P_BROADCAST, folder_name)
    write_param_file_to_file(ParametricKinds.TREE_BROADCAST, folder_name)


def write_mst_averages_to_file(directory, kind):
    prefix = "*evaluation"
    out_filename = directory + "a_summary_" + kind.value + ".txt"
    pattern = directory + prefix + "*.txt"
    files = glob.glob(pattern)
    data = [np.loadtxt(f, skiprows=1) for f in files]
    x = np.array([d[:, 2] for d in data])
    # use mask to exclude max and min
    # x_mask = np.logical_or(x == x.max(1, keepdims=1), x == x.min(1, keepdims=1))
    # x_masked = np.ma.masked_array(x, x_mask)
    # m = np.(x_masked, axis=0)
    m = np.mean(x, axis=0)
    snd = data[0][:, 1]     # use [0, 1] instead of 1 for size of type
    without_size = np.column_stack([snd, m])
    np.savetxt(out_filename, without_size)


def write_param_file_to_file(directory, kind):
    prefix = "*evaluation"
    out_filename = directory + "a_mapping_" + kind.value + ".txt"
    pattern = directory + prefix + "*.txt"
    files = glob.glob(pattern)
    file = files[0]
    data = np.loadtxt(file, skiprows=1)
    size = data[:, 0]
    param = data[:, 1]
    param_size = np.column_stack([param, size])
    np.savetxt(out_filename, param_size)


# helper function
def get_unified_repr_floats(number):
    return "%10.3f" % number

