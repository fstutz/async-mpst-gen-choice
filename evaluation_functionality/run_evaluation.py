import os
import time
from math import trunc
import linecache
from parsing.InputParser import get_gt_from_file
from evaluation_functionality.evaluation_config import EXAMPLE_NAME_FILENAME_MAP, MAP_EXAMPLE_NAME_GROUND_TRUTH, PREFIX_TYPES


# this function runs the evaluation_functionality and is the only function that is called from outside
def run_evaluation(experiment_class, num_runs):
    # a) create new folder for evaluation_data data
    eval_folder_path = experiment_class.get_eval_data_path_prefix() + "evaluation_" + str(num_runs) + "_" + str(trunc(time.time()))
    os.mkdir(eval_folder_path)
    # b) add header for table with results
    results = tableheader(num_runs)
    for example_name in EXAMPLE_NAME_FILENAME_MAP.keys():
        # c) compute the path names for the example
        filename_type = EXAMPLE_NAME_FILENAME_MAP[example_name]
        path_type = PREFIX_TYPES + filename_type + ".gt"
        path_eval = eval_folder_path + "/" + filename_type + ".txt"
        # d) run the evaluation_functionality for each example
        run_projection_of_and_save_to(experiment_class, num_runs, example_name, path_type, path_eval)
        # e) retrieve the data and add it to the table
        results += collect_and_add_data(num_runs, example_name, path_eval)
    results += "\nThese results were checked against the expected results for this method and matched.\n"
    return results

# d) run the evaluation_functionality for every global type as often as specified
def run_projection_of_and_save_to(experiment_class, num_runs, example_name, path_type, path_eval):
    text_file = open(path_eval, "w+")
    res = 'Time\n'
    sum_all = 0.0
    assert num_runs > 0 # guaranteed by check in main
    # We get the global type, construct the respective evaluation object and get the expected result
    gt = get_gt_from_file(path_type)
    eval_object = experiment_class(gt, example_name)
    expected_result = eval_object.get_expected_result()
    # We run the evaluation_functionality as often as specified
    for i in range(num_runs):
        projections = []
        start = time.time()
        result = True
        for proc in gt.get_procs():
            proj = eval_object.project_onto(proc)
            projections.append(proj)
            result = result and (proj is not None)
        assert result == expected_result
        end = time.time()
        this_time_overall = 1000 * (end-start)
        sum_all += this_time_overall
        res += "{:<7}".format("%0.3f" % this_time_overall) + "\n"
    # Calculate the size of all projections if they are defined and only for last one
    size_projs = 0
    if expected_result:
        for proj in projections:
            size_projs += proj.get_size()
    else:
        size_projs = -1
    num_procs = len(gt.get_procs())
    res += '\nSummary:\n'
    res += "{:<7}".format("Size") + \
           "{:<7}".format("Time") + \
           "{:<7}".format("|P|") + \
           "{:<14}".format("SizeProjs") + \
           "{:<10}".format("ExpRes") + \
           "\n"
    result_to_print = "yes" if expected_result else "no" # the expected_result is ensured to agree with the computed one
    average_time = sum_all / (float(num_runs))
    res += "{:<7}".format("%3.0f" % eval_object.get_size()) + \
           "{:<7}".format("%0.3f" % average_time) + \
            "{:<7}".format("%3.0f" % num_procs) + \
            "{:<14}".format("%3.0f" % size_projs) + \
            result_to_print + \
           '\n'
    text_file.write(res)
    text_file.close()

# e) retrieve the data and add it to the table
def collect_and_add_data(num_runs, example_name, path_eval):
    line_result = linecache.getline(path_eval, 5 + num_runs)
    split_result = line_result.split()
    size = int(split_result[0])
    runtime = float(split_result[1])
    numprocs = int(split_result[2])
    size_sum_projs = int(split_result[3])
    result = split_result[4]
    ground_truth = MAP_EXAMPLE_NAME_GROUND_TRUTH.get(example_name)
    return "{:<35}".format(example_name) + \
        "{:<10}".format(ground_truth) + \
        "{:<10}".format(str(result)) + \
        "{:<8}".format("%2.0f" % size) + \
        "{:<7}".format("%2.0f" % numprocs) + \
        "{:<14}".format("   %3.0f" % size_sum_projs) + \
        "{:<11}".format("%6.3f" % runtime) + \
        "\n"

# generates table header for summary
def tableheader(num_runs):
    return "\n" + \
        "{:<34}".format("Name") + \
        "{:<10}".format("Impl.?") + \
        "{:<10}".format("Result") + \
        "{:<9}".format("Size") + \
        "{:<7}".format("|P|") + \
        "{:<14}".format("Size Proj's") + \
        "{:<8}".format("Time in ms ") + \
        "(average of " + str(num_runs) + (" run)" if num_runs == 1 else " runs)") + \
        "\n\n"
